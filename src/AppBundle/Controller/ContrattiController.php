<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Account;
use AppBundle\Entity\Contratti;
use AppBundle\Entity\Users;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\User\User;

/**
 * Contratti controller.
 *
 * @Route("contratti")
 */
class ContrattiController extends Controller
{
    /**
     * Lists all contratti entities.
     *
     * @Route("/", name="contratti_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        /** @var QueryBuilder $qb */
        $qb = $em->createQueryBuilder();
        $qb->select('c')
            ->from('AppBundle:Contratti', 'c')
            ->innerJoin('c.contrattiCstm', 'cc')
            ->innerJoin('cc.offerteWeb', 'ow')
            ->andWhere('ow.ricaricabile = 1')
            ->andWhere('c.deleted = 0')
        ;

        $query = $qb->getQuery();
        $contrattis = $query->getResult();

        $accounts = $em->getRepository(Account::class)->findAll();
        $users = $em->getRepository(Users::class)->findAll();



//        $b = $contrattis->getAccountContratti()


//        foreach ($contrattis as $contratti) {
//            $clientName[] = $contratti->getAccountContratti()->getAccount()->getName();
//            var_dump($clientName);
//        }



        return $this->render('contratti/index.html.twig', array(
            'contrattis' => $contrattis, 'accounts' => $accounts, 'users' => $users
        ));
    }

    /**
     * Creates a new contratti entity.
     *
     * @Route("/new", name="contratti_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $contratti = new Contratti();
        $form = $this->createForm('AppBundle\Form\ContrattiType', $contratti);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($contratti);
            $em->flush();

            return $this->redirectToRoute('contratti_show', array('id' => $contratti->getId()));
        }

        return $this->render('contratti/new.html.twig', array(
            'contratti' => $contratti,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a contratti entity.
     *
     * @Route("/{id}", name="contratti_show")
     * @Method("GET")
     */
    public function showAction(Contratti $contratti)
    {
        $deleteForm = $this->createDeleteForm($contratti);

        return $this->render('contratti/show.html.twig', array(
            'contratti' => $contratti,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing contratti entity.
     *
     * @Route("/{id}/edit", name="contratti_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Contratti $contratti)
    {
        $deleteForm = $this->createDeleteForm($contratti);
        $editForm = $this->createForm('AppBundle\Form\ContrattiType', $contratti);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('contratti_edit', array('id' => $contratti->getId()));
        }

        return $this->render('contratti/edit.html.twig', array(
            'contratti' => $contratti,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a contratti entity.
     *
     * @Route("/{id}", name="contratti_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Contratti $contratti)
    {
        $form = $this->createDeleteForm($contratti);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($contratti);
            $em->flush();
        }

        return $this->redirectToRoute('contratti_index');
    }

    /**
     * Creates a form to delete a contratti entity.
     *
     * @param Contratti $contratti The contratti entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Contratti $contratti)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('contratti_delete', array('id' => $contratti->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
