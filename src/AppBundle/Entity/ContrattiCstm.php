<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ContrattiCstm
 *
 * @ORM\Table(name="contratti_cstm", indexes={@ORM\Index(name="id_c", columns={"id_c"}), @ORM\Index(name="IDX_contratti_cstm", columns={"id_c", "ContrattoStatus_c"}), @ORM\Index(name="IDX_contratti_cstm_ContrattoStatus_c", columns={"ContrattoStatus_c"}), @ORM\Index(name="IDX_contratti_cstm_DataDisdetta_c", columns={"DataDisdetta_c"}), @ORM\Index(name="IDX_contratti_cstm_DataOrdine_c", columns={"DataOrdine_c"}), @ORM\Index(name="IDX_contratti_cstm_DataSpedizione_c", columns={"DataSpedizione_c"}), @ORM\Index(name="IDX_contratti_cstm_MacAddress_c", columns={"MacAddress_c"}), @ORM\Index(name="IDX_contratti_cstm_NumeroVoucher_c", columns={"NumeroVoucher_c"}), @ORM\Index(name="IDX_contratti_cstm_Rid_Sottoscrittore_CodiceFiscale_c", columns={"Rid_Sottoscrittore_CodiceFiscale_c"}), @ORM\Index(name="IDX_contratti_cstm_TicketFibra", columns={"TicketFibraCostiSupp_c"}), @ORM\Index(name="IDX_contratti_cstm2", columns={"DataOrdine_c", "DataSpedizione_c", "ContrattoStatus_c", "AutorizzazioneRID_c", "TipoPagamento_c", "IDUserConsegna_c", "IDInstallatore_c"}), @ORM\Index(name="IDX_contratti_cstm3", columns={"DataOrdine_c", "DataSpedizione_c", "AutorizzazioneRID_c", "TipoPagamento_c", "ContrattoStatus_c", "IDInstallatore_c", "IDUserConsegna_c", "UbicazioneComune_c", "shippingapparato_localita_c"}), @ORM\Index(name="IDX_contratti_cstm4", columns={"CodiceAntenna_c", "MacAddress_c", "id_c"}), @ORM\Index(name="NumeroContratto__c", columns={"NumeroContratto_c"}), @ORM\Index(name="UK_contratti_cstm", columns={"AutorizzazioneRID_c", "ContrattoStatus_c"}), @ORM\Index(name="UK_contratti_cstm_AutorizzazioneRID_c", columns={"AutorizzazioneRID_c"}), @ORM\Index(name="UK_contratti_cstm_CodiceRID_c", columns={"CodiceRID_c"})})
 * @ORM\Entity
 */
class ContrattiCstm
{



    /**
     * @var Users
     *
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Users", inversedBy="contrattiCstm")
     * @ORM\JoinColumn(name="IDUserConsegna_c", referencedColumnName="id")
     */
    private $users;

    /**
     * @var OfferteWeb
     *
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\OfferteWeb")
     * @ORM\JoinColumn(name="ID_OffertaWeb_c", referencedColumnName="ID", nullable=true)
     */
    private $offerteWeb;



    /**
     * @var Contratti
     *
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Contratti", inversedBy="contrattiCstm")
     * @ORM\JoinColumn(name="id_c", referencedColumnName="id")
     */
    private $contratti;

    /**
     * @var string
     *
     * @ORM\Column(name="id_c", type="string", length=36, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $idC = '\'\'';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DataOrdine_c", type="date", nullable=false)
     */
    private $dataordineC;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DataSpedizione_c", type="date", nullable=true)
     */
    private $dataspedizioneC = 'NULL';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DataInstallazione_c", type="date", nullable=true)
     */
    private $datainstallazioneC = 'NULL';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DataAttivazione_c", type="date", nullable=true)
     */
    private $dataattivazioneC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="Rid_Banca_c", type="string", length=45, nullable=false)
     */
    private $ridBancaC = '\'\'';

    /**
     * @var string
     *
     * @ORM\Column(name="Rid_Agenzia_c", type="string", length=45, nullable=false)
     */
    private $ridAgenziaC = '\'\'';

    /**
     * @var string
     *
     * @ORM\Column(name="Rid_IndirizzoBanca_c", type="string", length=45, nullable=false)
     */
    private $ridIndirizzobancaC = '\'\'';

    /**
     * @var string
     *
     * @ORM\Column(name="Rid_ABI_c", type="string", length=5, nullable=false)
     */
    private $ridAbiC = '\'\'';

    /**
     * @var string
     *
     * @ORM\Column(name="Rid_CAB_c", type="string", length=5, nullable=false)
     */
    private $ridCabC = '\'\'';

    /**
     * @var string
     *
     * @ORM\Column(name="Rid_CIN_c", type="string", length=1, nullable=true)
     */
    private $ridCinC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="Rid_CC_c", type="string", length=20, nullable=false)
     */
    private $ridCcC = '\'\'';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="Rid_DataAdesione_c", type="date", nullable=false)
     */
    private $ridDataadesioneC;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="Rid_DataRevoca_c", type="date", nullable=true)
     */
    private $ridDatarevocaC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="Rid_Sottoscrittore_c", type="string", length=100, nullable=false)
     */
    private $ridSottoscrittoreC = '\'\'';

    /**
     * @var string
     *
     * @ORM\Column(name="Rid_Sottoscrittore_Indirizzo_c", type="string", length=255, nullable=true)
     */
    private $ridSottoscrittoreIndirizzoC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="Rid_Sottoscrittore_CodiceFiscale_c", type="string", length=16, nullable=false)
     */
    private $ridSottoscrittoreCodicefiscaleC = '\'\'';

    /**
     * @var string
     *
     * @ORM\Column(name="Rid_IntestatarioConto_c", type="string", length=45, nullable=true)
     */
    private $ridIntestatariocontoC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="Rid_IntestatarioConto_CodiceFiscale_c", type="string", length=16, nullable=true)
     */
    private $ridIntestatariocontoCodicefiscaleC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="CodiceAntenna_c", type="string", length=45, nullable=false)
     */
    private $codiceantennaC = '\'\'';

    /**
     * @var string
     *
     * @ORM\Column(name="IPAntenna_c", type="string", length=15, nullable=false)
     */
    private $ipantennaC = '\'\'';

    /**
     * @var string
     *
     * @ORM\Column(name="LBL_DatiRid_c", type="string", length=1, nullable=true)
     */
    private $lblDatiridC = 'NULL';

    /**
     * @var boolean
     *
     * @ORM\Column(name="RichiestaInstallazione_c", type="boolean", nullable=true)
     */
    private $richiestainstallazioneC = 'NULL';

    /**
     * @var float
     *
     * @ORM\Column(name="ImportoAnticipo_c", type="float", precision=10, scale=0, nullable=true)
     */
    private $importoanticipoC = 'NULL';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DataDisdetta_c", type="date", nullable=true)
     */
    private $datadisdettaC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="TipoPagamento_c", type="string", length=50, nullable=false)
     */
    private $tipopagamentoC = '\'P1\'';

    /**
     * @var string
     *
     * @ORM\Column(name="ContrattoStatus_c", type="string", length=20, nullable=false)
     */
    private $contrattostatusC = '\'Attivo\'';

    /**
     * @var string
     *
     * @ORM\Column(name="AutorizzazioneRID_c", type="string", length=20, nullable=false)
     */
    private $autorizzazioneridC = '\'K1\'';

    /**
     * @var string
     *
     * @ORM\Column(name="ModalitaInvioDocumenti_c", type="string", length=10, nullable=false)
     */
    private $modalitainviodocumentiC = '\'K1\'';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DataRiconsegnaAntenna_c", type="date", nullable=true)
     */
    private $datariconsegnaantennaC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="Azienda_c", type="string", length=20, nullable=false)
     */
    private $aziendaC = '\'wavemax\'';

    /**
     * @var string
     *
     * @ORM\Column(name="NoteSpedizione_c", type="string", length=255, nullable=true)
     */
    private $notespedizioneC = 'NULL';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DataSpedizionePrevista_c", type="date", nullable=true)
     */
    private $dataspedizioneprevistaC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="StatoDataSpedizionePrevista_c", type="string", length=10, nullable=true)
     */
    private $statodataspedizioneprevistaC = '\'K2\'';

    /**
     * @var boolean
     *
     * @ORM\Column(name="CreditoConsumo_c", type="boolean", nullable=false)
     */
    private $creditoconsumoC = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="NumeroContrattoCC_c", type="string", length=50, nullable=true)
     */
    private $numerocontrattoccC = 'NULL';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DataInizioFatturazione_c", type="date", nullable=true)
     */
    private $datainiziofatturazioneC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="Azienda_Proprietaria_c", type="string", length=50, nullable=false)
     */
    private $aziendaProprietariaC = '\'Ariacom\'';

    /**
     * @var integer
     *
     * @ORM\Column(name="MinutiBonus_c", type="integer", nullable=true)
     */
    private $minutibonusC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="IBAN_c", type="string", length=30, nullable=false)
     */
    private $ibanC = '\'\'';

    /**
     * @var boolean
     *
     * @ORM\Column(name="EscludiDaStatistiche_c", type="boolean", nullable=true)
     */
    private $escludidastatisticheC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="CodiceIndividuale_c", type="string", length=16, nullable=true)
     */
    private $codiceindividualeC = 'NULL';

    /**
     * @var integer
     *
     * @ORM\Column(name="NumeroContratto__c", type="integer", nullable=false)
     */
    private $numerocontrattocC = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="UbicazioneCognome_c", type="string", length=255, nullable=true)
     */
    private $ubicazionecognomeC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="UbicazioneIndirizzo_c", type="string", length=255, nullable=true)
     */
    private $ubicazioneindirizzoC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="UbicazioneComune_c", type="string", length=255, nullable=true)
     */
    private $ubicazionecomuneC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="UbicazioneProvincia_c", type="string", length=255, nullable=true)
     */
    private $ubicazioneprovinciaC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="UbicazioneCAP_c", type="string", length=255, nullable=true)
     */
    private $ubicazionecapC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="MacAddress_c", type="string", length=20, nullable=false)
     */
    private $macaddressC = '\'\'';

    /**
     * @var string
     *
     * @ORM\Column(name="idTipoQuestionario_c", type="string", length=20, nullable=true)
     */
    private $idtipoquestionarioC = 'NULL';

    /**
     * @var integer
     *
     * @ORM\Column(name="NumeroContratto_c", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $numerocontrattoC;

    /**
     * @var string
     *
     * @ORM\Column(name="UbicazioneNome_c", type="string", length=255, nullable=true)
     */
    private $ubicazionenomeC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="NoteInstallatore_c", type="text", length=65535, nullable=true)
     */
    private $noteinstallatoreC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="shippingapparato_cognome_c", type="string", length=50, nullable=true)
     */
    private $shippingapparatoCognomeC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="shippingapparato_nome_c", type="string", length=50, nullable=true)
     */
    private $shippingapparatoNomeC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="shippingapparato_indirizzo_c", type="string", length=200, nullable=true)
     */
    private $shippingapparatoIndirizzoC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="shippingapparato_localita_c", type="string", length=200, nullable=true)
     */
    private $shippingapparatoLocalitaC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="shippingapparato_citta_c", type="string", length=150, nullable=true)
     */
    private $shippingapparatoCittaC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="shippingapparato_provincia_c", type="string", length=150, nullable=true)
     */
    private $shippingapparatoProvinciaC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="shippingapparato_cap_c", type="string", length=6, nullable=true)
     */
    private $shippingapparatoCapC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="Rid_Sottoscrittore_Localita_c", type="string", length=255, nullable=true)
     */
    private $ridSottoscrittoreLocalitaC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="UbicazioneFrazione_c", type="string", length=255, nullable=true)
     */
    private $ubicazionefrazioneC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="IDInstallatore_c", type="string", length=36, nullable=true)
     */
    private $idinstallatoreC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="UsernameInstallatore_c", type="string", length=255, nullable=true)
     */
    private $usernameinstallatoreC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="EsitoInstallazione_c", type="string", length=5, nullable=true)
     */
    private $esitoinstallazioneC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="EsitoSpedizione_c", type="string", length=5, nullable=true)
     */
    private $esitospedizioneC = 'NULL';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DataConsegna_c", type="date", nullable=true)
     */
    private $dataconsegnaC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="NoteEsitoSpedizione_c", type="string", length=255, nullable=true)
     */
    private $noteesitospedizioneC = 'NULL';

    /**
     * @var boolean
     *
     * @ORM\Column(name="UbicazioneBypassCopertura_c", type="boolean", nullable=true)
     */
    private $ubicazionebypasscoperturaC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="UbicazioneLatGM_c", type="decimal", precision=12, scale=9, nullable=true)
     */
    private $ubicazionelatgmC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="UbicazioneLonGM_c", type="decimal", precision=12, scale=9, nullable=true)
     */
    private $ubicazionelongmC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="UbicazioneLatDB_c", type="decimal", precision=12, scale=9, nullable=true)
     */
    private $ubicazionelatdbC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="UbicazioneLonDB_c", type="decimal", precision=12, scale=9, nullable=true)
     */
    private $ubicazionelondbC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="IDUserConsegna_c", type="string", length=36, nullable=false)
     */
    private $iduserconsegnaC = NULL;

    /**
     * @var string
     *
     * @ORM\Column(name="UserNameConsegna_c", type="string", length=255, nullable=true)
     */
    private $usernameconsegnaC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="ID_OffertaWeb_c", type="string", length=36, nullable=true)
     */
    private $idOffertawebC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="StatoRicezioneContratto_c", type="string", length=5, nullable=true)
     */
    private $statoricezionecontrattoC = '\'K0\'';

    /**
     * @var string
     *
     * @ORM\Column(name="NrDDT_c", type="string", length=20, nullable=true)
     */
    private $nrddtC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="NoteDisdetta_c", type="text", length=65535, nullable=true)
     */
    private $notedisdettaC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="IDUserRiconsegna_c", type="string", length=36, nullable=true)
     */
    private $iduserriconsegnaC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="TipoFattura_c", type="string", length=10, nullable=false)
     */
    private $tipofatturaC = '\'GOWM\'';

    /**
     * @var string
     *
     * @ORM\Column(name="UltimaPeriodicitaFatt_c", type="string", length=10, nullable=true)
     */
    private $ultimaperiodicitafattC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="CodiceRID_c", type="string", length=20, nullable=true)
     */
    private $codiceridC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="TipoCodiceRID_c", type="string", length=5, nullable=true)
     */
    private $tipocodiceridC = '\'K4\'';

    /**
     * @var integer
     *
     * @ORM\Column(name="percentuale_provvigione_c", type="integer", nullable=true)
     */
    private $percentualeProvvigioneC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="actionaea_c", type="string", length=30, nullable=true)
     */
    private $actionaeaC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="MotivoDisdetta_c", type="text", length=65535, nullable=true)
     */
    private $motivodisdettaC = 'NULL';

    /**
     * @var boolean
     *
     * @ORM\Column(name="Contabilizzato_c", type="boolean", nullable=true)
     */
    private $contabilizzatoC = 'NULL';

    /**
     * @var boolean
     *
     * @ORM\Column(name="CPEAttiva_c", type="boolean", nullable=true)
     */
    private $cpeattivaC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="NumeroVoucher_c", type="string", length=20, nullable=true)
     */
    private $numerovoucherC = 'NULL';

    /**
     * @var boolean
     *
     * @ORM\Column(name="migrazione_c", type="boolean", nullable=true)
     */
    private $migrazioneC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="StatoDisdetta_c", type="string", length=2, nullable=true)
     */
    private $statodisdettaC = 'NULL';

    /**
     * @var boolean
     *
     * @ORM\Column(name="esclusioneinviti_c", type="boolean", nullable=true)
     */
    private $esclusioneinvitiC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="IDBuilding_c", type="string", length=50, nullable=true)
     */
    private $idbuildingC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="UbicazioneTelefono_c_c", type="string", length=30, nullable=true)
     */
    private $ubicazionetelefonoCC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="UbicazioneTelefono_c", type="string", length=30, nullable=true)
     */
    private $ubicazionetelefonoC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="bs_name_c", type="string", length=255, nullable=true)
     */
    private $bsNameC = 'NULL';

    /**
     * @var integer
     *
     * @ORM\Column(name="settore_id_c", type="integer", nullable=true)
     */
    private $settoreIdC = 'NULL';

    /**
     * @var integer
     *
     * @ORM\Column(name="bs_int_id_c", type="integer", nullable=true)
     */
    private $bsIntIdC = 'NULL';

    /**
     * @var integer
     *
     * @ORM\Column(name="eof_c", type="integer", nullable=true)
     */
    private $eofC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="Profilo_VoIP_c", type="string", length=50, nullable=true)
     */
    private $profiloVoipC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="numeroappoggio_c", type="string", length=30, nullable=true)
     */
    private $numeroappoggioC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="_06_GPON_DI_ATTESTAZIONE_c", type="string", length=100, nullable=true)
     */
    private $GponDiAttestazioneC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="_15_ID_SPLITTER_SECONDARIO_c", type="string", length=100, nullable=true)
     */
    private $IdSplitterSecondarioC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="_16_POSIZIONE_SPLITTER_SECONDARIO_c", type="string", length=100, nullable=true)
     */
    private $PosizioneSplitterSecondarioC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="_14_PARAMETRI_TRASMISSIVI_OTTICI_c", type="string", length=100, nullable=true)
     */
    private $ParametriTrasmissiviOtticiC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="fr_tipo_com_c", type="string", length=255, nullable=true)
     */
    private $frTipoComC = 'NULL';

    /**
     * @var boolean
     *
     * @ORM\Column(name="coverage_type_c", type="boolean", nullable=true)
     */
    private $coverageTypeC = 'NULL';

    /**
     * @var boolean
     *
     * @ORM\Column(name="coverage_forced_c", type="boolean", nullable=true)
     */
    private $coverageForcedC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="idcnt_c", type="string", length=36, nullable=true)
     */
    private $idcntC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="TicketFibraCostiSupp_c", type="string", length=6, nullable=true)
     */
    private $ticketfibracostisuppC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="pin_c", type="string", length=5, nullable=true)
     */
    private $pinC = 'NULL';

    /**
     * @return string
     */
    public function getIdC()
    {
        return $this->idC;
    }

    /**
     * @param string $idC
     * @return ContrattiCstm
     */
    public function setIdC($idC)
    {
        $this->idC = $idC;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDataordineC()
    {
        return $this->dataordineC;
    }

    /**
     * @param \DateTime $dataordineC
     * @return ContrattiCstm
     */
    public function setDataordineC($dataordineC)
    {
        $this->dataordineC = $dataordineC;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDataspedizioneC()
    {
        return $this->dataspedizioneC;
    }

    /**
     * @param \DateTime $dataspedizioneC
     * @return ContrattiCstm
     */
    public function setDataspedizioneC($dataspedizioneC)
    {
        $this->dataspedizioneC = $dataspedizioneC;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDatainstallazioneC()
    {
        return $this->datainstallazioneC;
    }

    /**
     * @param \DateTime $datainstallazioneC
     * @return ContrattiCstm
     */
    public function setDatainstallazioneC($datainstallazioneC)
    {
        $this->datainstallazioneC = $datainstallazioneC;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDataattivazioneC()
    {
        return $this->dataattivazioneC;
    }

    /**
     * @param \DateTime $dataattivazioneC
     * @return ContrattiCstm
     */
    public function setDataattivazioneC($dataattivazioneC)
    {
        $this->dataattivazioneC = $dataattivazioneC;
        return $this;
    }

    /**
     * @return string
     */
    public function getRidBancaC()
    {
        return $this->ridBancaC;
    }

    /**
     * @param string $ridBancaC
     * @return ContrattiCstm
     */
    public function setRidBancaC($ridBancaC)
    {
        $this->ridBancaC = $ridBancaC;
        return $this;
    }

    /**
     * @return string
     */
    public function getRidAgenziaC()
    {
        return $this->ridAgenziaC;
    }

    /**
     * @param string $ridAgenziaC
     * @return ContrattiCstm
     */
    public function setRidAgenziaC($ridAgenziaC)
    {
        $this->ridAgenziaC = $ridAgenziaC;
        return $this;
    }

    /**
     * @return string
     */
    public function getRidIndirizzobancaC()
    {
        return $this->ridIndirizzobancaC;
    }

    /**
     * @param string $ridIndirizzobancaC
     * @return ContrattiCstm
     */
    public function setRidIndirizzobancaC($ridIndirizzobancaC)
    {
        $this->ridIndirizzobancaC = $ridIndirizzobancaC;
        return $this;
    }

    /**
     * @return string
     */
    public function getRidAbiC()
    {
        return $this->ridAbiC;
    }

    /**
     * @param string $ridAbiC
     * @return ContrattiCstm
     */
    public function setRidAbiC($ridAbiC)
    {
        $this->ridAbiC = $ridAbiC;
        return $this;
    }

    /**
     * @return string
     */
    public function getRidCabC()
    {
        return $this->ridCabC;
    }

    /**
     * @param string $ridCabC
     * @return ContrattiCstm
     */
    public function setRidCabC($ridCabC)
    {
        $this->ridCabC = $ridCabC;
        return $this;
    }

    /**
     * @return string
     */
    public function getRidCinC()
    {
        return $this->ridCinC;
    }

    /**
     * @param string $ridCinC
     * @return ContrattiCstm
     */
    public function setRidCinC($ridCinC)
    {
        $this->ridCinC = $ridCinC;
        return $this;
    }

    /**
     * @return string
     */
    public function getRidCcC()
    {
        return $this->ridCcC;
    }

    /**
     * @param string $ridCcC
     * @return ContrattiCstm
     */
    public function setRidCcC($ridCcC)
    {
        $this->ridCcC = $ridCcC;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getRidDataadesioneC()
    {
        return $this->ridDataadesioneC;
    }

    /**
     * @param \DateTime $ridDataadesioneC
     * @return ContrattiCstm
     */
    public function setRidDataadesioneC($ridDataadesioneC)
    {
        $this->ridDataadesioneC = $ridDataadesioneC;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getRidDatarevocaC()
    {
        return $this->ridDatarevocaC;
    }

    /**
     * @param \DateTime $ridDatarevocaC
     * @return ContrattiCstm
     */
    public function setRidDatarevocaC($ridDatarevocaC)
    {
        $this->ridDatarevocaC = $ridDatarevocaC;
        return $this;
    }

    /**
     * @return string
     */
    public function getRidSottoscrittoreC()
    {
        return $this->ridSottoscrittoreC;
    }

    /**
     * @param string $ridSottoscrittoreC
     * @return ContrattiCstm
     */
    public function setRidSottoscrittoreC($ridSottoscrittoreC)
    {
        $this->ridSottoscrittoreC = $ridSottoscrittoreC;
        return $this;
    }

    /**
     * @return string
     */
    public function getRidSottoscrittoreIndirizzoC()
    {
        return $this->ridSottoscrittoreIndirizzoC;
    }

    /**
     * @param string $ridSottoscrittoreIndirizzoC
     * @return ContrattiCstm
     */
    public function setRidSottoscrittoreIndirizzoC($ridSottoscrittoreIndirizzoC)
    {
        $this->ridSottoscrittoreIndirizzoC = $ridSottoscrittoreIndirizzoC;
        return $this;
    }

    /**
     * @return string
     */
    public function getRidSottoscrittoreCodicefiscaleC()
    {
        return $this->ridSottoscrittoreCodicefiscaleC;
    }

    /**
     * @param string $ridSottoscrittoreCodicefiscaleC
     * @return ContrattiCstm
     */
    public function setRidSottoscrittoreCodicefiscaleC($ridSottoscrittoreCodicefiscaleC)
    {
        $this->ridSottoscrittoreCodicefiscaleC = $ridSottoscrittoreCodicefiscaleC;
        return $this;
    }

    /**
     * @return string
     */
    public function getRidIntestatariocontoC()
    {
        return $this->ridIntestatariocontoC;
    }

    /**
     * @param string $ridIntestatariocontoC
     * @return ContrattiCstm
     */
    public function setRidIntestatariocontoC($ridIntestatariocontoC)
    {
        $this->ridIntestatariocontoC = $ridIntestatariocontoC;
        return $this;
    }

    /**
     * @return string
     */
    public function getRidIntestatariocontoCodicefiscaleC()
    {
        return $this->ridIntestatariocontoCodicefiscaleC;
    }

    /**
     * @param string $ridIntestatariocontoCodicefiscaleC
     * @return ContrattiCstm
     */
    public function setRidIntestatariocontoCodicefiscaleC($ridIntestatariocontoCodicefiscaleC)
    {
        $this->ridIntestatariocontoCodicefiscaleC = $ridIntestatariocontoCodicefiscaleC;
        return $this;
    }

    /**
     * @return string
     */
    public function getCodiceantennaC()
    {
        return $this->codiceantennaC;
    }

    /**
     * @param string $codiceantennaC
     * @return ContrattiCstm
     */
    public function setCodiceantennaC($codiceantennaC)
    {
        $this->codiceantennaC = $codiceantennaC;
        return $this;
    }

    /**
     * @return string
     */
    public function getIpantennaC()
    {
        return $this->ipantennaC;
    }

    /**
     * @param string $ipantennaC
     * @return ContrattiCstm
     */
    public function setIpantennaC($ipantennaC)
    {
        $this->ipantennaC = $ipantennaC;
        return $this;
    }

    /**
     * @return string
     */
    public function getLblDatiridC()
    {
        return $this->lblDatiridC;
    }

    /**
     * @param string $lblDatiridC
     * @return ContrattiCstm
     */
    public function setLblDatiridC($lblDatiridC)
    {
        $this->lblDatiridC = $lblDatiridC;
        return $this;
    }

    /**
     * @return bool
     */
    public function isRichiestainstallazioneC()
    {
        return $this->richiestainstallazioneC;
    }

    /**
     * @param bool $richiestainstallazioneC
     * @return ContrattiCstm
     */
    public function setRichiestainstallazioneC($richiestainstallazioneC)
    {
        $this->richiestainstallazioneC = $richiestainstallazioneC;
        return $this;
    }

    /**
     * @return float
     */
    public function getImportoanticipoC()
    {
        return $this->importoanticipoC;
    }

    /**
     * @param float $importoanticipoC
     * @return ContrattiCstm
     */
    public function setImportoanticipoC($importoanticipoC)
    {
        $this->importoanticipoC = $importoanticipoC;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDatadisdettaC()
    {
        return $this->datadisdettaC;
    }

    /**
     * @param \DateTime $datadisdettaC
     * @return ContrattiCstm
     */
    public function setDatadisdettaC($datadisdettaC)
    {
        $this->datadisdettaC = $datadisdettaC;
        return $this;
    }

    /**
     * @return string
     */
    public function getTipopagamentoC()
    {
        return $this->tipopagamentoC;
    }

    /**
     * @param string $tipopagamentoC
     * @return ContrattiCstm
     */
    public function setTipopagamentoC($tipopagamentoC)
    {
        $this->tipopagamentoC = $tipopagamentoC;
        return $this;
    }

    /**
     * @return string
     */
    public function getContrattostatusC()
    {
        return $this->contrattostatusC;
    }

    /**
     * @param string $contrattostatusC
     * @return ContrattiCstm
     */
    public function setContrattostatusC($contrattostatusC)
    {
        $this->contrattostatusC = $contrattostatusC;
        return $this;
    }

    /**
     * @return string
     */
    public function getAutorizzazioneridC()
    {
        return $this->autorizzazioneridC;
    }

    /**
     * @param string $autorizzazioneridC
     * @return ContrattiCstm
     */
    public function setAutorizzazioneridC($autorizzazioneridC)
    {
        $this->autorizzazioneridC = $autorizzazioneridC;
        return $this;
    }

    /**
     * @return string
     */
    public function getModalitainviodocumentiC()
    {
        return $this->modalitainviodocumentiC;
    }

    /**
     * @param string $modalitainviodocumentiC
     * @return ContrattiCstm
     */
    public function setModalitainviodocumentiC($modalitainviodocumentiC)
    {
        $this->modalitainviodocumentiC = $modalitainviodocumentiC;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDatariconsegnaantennaC()
    {
        return $this->datariconsegnaantennaC;
    }

    /**
     * @param \DateTime $datariconsegnaantennaC
     * @return ContrattiCstm
     */
    public function setDatariconsegnaantennaC($datariconsegnaantennaC)
    {
        $this->datariconsegnaantennaC = $datariconsegnaantennaC;
        return $this;
    }

    /**
     * @return string
     */
    public function getAziendaC()
    {
        return $this->aziendaC;
    }

    /**
     * @param string $aziendaC
     * @return ContrattiCstm
     */
    public function setAziendaC($aziendaC)
    {
        $this->aziendaC = $aziendaC;
        return $this;
    }

    /**
     * @return string
     */
    public function getNotespedizioneC()
    {
        return $this->notespedizioneC;
    }

    /**
     * @param string $notespedizioneC
     * @return ContrattiCstm
     */
    public function setNotespedizioneC($notespedizioneC)
    {
        $this->notespedizioneC = $notespedizioneC;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDataspedizioneprevistaC()
    {
        return $this->dataspedizioneprevistaC;
    }

    /**
     * @param \DateTime $dataspedizioneprevistaC
     * @return ContrattiCstm
     */
    public function setDataspedizioneprevistaC($dataspedizioneprevistaC)
    {
        $this->dataspedizioneprevistaC = $dataspedizioneprevistaC;
        return $this;
    }

    /**
     * @return string
     */
    public function getStatodataspedizioneprevistaC()
    {
        return $this->statodataspedizioneprevistaC;
    }

    /**
     * @param string $statodataspedizioneprevistaC
     * @return ContrattiCstm
     */
    public function setStatodataspedizioneprevistaC($statodataspedizioneprevistaC)
    {
        $this->statodataspedizioneprevistaC = $statodataspedizioneprevistaC;
        return $this;
    }

    /**
     * @return bool
     */
    public function isCreditoconsumoC()
    {
        return $this->creditoconsumoC;
    }

    /**
     * @param bool $creditoconsumoC
     * @return ContrattiCstm
     */
    public function setCreditoconsumoC($creditoconsumoC)
    {
        $this->creditoconsumoC = $creditoconsumoC;
        return $this;
    }

    /**
     * @return string
     */
    public function getNumerocontrattoccC()
    {
        return $this->numerocontrattoccC;
    }

    /**
     * @param string $numerocontrattoccC
     * @return ContrattiCstm
     */
    public function setNumerocontrattoccC($numerocontrattoccC)
    {
        $this->numerocontrattoccC = $numerocontrattoccC;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDatainiziofatturazioneC()
    {
        return $this->datainiziofatturazioneC;
    }

    /**
     * @param \DateTime $datainiziofatturazioneC
     * @return ContrattiCstm
     */
    public function setDatainiziofatturazioneC($datainiziofatturazioneC)
    {
        $this->datainiziofatturazioneC = $datainiziofatturazioneC;
        return $this;
    }

    /**
     * @return string
     */
    public function getAziendaProprietariaC()
    {
        return $this->aziendaProprietariaC;
    }

    /**
     * @param string $aziendaProprietariaC
     * @return ContrattiCstm
     */
    public function setAziendaProprietariaC($aziendaProprietariaC)
    {
        $this->aziendaProprietariaC = $aziendaProprietariaC;
        return $this;
    }

    /**
     * @return int
     */
    public function getMinutibonusC()
    {
        return $this->minutibonusC;
    }

    /**
     * @param int $minutibonusC
     * @return ContrattiCstm
     */
    public function setMinutibonusC($minutibonusC)
    {
        $this->minutibonusC = $minutibonusC;
        return $this;
    }

    /**
     * @return string
     */
    public function getIbanC()
    {
        return $this->ibanC;
    }

    /**
     * @param string $ibanC
     * @return ContrattiCstm
     */
    public function setIbanC($ibanC)
    {
        $this->ibanC = $ibanC;
        return $this;
    }

    /**
     * @return bool
     */
    public function isEscludidastatisticheC()
    {
        return $this->escludidastatisticheC;
    }

    /**
     * @param bool $escludidastatisticheC
     * @return ContrattiCstm
     */
    public function setEscludidastatisticheC($escludidastatisticheC)
    {
        $this->escludidastatisticheC = $escludidastatisticheC;
        return $this;
    }

    /**
     * @return string
     */
    public function getCodiceindividualeC()
    {
        return $this->codiceindividualeC;
    }

    /**
     * @param string $codiceindividualeC
     * @return ContrattiCstm
     */
    public function setCodiceindividualeC($codiceindividualeC)
    {
        $this->codiceindividualeC = $codiceindividualeC;
        return $this;
    }

    /**
     * @return int
     */
    public function getNumerocontrattocC()
    {
        return $this->numerocontrattocC;
    }

    /**
     * @param int $numerocontrattocC
     * @return ContrattiCstm
     */
    public function setNumerocontrattocC($numerocontrattocC)
    {
        $this->numerocontrattocC = $numerocontrattocC;
        return $this;
    }

    /**
     * @return string
     */
    public function getUbicazionecognomeC()
    {
        return $this->ubicazionecognomeC;
    }

    /**
     * @param string $ubicazionecognomeC
     * @return ContrattiCstm
     */
    public function setUbicazionecognomeC($ubicazionecognomeC)
    {
        $this->ubicazionecognomeC = $ubicazionecognomeC;
        return $this;
    }

    /**
     * @return string
     */
    public function getUbicazioneindirizzoC()
    {
        return $this->ubicazioneindirizzoC;
    }

    /**
     * @param string $ubicazioneindirizzoC
     * @return ContrattiCstm
     */
    public function setUbicazioneindirizzoC($ubicazioneindirizzoC)
    {
        $this->ubicazioneindirizzoC = $ubicazioneindirizzoC;
        return $this;
    }

    /**
     * @return string
     */
    public function getUbicazionecomuneC()
    {
        return $this->ubicazionecomuneC;
    }

    /**
     * @param string $ubicazionecomuneC
     * @return ContrattiCstm
     */
    public function setUbicazionecomuneC($ubicazionecomuneC)
    {
        $this->ubicazionecomuneC = $ubicazionecomuneC;
        return $this;
    }

    /**
     * @return string
     */
    public function getUbicazioneprovinciaC()
    {
        return $this->ubicazioneprovinciaC;
    }

    /**
     * @param string $ubicazioneprovinciaC
     * @return ContrattiCstm
     */
    public function setUbicazioneprovinciaC($ubicazioneprovinciaC)
    {
        $this->ubicazioneprovinciaC = $ubicazioneprovinciaC;
        return $this;
    }

    /**
     * @return string
     */
    public function getUbicazionecapC()
    {
        return $this->ubicazionecapC;
    }

    /**
     * @param string $ubicazionecapC
     * @return ContrattiCstm
     */
    public function setUbicazionecapC($ubicazionecapC)
    {
        $this->ubicazionecapC = $ubicazionecapC;
        return $this;
    }

    /**
     * @return string
     */
    public function getMacaddressC()
    {
        return $this->macaddressC;
    }

    /**
     * @param string $macaddressC
     * @return ContrattiCstm
     */
    public function setMacaddressC($macaddressC)
    {
        $this->macaddressC = $macaddressC;
        return $this;
    }

    /**
     * @return string
     */
    public function getIdtipoquestionarioC()
    {
        return $this->idtipoquestionarioC;
    }

    /**
     * @param string $idtipoquestionarioC
     * @return ContrattiCstm
     */
    public function setIdtipoquestionarioC($idtipoquestionarioC)
    {
        $this->idtipoquestionarioC = $idtipoquestionarioC;
        return $this;
    }

    /**
     * @return int
     */
    public function getNumerocontrattoC()
    {
        return $this->numerocontrattoC;
    }

    /**
     * @param int $numerocontrattoC
     * @return ContrattiCstm
     */
    public function setNumerocontrattoC($numerocontrattoC)
    {
        $this->numerocontrattoC = $numerocontrattoC;
        return $this;
    }

    /**
     * @return string
     */
    public function getUbicazionenomeC()
    {
        return $this->ubicazionenomeC;
    }

    /**
     * @param string $ubicazionenomeC
     * @return ContrattiCstm
     */
    public function setUbicazionenomeC($ubicazionenomeC)
    {
        $this->ubicazionenomeC = $ubicazionenomeC;
        return $this;
    }

    /**
     * @return string
     */
    public function getNoteinstallatoreC()
    {
        return $this->noteinstallatoreC;
    }

    /**
     * @param string $noteinstallatoreC
     * @return ContrattiCstm
     */
    public function setNoteinstallatoreC($noteinstallatoreC)
    {
        $this->noteinstallatoreC = $noteinstallatoreC;
        return $this;
    }

    /**
     * @return string
     */
    public function getShippingapparatoCognomeC()
    {
        return $this->shippingapparatoCognomeC;
    }

    /**
     * @param string $shippingapparatoCognomeC
     * @return ContrattiCstm
     */
    public function setShippingapparatoCognomeC($shippingapparatoCognomeC)
    {
        $this->shippingapparatoCognomeC = $shippingapparatoCognomeC;
        return $this;
    }

    /**
     * @return string
     */
    public function getShippingapparatoNomeC()
    {
        return $this->shippingapparatoNomeC;
    }

    /**
     * @param string $shippingapparatoNomeC
     * @return ContrattiCstm
     */
    public function setShippingapparatoNomeC($shippingapparatoNomeC)
    {
        $this->shippingapparatoNomeC = $shippingapparatoNomeC;
        return $this;
    }

    /**
     * @return string
     */
    public function getShippingapparatoIndirizzoC()
    {
        return $this->shippingapparatoIndirizzoC;
    }

    /**
     * @param string $shippingapparatoIndirizzoC
     * @return ContrattiCstm
     */
    public function setShippingapparatoIndirizzoC($shippingapparatoIndirizzoC)
    {
        $this->shippingapparatoIndirizzoC = $shippingapparatoIndirizzoC;
        return $this;
    }

    /**
     * @return string
     */
    public function getShippingapparatoLocalitaC()
    {
        return $this->shippingapparatoLocalitaC;
    }

    /**
     * @param string $shippingapparatoLocalitaC
     * @return ContrattiCstm
     */
    public function setShippingapparatoLocalitaC($shippingapparatoLocalitaC)
    {
        $this->shippingapparatoLocalitaC = $shippingapparatoLocalitaC;
        return $this;
    }

    /**
     * @return string
     */
    public function getShippingapparatoCittaC()
    {
        return $this->shippingapparatoCittaC;
    }

    /**
     * @param string $shippingapparatoCittaC
     * @return ContrattiCstm
     */
    public function setShippingapparatoCittaC($shippingapparatoCittaC)
    {
        $this->shippingapparatoCittaC = $shippingapparatoCittaC;
        return $this;
    }

    /**
     * @return string
     */
    public function getShippingapparatoProvinciaC()
    {
        return $this->shippingapparatoProvinciaC;
    }

    /**
     * @param string $shippingapparatoProvinciaC
     * @return ContrattiCstm
     */
    public function setShippingapparatoProvinciaC($shippingapparatoProvinciaC)
    {
        $this->shippingapparatoProvinciaC = $shippingapparatoProvinciaC;
        return $this;
    }

    /**
     * @return string
     */
    public function getShippingapparatoCapC()
    {
        return $this->shippingapparatoCapC;
    }

    /**
     * @param string $shippingapparatoCapC
     * @return ContrattiCstm
     */
    public function setShippingapparatoCapC($shippingapparatoCapC)
    {
        $this->shippingapparatoCapC = $shippingapparatoCapC;
        return $this;
    }

    /**
     * @return string
     */
    public function getRidSottoscrittoreLocalitaC()
    {
        return $this->ridSottoscrittoreLocalitaC;
    }

    /**
     * @param string $ridSottoscrittoreLocalitaC
     * @return ContrattiCstm
     */
    public function setRidSottoscrittoreLocalitaC($ridSottoscrittoreLocalitaC)
    {
        $this->ridSottoscrittoreLocalitaC = $ridSottoscrittoreLocalitaC;
        return $this;
    }

    /**
     * @return string
     */
    public function getUbicazionefrazioneC()
    {
        return $this->ubicazionefrazioneC;
    }

    /**
     * @param string $ubicazionefrazioneC
     * @return ContrattiCstm
     */
    public function setUbicazionefrazioneC($ubicazionefrazioneC)
    {
        $this->ubicazionefrazioneC = $ubicazionefrazioneC;
        return $this;
    }

    /**
     * @return string
     */
    public function getIdinstallatoreC()
    {
        return $this->idinstallatoreC;
    }

    /**
     * @param string $idinstallatoreC
     * @return ContrattiCstm
     */
    public function setIdinstallatoreC($idinstallatoreC)
    {
        $this->idinstallatoreC = $idinstallatoreC;
        return $this;
    }

    /**
     * @return string
     */
    public function getUsernameinstallatoreC()
    {
        return $this->usernameinstallatoreC;
    }

    /**
     * @param string $usernameinstallatoreC
     * @return ContrattiCstm
     */
    public function setUsernameinstallatoreC($usernameinstallatoreC)
    {
        $this->usernameinstallatoreC = $usernameinstallatoreC;
        return $this;
    }

    /**
     * @return string
     */
    public function getEsitoinstallazioneC()
    {
        return $this->esitoinstallazioneC;
    }

    /**
     * @param string $esitoinstallazioneC
     * @return ContrattiCstm
     */
    public function setEsitoinstallazioneC($esitoinstallazioneC)
    {
        $this->esitoinstallazioneC = $esitoinstallazioneC;
        return $this;
    }

    /**
     * @return string
     */
    public function getEsitospedizioneC()
    {
        return $this->esitospedizioneC;
    }

    /**
     * @param string $esitospedizioneC
     * @return ContrattiCstm
     */
    public function setEsitospedizioneC($esitospedizioneC)
    {
        $this->esitospedizioneC = $esitospedizioneC;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDataconsegnaC()
    {
        return $this->dataconsegnaC;
    }

    /**
     * @param \DateTime $dataconsegnaC
     * @return ContrattiCstm
     */
    public function setDataconsegnaC($dataconsegnaC)
    {
        $this->dataconsegnaC = $dataconsegnaC;
        return $this;
    }

    /**
     * @return string
     */
    public function getNoteesitospedizioneC()
    {
        return $this->noteesitospedizioneC;
    }

    /**
     * @param string $noteesitospedizioneC
     * @return ContrattiCstm
     */
    public function setNoteesitospedizioneC($noteesitospedizioneC)
    {
        $this->noteesitospedizioneC = $noteesitospedizioneC;
        return $this;
    }

    /**
     * @return bool
     */
    public function isUbicazionebypasscoperturaC()
    {
        return $this->ubicazionebypasscoperturaC;
    }

    /**
     * @param bool $ubicazionebypasscoperturaC
     * @return ContrattiCstm
     */
    public function setUbicazionebypasscoperturaC($ubicazionebypasscoperturaC)
    {
        $this->ubicazionebypasscoperturaC = $ubicazionebypasscoperturaC;
        return $this;
    }

    /**
     * @return string
     */
    public function getUbicazionelatgmC()
    {
        return $this->ubicazionelatgmC;
    }

    /**
     * @param string $ubicazionelatgmC
     * @return ContrattiCstm
     */
    public function setUbicazionelatgmC($ubicazionelatgmC)
    {
        $this->ubicazionelatgmC = $ubicazionelatgmC;
        return $this;
    }

    /**
     * @return string
     */
    public function getUbicazionelongmC()
    {
        return $this->ubicazionelongmC;
    }

    /**
     * @param string $ubicazionelongmC
     * @return ContrattiCstm
     */
    public function setUbicazionelongmC($ubicazionelongmC)
    {
        $this->ubicazionelongmC = $ubicazionelongmC;
        return $this;
    }

    /**
     * @return string
     */
    public function getUbicazionelatdbC()
    {
        return $this->ubicazionelatdbC;
    }

    /**
     * @param string $ubicazionelatdbC
     * @return ContrattiCstm
     */
    public function setUbicazionelatdbC($ubicazionelatdbC)
    {
        $this->ubicazionelatdbC = $ubicazionelatdbC;
        return $this;
    }

    /**
     * @return string
     */
    public function getUbicazionelondbC()
    {
        return $this->ubicazionelondbC;
    }

    /**
     * @param string $ubicazionelondbC
     * @return ContrattiCstm
     */
    public function setUbicazionelondbC($ubicazionelondbC)
    {
        $this->ubicazionelondbC = $ubicazionelondbC;
        return $this;
    }

    /**
     * @return string
     */
    public function getIduserconsegnaC()
    {
        return $this->iduserconsegnaC;
    }

    /**
     * @param string $iduserconsegnaC
     * @return ContrattiCstm
     */
    public function setIduserconsegnaC($iduserconsegnaC)
    {
        $this->iduserconsegnaC = $iduserconsegnaC;
        return $this;
    }

    /**
     * @return string
     */
    public function getUsernameconsegnaC()
    {
        return $this->usernameconsegnaC;
    }

    /**
     * @param string $usernameconsegnaC
     * @return ContrattiCstm
     */
    public function setUsernameconsegnaC($usernameconsegnaC)
    {
        $this->usernameconsegnaC = $usernameconsegnaC;
        return $this;
    }

    /**
     * @return string
     */
    public function getIdOffertawebC()
    {
        return $this->idOffertawebC;
    }

    /**
     * @param string $idOffertawebC
     * @return ContrattiCstm
     */
    public function setIdOffertawebC($idOffertawebC)
    {
        $this->idOffertawebC = $idOffertawebC;
        return $this;
    }

    /**
     * @return string
     */
    public function getStatoricezionecontrattoC()
    {
        return $this->statoricezionecontrattoC;
    }

    /**
     * @param string $statoricezionecontrattoC
     * @return ContrattiCstm
     */
    public function setStatoricezionecontrattoC($statoricezionecontrattoC)
    {
        $this->statoricezionecontrattoC = $statoricezionecontrattoC;
        return $this;
    }

    /**
     * @return string
     */
    public function getNrddtC()
    {
        return $this->nrddtC;
    }

    /**
     * @param string $nrddtC
     * @return ContrattiCstm
     */
    public function setNrddtC($nrddtC)
    {
        $this->nrddtC = $nrddtC;
        return $this;
    }

    /**
     * @return string
     */
    public function getNotedisdettaC()
    {
        return $this->notedisdettaC;
    }

    /**
     * @param string $notedisdettaC
     * @return ContrattiCstm
     */
    public function setNotedisdettaC($notedisdettaC)
    {
        $this->notedisdettaC = $notedisdettaC;
        return $this;
    }

    /**
     * @return string
     */
    public function getIduserriconsegnaC()
    {
        return $this->iduserriconsegnaC;
    }

    /**
     * @param string $iduserriconsegnaC
     * @return ContrattiCstm
     */
    public function setIduserriconsegnaC($iduserriconsegnaC)
    {
        $this->iduserriconsegnaC = $iduserriconsegnaC;
        return $this;
    }

    /**
     * @return string
     */
    public function getTipofatturaC()
    {
        return $this->tipofatturaC;
    }

    /**
     * @param string $tipofatturaC
     * @return ContrattiCstm
     */
    public function setTipofatturaC($tipofatturaC)
    {
        $this->tipofatturaC = $tipofatturaC;
        return $this;
    }

    /**
     * @return string
     */
    public function getUltimaperiodicitafattC()
    {
        return $this->ultimaperiodicitafattC;
    }

    /**
     * @param string $ultimaperiodicitafattC
     * @return ContrattiCstm
     */
    public function setUltimaperiodicitafattC($ultimaperiodicitafattC)
    {
        $this->ultimaperiodicitafattC = $ultimaperiodicitafattC;
        return $this;
    }

    /**
     * @return string
     */
    public function getCodiceridC()
    {
        return $this->codiceridC;
    }

    /**
     * @param string $codiceridC
     * @return ContrattiCstm
     */
    public function setCodiceridC($codiceridC)
    {
        $this->codiceridC = $codiceridC;
        return $this;
    }

    /**
     * @return string
     */
    public function getTipocodiceridC()
    {
        return $this->tipocodiceridC;
    }

    /**
     * @param string $tipocodiceridC
     * @return ContrattiCstm
     */
    public function setTipocodiceridC($tipocodiceridC)
    {
        $this->tipocodiceridC = $tipocodiceridC;
        return $this;
    }

    /**
     * @return int
     */
    public function getPercentualeProvvigioneC()
    {
        return $this->percentualeProvvigioneC;
    }

    /**
     * @param int $percentualeProvvigioneC
     * @return ContrattiCstm
     */
    public function setPercentualeProvvigioneC($percentualeProvvigioneC)
    {
        $this->percentualeProvvigioneC = $percentualeProvvigioneC;
        return $this;
    }

    /**
     * @return string
     */
    public function getActionaeaC()
    {
        return $this->actionaeaC;
    }

    /**
     * @param string $actionaeaC
     * @return ContrattiCstm
     */
    public function setActionaeaC($actionaeaC)
    {
        $this->actionaeaC = $actionaeaC;
        return $this;
    }

    /**
     * @return string
     */
    public function getMotivodisdettaC()
    {
        return $this->motivodisdettaC;
    }

    /**
     * @param string $motivodisdettaC
     * @return ContrattiCstm
     */
    public function setMotivodisdettaC($motivodisdettaC)
    {
        $this->motivodisdettaC = $motivodisdettaC;
        return $this;
    }

    /**
     * @return bool
     */
    public function isContabilizzatoC()
    {
        return $this->contabilizzatoC;
    }

    /**
     * @param bool $contabilizzatoC
     * @return ContrattiCstm
     */
    public function setContabilizzatoC($contabilizzatoC)
    {
        $this->contabilizzatoC = $contabilizzatoC;
        return $this;
    }

    /**
     * @return bool
     */
    public function isCpeattivaC()
    {
        return $this->cpeattivaC;
    }

    /**
     * @param bool $cpeattivaC
     * @return ContrattiCstm
     */
    public function setCpeattivaC($cpeattivaC)
    {
        $this->cpeattivaC = $cpeattivaC;
        return $this;
    }

    /**
     * @return string
     */
    public function getNumerovoucherC()
    {
        return $this->numerovoucherC;
    }

    /**
     * @param string $numerovoucherC
     * @return ContrattiCstm
     */
    public function setNumerovoucherC($numerovoucherC)
    {
        $this->numerovoucherC = $numerovoucherC;
        return $this;
    }

    /**
     * @return bool
     */
    public function isMigrazioneC()
    {
        return $this->migrazioneC;
    }

    /**
     * @param bool $migrazioneC
     * @return ContrattiCstm
     */
    public function setMigrazioneC($migrazioneC)
    {
        $this->migrazioneC = $migrazioneC;
        return $this;
    }

    /**
     * @return string
     */
    public function getStatodisdettaC()
    {
        return $this->statodisdettaC;
    }

    /**
     * @param string $statodisdettaC
     * @return ContrattiCstm
     */
    public function setStatodisdettaC($statodisdettaC)
    {
        $this->statodisdettaC = $statodisdettaC;
        return $this;
    }

    /**
     * @return bool
     */
    public function isEsclusioneinvitiC()
    {
        return $this->esclusioneinvitiC;
    }

    /**
     * @param bool $esclusioneinvitiC
     * @return ContrattiCstm
     */
    public function setEsclusioneinvitiC($esclusioneinvitiC)
    {
        $this->esclusioneinvitiC = $esclusioneinvitiC;
        return $this;
    }

    /**
     * @return string
     */
    public function getIdbuildingC()
    {
        return $this->idbuildingC;
    }

    /**
     * @param string $idbuildingC
     * @return ContrattiCstm
     */
    public function setIdbuildingC($idbuildingC)
    {
        $this->idbuildingC = $idbuildingC;
        return $this;
    }

    /**
     * @return string
     */
    public function getUbicazionetelefonoCC()
    {
        return $this->ubicazionetelefonoCC;
    }

    /**
     * @param string $ubicazionetelefonoCC
     * @return ContrattiCstm
     */
    public function setUbicazionetelefonoCC($ubicazionetelefonoCC)
    {
        $this->ubicazionetelefonoCC = $ubicazionetelefonoCC;
        return $this;
    }

    /**
     * @return string
     */
    public function getUbicazionetelefonoC()
    {
        return $this->ubicazionetelefonoC;
    }

    /**
     * @param string $ubicazionetelefonoC
     * @return ContrattiCstm
     */
    public function setUbicazionetelefonoC($ubicazionetelefonoC)
    {
        $this->ubicazionetelefonoC = $ubicazionetelefonoC;
        return $this;
    }

    /**
     * @return string
     */
    public function getBsNameC()
    {
        return $this->bsNameC;
    }

    /**
     * @param string $bsNameC
     * @return ContrattiCstm
     */
    public function setBsNameC($bsNameC)
    {
        $this->bsNameC = $bsNameC;
        return $this;
    }

    /**
     * @return int
     */
    public function getSettoreIdC()
    {
        return $this->settoreIdC;
    }

    /**
     * @param int $settoreIdC
     * @return ContrattiCstm
     */
    public function setSettoreIdC($settoreIdC)
    {
        $this->settoreIdC = $settoreIdC;
        return $this;
    }

    /**
     * @return int
     */
    public function getBsIntIdC()
    {
        return $this->bsIntIdC;
    }

    /**
     * @param int $bsIntIdC
     * @return ContrattiCstm
     */
    public function setBsIntIdC($bsIntIdC)
    {
        $this->bsIntIdC = $bsIntIdC;
        return $this;
    }

    /**
     * @return int
     */
    public function getEofC()
    {
        return $this->eofC;
    }

    /**
     * @param int $eofC
     * @return ContrattiCstm
     */
    public function setEofC($eofC)
    {
        $this->eofC = $eofC;
        return $this;
    }

    /**
     * @return string
     */
    public function getProfiloVoipC()
    {
        return $this->profiloVoipC;
    }

    /**
     * @param string $profiloVoipC
     * @return ContrattiCstm
     */
    public function setProfiloVoipC($profiloVoipC)
    {
        $this->profiloVoipC = $profiloVoipC;
        return $this;
    }

    /**
     * @return string
     */
    public function getNumeroappoggioC()
    {
        return $this->numeroappoggioC;
    }

    /**
     * @param string $numeroappoggioC
     * @return ContrattiCstm
     */
    public function setNumeroappoggioC($numeroappoggioC)
    {
        $this->numeroappoggioC = $numeroappoggioC;
        return $this;
    }

    /**
     * @return string
     */
    public function getGponDiAttestazioneC()
    {
        return $this->GponDiAttestazioneC;
    }

    /**
     * @param string $GponDiAttestazioneC
     * @return ContrattiCstm
     */
    public function setGponDiAttestazioneC($GponDiAttestazioneC)
    {
        $this->GponDiAttestazioneC = $GponDiAttestazioneC;
        return $this;
    }

    /**
     * @return string
     */
    public function getIdSplitterSecondarioC()
    {
        return $this->IdSplitterSecondarioC;
    }

    /**
     * @param string $IdSplitterSecondarioC
     * @return ContrattiCstm
     */
    public function setIdSplitterSecondarioC($IdSplitterSecondarioC)
    {
        $this->IdSplitterSecondarioC = $IdSplitterSecondarioC;
        return $this;
    }

    /**
     * @return string
     */
    public function getPosizioneSplitterSecondarioC()
    {
        return $this->PosizioneSplitterSecondarioC;
    }

    /**
     * @param string $PosizioneSplitterSecondarioC
     * @return ContrattiCstm
     */
    public function setPosizioneSplitterSecondarioC($PosizioneSplitterSecondarioC)
    {
        $this->PosizioneSplitterSecondarioC = $PosizioneSplitterSecondarioC;
        return $this;
    }

    /**
     * @return string
     */
    public function getParametriTrasmissiviOtticiC()
    {
        return $this->ParametriTrasmissiviOtticiC;
    }

    /**
     * @param string $ParametriTrasmissiviOtticiC
     * @return ContrattiCstm
     */
    public function setParametriTrasmissiviOtticiC($ParametriTrasmissiviOtticiC)
    {
        $this->ParametriTrasmissiviOtticiC = $ParametriTrasmissiviOtticiC;
        return $this;
    }

    /**
     * @return string
     */
    public function getFrTipoComC()
    {
        return $this->frTipoComC;
    }

    /**
     * @param string $frTipoComC
     * @return ContrattiCstm
     */
    public function setFrTipoComC($frTipoComC)
    {
        $this->frTipoComC = $frTipoComC;
        return $this;
    }

    /**
     * @return bool
     */
    public function isCoverageTypeC()
    {
        return $this->coverageTypeC;
    }

    /**
     * @param bool $coverageTypeC
     * @return ContrattiCstm
     */
    public function setCoverageTypeC($coverageTypeC)
    {
        $this->coverageTypeC = $coverageTypeC;
        return $this;
    }

    /**
     * @return bool
     */
    public function isCoverageForcedC()
    {
        return $this->coverageForcedC;
    }

    /**
     * @param bool $coverageForcedC
     * @return ContrattiCstm
     */
    public function setCoverageForcedC($coverageForcedC)
    {
        $this->coverageForcedC = $coverageForcedC;
        return $this;
    }

    /**
     * @return string
     */
    public function getIdcntC()
    {
        return $this->idcntC;
    }

    /**
     * @param string $idcntC
     * @return ContrattiCstm
     */
    public function setIdcntC($idcntC)
    {
        $this->idcntC = $idcntC;
        return $this;
    }

    /**
     * @return string
     */
    public function getTicketfibracostisuppC()
    {
        return $this->ticketfibracostisuppC;
    }

    /**
     * @param string $ticketfibracostisuppC
     * @return ContrattiCstm
     */
    public function setTicketfibracostisuppC($ticketfibracostisuppC)
    {
        $this->ticketfibracostisuppC = $ticketfibracostisuppC;
        return $this;
    }

    /**
     * @return string
     */
    public function getPinC()
    {
        return $this->pinC;
    }

    /**
     * @param string $pinC
     * @return ContrattiCstm
     */
    public function setPinC($pinC)
    {
        $this->pinC = $pinC;
        return $this;
    }

    /**
     * @return OfferteWeb
     */
    public function getOfferteWeb()
    {
        return $this->offerteWeb;
    }

    /**
     * @param OfferteWeb $offerteWeb
     * @return ContrattiCstm
     */
    public function setOfferteWeb($offerteWeb)
    {
        $this->offerteWeb = $offerteWeb;
        return $this;
    }

    /**
     * @return Contratti
     */
    public function getContratti()
    {
        return $this->contratti;
    }

    /**
     * @param Contratti $contratti
     * @return ContrattiCstm
     */
    public function setContratti($contratti)
    {
        $this->contratti = $contratti;
        return $this;
    }

    /**
     * @return Users
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * @param Users $users
     * @return ContrattiCstm
     */
    public function setUsers($users)
    {
        $this->users = $users;
        return $this;
    }





}

