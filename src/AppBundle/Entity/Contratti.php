<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Contratti
 *
 * @ORM\Table(name="contratti", uniqueConstraints={@ORM\UniqueConstraint(name="UK_contratti_id", columns={"id"})}, indexes={@ORM\Index(name="IDX_contratti", columns={"id", "date_entered"}), @ORM\Index(name="IDX_contratti_aui_del", columns={"assigned_user_id", "deleted"}), @ORM\Index(name="IDX_contratti_date_entered", columns={"date_entered"}), @ORM\Index(name="IDX_contratti2", columns={"deleted", "name"}), @ORM\Index(name="IDX_contratti3", columns={"id", "deleted"}), @ORM\Index(name="idx_created_by", columns={"created_by"}), @ORM\Index(name="idx_opp_name", columns={"name"}), @ORM\Index(name="IX_contratti", columns={"created_by", "id"}), @ORM\Index(name="IDX_contratti_assigned_user_id", columns={"assigned_user_id"})})
 * @ORM\Entity
 */
class Contratti
{
    /**
     * @var ContrattiDettaglio[]
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\ContrattiDettaglio", mappedBy="contratti", cascade={"persist"})
     */
    private $contrattiDettaglios;

    /**
     * @var FattureTestata
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\FattureTestata", mappedBy="contratti", cascade={"persist"})
     */
    private $fattureTestata;

    /**
     * @var Users
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Users", inversedBy="contratti")
     * @ORM\JoinColumn(name="assigned_user_id", referencedColumnName="id")
     */
    private $users;

    /**
     * @var AccountContratti
     *
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\AccountContratti", mappedBy="contratti", cascade={"persist"})
     */
    private $accountContratti;

    /**
     * @var ContrattiCstm
     *
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\ContrattiCstm", mappedBy="contratti", cascade={"persist"})
     */
    private $contrattiCstm;

    /**
     * @var string
     *
     * @ORM\Column(name="id", type="string", length=36, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id = '\'\'';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_entered", type="datetime", nullable=false)
     */
    private $dateEntered = '\'0000-00-00 00:00:00\'';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_modified", type="datetime", nullable=false)
     */
    private $dateModified = '\'0000-00-00 00:00:00\'';

    /**
     * @var string
     *
     * @ORM\Column(name="modified_user_id", type="string", length=36, nullable=true)
     */
    private $modifiedUserId = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="assigned_user_id", type="string", length=36, nullable=false)
     */
    private $assignedUserId;

    /**
     * @var string
     *
     * @ORM\Column(name="created_by", type="string", length=36, nullable=true)
     */
    private $createdBy = 'NULL';

    /**
     * @var boolean
     *
     * @ORM\Column(name="deleted", type="boolean", nullable=false)
     */
    private $deleted = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100, nullable=false)
     */
    private $name = '\'\'';

    /**
     * @var string
     *
     * @ORM\Column(name="opportunity_type", type="string", length=255, nullable=true)
     */
    private $opportunityType = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="lead_source", type="string", length=50, nullable=true)
     */
    private $leadSource = 'NULL';

    /**
     * @var float
     *
     * @ORM\Column(name="amount", type="float", precision=10, scale=0, nullable=true)
     */
    private $amount = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="amount_backup", type="string", length=25, nullable=true)
     */
    private $amountBackup = 'NULL';

    /**
     * @var float
     *
     * @ORM\Column(name="amount_usdollar", type="float", precision=10, scale=0, nullable=true)
     */
    private $amountUsdollar = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="currency_id", type="string", length=36, nullable=true)
     */
    private $currencyId = 'NULL';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_closed", type="date", nullable=true)
     */
    private $dateClosed = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="next_step", type="string", length=100, nullable=true)
     */
    private $nextStep = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="sales_stage", type="string", length=25, nullable=true)
     */
    private $salesStage = 'NULL';

    /**
     * @var float
     *
     * @ORM\Column(name="probability", type="float", precision=10, scale=0, nullable=true)
     */
    private $probability = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", length=65535, nullable=true)
     */
    private $description = 'NULL';

    /**
     * @return ContrattiCstm
     */
    public function getContrattiCstm()
    {
        return $this->contrattiCstm;
    }

    /**
     * @param ContrattiCstm $contrattiCstm
     * @return Contratti
     */
    public function setContrattiCstm($contrattiCstm)
    {
        $this->contrattiCstm = $contrattiCstm;
        return $this;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return Contratti
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateEntered()
    {
        return $this->dateEntered;
    }

    /**
     * @param \DateTime $dateEntered
     * @return Contratti
     */
    public function setDateEntered($dateEntered)
    {
        $this->dateEntered = $dateEntered;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateModified()
    {
        return $this->dateModified;
    }

    /**
     * @param \DateTime $dateModified
     * @return Contratti
     */
    public function setDateModified($dateModified)
    {
        $this->dateModified = $dateModified;
        return $this;
    }

    /**
     * @return string
     */
    public function getModifiedUserId()
    {
        return $this->modifiedUserId;
    }

    /**
     * @param string $modifiedUserId
     * @return Contratti
     */
    public function setModifiedUserId($modifiedUserId)
    {
        $this->modifiedUserId = $modifiedUserId;
        return $this;
    }

    /**
     * @return string
     */
    public function getAssignedUserId()
    {
        return $this->assignedUserId;
    }

    /**
     * @param string $assignedUserId
     * @return Contratti
     */
    public function setAssignedUserId($assignedUserId)
    {
        $this->assignedUserId = $assignedUserId;
        return $this;
    }

    /**
     * @return string
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * @param string $createdBy
     * @return Contratti
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;
        return $this;
    }

    /**
     * @return bool
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * @param bool $deleted
     * @return Contratti
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;
        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Contratti
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getOpportunityType()
    {
        return $this->opportunityType;
    }

    /**
     * @param string $opportunityType
     * @return Contratti
     */
    public function setOpportunityType($opportunityType)
    {
        $this->opportunityType = $opportunityType;
        return $this;
    }

    /**
     * @return string
     */
    public function getLeadSource()
    {
        return $this->leadSource;
    }

    /**
     * @param string $leadSource
     * @return Contratti
     */
    public function setLeadSource($leadSource)
    {
        $this->leadSource = $leadSource;
        return $this;
    }

    /**
     * @return float
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * @param float $amount
     * @return Contratti
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
        return $this;
    }

    /**
     * @return string
     */
    public function getAmountBackup()
    {
        return $this->amountBackup;
    }

    /**
     * @param string $amountBackup
     * @return Contratti
     */
    public function setAmountBackup($amountBackup)
    {
        $this->amountBackup = $amountBackup;
        return $this;
    }

    /**
     * @return float
     */
    public function getAmountUsdollar()
    {
        return $this->amountUsdollar;
    }

    /**
     * @param float $amountUsdollar
     * @return Contratti
     */
    public function setAmountUsdollar($amountUsdollar)
    {
        $this->amountUsdollar = $amountUsdollar;
        return $this;
    }

    /**
     * @return string
     */
    public function getCurrencyId()
    {
        return $this->currencyId;
    }

    /**
     * @param string $currencyId
     * @return Contratti
     */
    public function setCurrencyId($currencyId)
    {
        $this->currencyId = $currencyId;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateClosed()
    {
        return $this->dateClosed;
    }

    /**
     * @param \DateTime $dateClosed
     * @return Contratti
     */
    public function setDateClosed($dateClosed)
    {
        $this->dateClosed = $dateClosed;
        return $this;
    }

    /**
     * @return string
     */
    public function getNextStep()
    {
        return $this->nextStep;
    }

    /**
     * @param string $nextStep
     * @return Contratti
     */
    public function setNextStep($nextStep)
    {
        $this->nextStep = $nextStep;
        return $this;
    }

    /**
     * @return string
     */
    public function getSalesStage()
    {
        return $this->salesStage;
    }

    /**
     * @param string $salesStage
     * @return Contratti
     */
    public function setSalesStage($salesStage)
    {
        $this->salesStage = $salesStage;
        return $this;
    }

    /**
     * @return float
     */
    public function getProbability()
    {
        return $this->probability;
    }

    /**
     * @param float $probability
     * @return Contratti
     */
    public function setProbability($probability)
    {
        $this->probability = $probability;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return Contratti
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return AccountContratti
     */
    public function getAccountContratti()
    {
        return $this->accountContratti;
    }

    /**
     * @param AccountContratti $accountContratti
     * @return Contratti
     */
    public function setAccountContratti($accountContratti)
    {
        $this->accountContratti = $accountContratti;
        return $this;
    }

    /**
     * @return Users
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * @param Users $users
     * @return Contratti
     */
    public function setUsers($users)
    {
        $this->users = $users;
        return $this;
    }

    /**
     * @return FattureTestata
     */
    public function getFattureTestata()
    {
        return $this->fattureTestata;
    }

    /**
     * @param FattureTestata $fattureTestata
     * @return Contratti
     */
    public function setFattureTestata($fattureTestata)
    {
        $this->fattureTestata = $fattureTestata;
        return $this;
    }

    public function __toString()
    {
        return $this->id;
    }

    /**
     * @return ContrattiDettaglio[]
     */
    public function getContrattiDettaglios()
    {
        return $this->contrattiDettaglios;
    }

    /**
     * @param ContrattiDettaglio $contrattiDettaglio
     * @return Contratti
     */
    public function setContrattiDettaglios($contrattiDettaglios)
    {
        $this->contrattiDettaglios = $contrattiDettaglios;
        return $this;
    }


}

