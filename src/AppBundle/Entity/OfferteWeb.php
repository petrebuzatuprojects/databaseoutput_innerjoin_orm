<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * OfferteWeb
 *
 * @ORM\Table(name="offerte_web", uniqueConstraints={@ORM\UniqueConstraint(name="UK_Offerte_Web_numeroofferta", columns={"numeroofferta"})}, indexes={@ORM\Index(name="ID", columns={"ID"}), @ORM\Index(name="IDX_Offerte_Web", columns={"DataScadenzaValiditaOfferta", "FlagVisibilita", "TipoLinea", "TipoClienteDestinatario", "Azienda"})})
 * @ORM\Entity
 */
class OfferteWeb
{

   /*
    * @var OfferteWebDettaglio
    *
    * @ORM\OneToMany(targetEntity="AppBundle\Entity\OfferteWebDettaglio", mappedBy="offerteWeb", cascade={"persist"})
    */
    private $offerteWebDettaglio;

    /*
    * @var ContrattiCstm
    *
    * @ORM\OneToOne(targetEntity="AppBundle\Entity\ContrattiCstm", mappedBy="offerteWeb", cascade={"persist"})
    */
    private $contrattiCstm;

    /**
     * @var integer
     *
     * @ORM\Column(name="numeroofferta", type="integer", nullable=false)
     */
    private $numeroofferta;

    /**
     * @var string
     *
     * @ORM\Column(name="ID", type="string", length=36, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private $id = '\'\'';

    /**
     * @var string
     *
     * @ORM\Column(name="Descrizione", type="text", length=65535, nullable=true)
     */
    private $descrizione = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="ImmagineLista", type="string", length=255, nullable=true)
     */
    private $immaginelista = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="HtmlProdotto", type="string", length=255, nullable=true)
     */
    private $htmlprodotto = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="PrezzoOfferta", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $prezzoofferta = 'NULL';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DataScadenzaValiditaOfferta", type="date", nullable=true)
     */
    private $datascadenzavaliditaofferta = 'NULL';

    /**
     * @var integer
     *
     * @ORM\Column(name="TipoLinea", type="smallint", nullable=true)
     */
    private $tipolinea = 'NULL';

    /**
     * @var integer
     *
     * @ORM\Column(name="TipoOfferta", type="smallint", nullable=true)
     */
    private $tipoofferta = 'NULL';

    /**
     * @var integer
     *
     * @ORM\Column(name="TipoClienteDestinatario", type="smallint", nullable=true)
     */
    private $tipoclientedestinatario = 'NULL';

    /**
     * @var integer
     *
     * @ORM\Column(name="OffertaDefault", type="smallint", nullable=true)
     */
    private $offertadefault = 'NULL';

    /**
     * @var integer
     *
     * @ORM\Column(name="OffertaPromo", type="smallint", nullable=true)
     */
    private $offertapromo = 'NULL';

    /**
     * @var integer
     *
     * @ORM\Column(name="Punteggio", type="integer", nullable=true)
     */
    private $punteggio = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="MsgPromozionale", type="text", length=65535, nullable=true)
     */
    private $msgpromozionale = 'NULL';

    /**
     * @var integer
     *
     * @ORM\Column(name="FlagVisibilita", type="integer", nullable=true)
     */
    private $flagvisibilita = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="Titolo", type="string", length=80, nullable=true)
     */
    private $titolo = 'NULL';

    /**
     * @var boolean
     *
     * @ORM\Column(name="AbilitaOffertaZona", type="boolean", nullable=true)
     */
    private $abilitaoffertazona = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="AbilitiaOffertaUtente", type="boolean", nullable=true)
     */
    private $abilitiaoffertautente = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="HtmlOfferta", type="text", length=65535, nullable=true)
     */
    private $htmlofferta = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="HtmlOffertaRiepilogo", type="text", length=65535, nullable=true)
     */
    private $htmloffertariepilogo = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="Azienda", type="string", length=20, nullable=true)
     */
    private $azienda = '\'GO\'';

    /**
     * @var string
     *
     * @ORM\Column(name="htmlcopertura", type="string", length=3000, nullable=true)
     */
    private $htmlcopertura = 'NULL';

    /**
     * @var boolean
     *
     * @ORM\Column(name="tipocopertura", type="boolean", nullable=true)
     */
    private $tipocopertura = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="listino", type="string", length=50, nullable=true)
     */
    private $listino = 'NULL';

    /**
     * @var integer
     *
     * @ORM\Column(name="tipobusiness", type="integer", nullable=true)
     */
    private $tipobusiness = 'NULL';

    /**
     * @var boolean
     *
     * @ORM\Column(name="ricaricabile", type="boolean", nullable=true)
     */
    private $ricaricabile = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="tipologiaOfferta", type="integer", nullable=true)
     */
    private $tipologiaofferta = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="titoloReport", type="string", length=80, nullable=true)
     */
    private $titoloreport = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="z_name", type="string", length=80, nullable=true)
     */
    private $zName = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="z_sub_name", type="string", length=255, nullable=true)
     */
    private $zSubName = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="z_options", type="string", length=255, nullable=true)
     */
    private $zOptions = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="z_price", type="string", length=50, nullable=true)
     */
    private $zPrice = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="aff_name", type="string", length=80, nullable=true)
     */
    private $affName = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="aff_sub_name", type="string", length=255, nullable=true)
     */
    private $affSubName = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="aff_options", type="string", length=255, nullable=true)
     */
    private $affOptions = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="aff_price", type="string", length=50, nullable=true)
     */
    private $affPrice = 'NULL';

    /**
     * @return mixed
     */
    public function getContrattiCstm()
    {
        return $this->contrattiCstm;
    }

    /**
     * @param mixed $contrattiCstm
     * @return OfferteWeb
     */
    public function setContrattiCstm($contrattiCstm)
    {
        $this->contrattiCstm = $contrattiCstm;
        return $this;
    }

    /**
     * @return int
     */
    public function getNumeroofferta()
    {
        return $this->numeroofferta;
    }

    /**
     * @param int $numeroofferta
     * @return OfferteWeb
     */
    public function setNumeroofferta($numeroofferta)
    {
        $this->numeroofferta = $numeroofferta;
        return $this;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return OfferteWeb
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescrizione()
    {
        return $this->descrizione;
    }

    /**
     * @param string $descrizione
     * @return OfferteWeb
     */
    public function setDescrizione($descrizione)
    {
        $this->descrizione = $descrizione;
        return $this;
    }

    /**
     * @return string
     */
    public function getImmaginelista()
    {
        return $this->immaginelista;
    }

    /**
     * @param string $immaginelista
     * @return OfferteWeb
     */
    public function setImmaginelista($immaginelista)
    {
        $this->immaginelista = $immaginelista;
        return $this;
    }

    /**
     * @return string
     */
    public function getHtmlprodotto()
    {
        return $this->htmlprodotto;
    }

    /**
     * @param string $htmlprodotto
     * @return OfferteWeb
     */
    public function setHtmlprodotto($htmlprodotto)
    {
        $this->htmlprodotto = $htmlprodotto;
        return $this;
    }

    /**
     * @return string
     */
    public function getPrezzoofferta()
    {
        return $this->prezzoofferta;
    }

    /**
     * @param string $prezzoofferta
     * @return OfferteWeb
     */
    public function setPrezzoofferta($prezzoofferta)
    {
        $this->prezzoofferta = $prezzoofferta;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDatascadenzavaliditaofferta()
    {
        return $this->datascadenzavaliditaofferta;
    }

    /**
     * @param \DateTime $datascadenzavaliditaofferta
     * @return OfferteWeb
     */
    public function setDatascadenzavaliditaofferta($datascadenzavaliditaofferta)
    {
        $this->datascadenzavaliditaofferta = $datascadenzavaliditaofferta;
        return $this;
    }

    /**
     * @return int
     */
    public function getTipolinea()
    {
        return $this->tipolinea;
    }

    /**
     * @param int $tipolinea
     * @return OfferteWeb
     */
    public function setTipolinea($tipolinea)
    {
        $this->tipolinea = $tipolinea;
        return $this;
    }

    /**
     * @return int
     */
    public function getTipoofferta()
    {
        return $this->tipoofferta;
    }

    /**
     * @param int $tipoofferta
     * @return OfferteWeb
     */
    public function setTipoofferta($tipoofferta)
    {
        $this->tipoofferta = $tipoofferta;
        return $this;
    }

    /**
     * @return int
     */
    public function getTipoclientedestinatario()
    {
        return $this->tipoclientedestinatario;
    }

    /**
     * @param int $tipoclientedestinatario
     * @return OfferteWeb
     */
    public function setTipoclientedestinatario($tipoclientedestinatario)
    {
        $this->tipoclientedestinatario = $tipoclientedestinatario;
        return $this;
    }

    /**
     * @return int
     */
    public function getOffertadefault()
    {
        return $this->offertadefault;
    }

    /**
     * @param int $offertadefault
     * @return OfferteWeb
     */
    public function setOffertadefault($offertadefault)
    {
        $this->offertadefault = $offertadefault;
        return $this;
    }

    /**
     * @return int
     */
    public function getOffertapromo()
    {
        return $this->offertapromo;
    }

    /**
     * @param int $offertapromo
     * @return OfferteWeb
     */
    public function setOffertapromo($offertapromo)
    {
        $this->offertapromo = $offertapromo;
        return $this;
    }

    /**
     * @return int
     */
    public function getPunteggio()
    {
        return $this->punteggio;
    }

    /**
     * @param int $punteggio
     * @return OfferteWeb
     */
    public function setPunteggio($punteggio)
    {
        $this->punteggio = $punteggio;
        return $this;
    }

    /**
     * @return string
     */
    public function getMsgpromozionale()
    {
        return $this->msgpromozionale;
    }

    /**
     * @param string $msgpromozionale
     * @return OfferteWeb
     */
    public function setMsgpromozionale($msgpromozionale)
    {
        $this->msgpromozionale = $msgpromozionale;
        return $this;
    }

    /**
     * @return int
     */
    public function getFlagvisibilita()
    {
        return $this->flagvisibilita;
    }

    /**
     * @param int $flagvisibilita
     * @return OfferteWeb
     */
    public function setFlagvisibilita($flagvisibilita)
    {
        $this->flagvisibilita = $flagvisibilita;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitolo()
    {
        return $this->titolo;
    }

    /**
     * @param string $titolo
     * @return OfferteWeb
     */
    public function setTitolo($titolo)
    {
        $this->titolo = $titolo;
        return $this;
    }

    /**
     * @return bool
     */
    public function isAbilitaoffertazona()
    {
        return $this->abilitaoffertazona;
    }

    /**
     * @param bool $abilitaoffertazona
     * @return OfferteWeb
     */
    public function setAbilitaoffertazona($abilitaoffertazona)
    {
        $this->abilitaoffertazona = $abilitaoffertazona;
        return $this;
    }

    /**
     * @return bool
     */
    public function isAbilitiaoffertautente()
    {
        return $this->abilitiaoffertautente;
    }

    /**
     * @param bool $abilitiaoffertautente
     * @return OfferteWeb
     */
    public function setAbilitiaoffertautente($abilitiaoffertautente)
    {
        $this->abilitiaoffertautente = $abilitiaoffertautente;
        return $this;
    }

    /**
     * @return string
     */
    public function getHtmlofferta()
    {
        return $this->htmlofferta;
    }

    /**
     * @param string $htmlofferta
     * @return OfferteWeb
     */
    public function setHtmlofferta($htmlofferta)
    {
        $this->htmlofferta = $htmlofferta;
        return $this;
    }

    /**
     * @return string
     */
    public function getHtmloffertariepilogo()
    {
        return $this->htmloffertariepilogo;
    }

    /**
     * @param string $htmloffertariepilogo
     * @return OfferteWeb
     */
    public function setHtmloffertariepilogo($htmloffertariepilogo)
    {
        $this->htmloffertariepilogo = $htmloffertariepilogo;
        return $this;
    }

    /**
     * @return string
     */
    public function getAzienda()
    {
        return $this->azienda;
    }

    /**
     * @param string $azienda
     * @return OfferteWeb
     */
    public function setAzienda($azienda)
    {
        $this->azienda = $azienda;
        return $this;
    }

    /**
     * @return string
     */
    public function getHtmlcopertura()
    {
        return $this->htmlcopertura;
    }

    /**
     * @param string $htmlcopertura
     * @return OfferteWeb
     */
    public function setHtmlcopertura($htmlcopertura)
    {
        $this->htmlcopertura = $htmlcopertura;
        return $this;
    }

    /**
     * @return bool
     */
    public function isTipocopertura()
    {
        return $this->tipocopertura;
    }

    /**
     * @param bool $tipocopertura
     * @return OfferteWeb
     */
    public function setTipocopertura($tipocopertura)
    {
        $this->tipocopertura = $tipocopertura;
        return $this;
    }

    /**
     * @return string
     */
    public function getListino()
    {
        return $this->listino;
    }

    /**
     * @param string $listino
     * @return OfferteWeb
     */
    public function setListino($listino)
    {
        $this->listino = $listino;
        return $this;
    }

    /**
     * @return int
     */
    public function getTipobusiness()
    {
        return $this->tipobusiness;
    }

    /**
     * @param int $tipobusiness
     * @return OfferteWeb
     */
    public function setTipobusiness($tipobusiness)
    {
        $this->tipobusiness = $tipobusiness;
        return $this;
    }

    /**
     * @return bool
     */
    public function isRicaricabile()
    {
        return $this->ricaricabile;
    }

    /**
     * @param bool $ricaricabile
     * @return OfferteWeb
     */
    public function setRicaricabile($ricaricabile)
    {
        $this->ricaricabile = $ricaricabile;
        return $this;
    }

    /**
     * @return int
     */
    public function getTipologiaofferta()
    {
        return $this->tipologiaofferta;
    }

    /**
     * @param int $tipologiaofferta
     * @return OfferteWeb
     */
    public function setTipologiaofferta($tipologiaofferta)
    {
        $this->tipologiaofferta = $tipologiaofferta;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitoloreport()
    {
        return $this->titoloreport;
    }

    /**
     * @param string $titoloreport
     * @return OfferteWeb
     */
    public function setTitoloreport($titoloreport)
    {
        $this->titoloreport = $titoloreport;
        return $this;
    }

    /**
     * @return string
     */
    public function getZName()
    {
        return $this->zName;
    }

    /**
     * @param string $zName
     * @return OfferteWeb
     */
    public function setZName($zName)
    {
        $this->zName = $zName;
        return $this;
    }

    /**
     * @return string
     */
    public function getZSubName()
    {
        return $this->zSubName;
    }

    /**
     * @param string $zSubName
     * @return OfferteWeb
     */
    public function setZSubName($zSubName)
    {
        $this->zSubName = $zSubName;
        return $this;
    }

    /**
     * @return string
     */
    public function getZOptions()
    {
        return $this->zOptions;
    }

    /**
     * @param string $zOptions
     * @return OfferteWeb
     */
    public function setZOptions($zOptions)
    {
        $this->zOptions = $zOptions;
        return $this;
    }

    /**
     * @return string
     */
    public function getZPrice()
    {
        return $this->zPrice;
    }

    /**
     * @param string $zPrice
     * @return OfferteWeb
     */
    public function setZPrice($zPrice)
    {
        $this->zPrice = $zPrice;
        return $this;
    }

    /**
     * @return string
     */
    public function getAffName()
    {
        return $this->affName;
    }

    /**
     * @param string $affName
     * @return OfferteWeb
     */
    public function setAffName($affName)
    {
        $this->affName = $affName;
        return $this;
    }

    /**
     * @return string
     */
    public function getAffSubName()
    {
        return $this->affSubName;
    }

    /**
     * @param string $affSubName
     * @return OfferteWeb
     */
    public function setAffSubName($affSubName)
    {
        $this->affSubName = $affSubName;
        return $this;
    }

    /**
     * @return string
     */
    public function getAffOptions()
    {
        return $this->affOptions;
    }

    /**
     * @param string $affOptions
     * @return OfferteWeb
     */
    public function setAffOptions($affOptions)
    {
        $this->affOptions = $affOptions;
        return $this;
    }

    /**
     * @return string
     */
    public function getAffPrice()
    {
        return $this->affPrice;
    }

    /**
     * @param string $affPrice
     * @return OfferteWeb
     */
    public function setAffPrice($affPrice)
    {
        $this->affPrice = $affPrice;
        return $this;
    }

    public function __toString()
    {
        return $this ->id;
    }

    /**
     * @return mixed
     */
    public function getOfferteWebDettaglio()
    {
        return $this->offerteWebDettaglio;
    }

    /**
     * @param mixed $offerteWebDettaglio
     * @return OfferteWeb
     */
    public function setOfferteWebDettaglio($offerteWebDettaglio)
    {
        $this->offerteWebDettaglio = $offerteWebDettaglio;
        return $this;
    }



}

