<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Users
 *
 * @ORM\Table(name="users", indexes={@ORM\Index(name="id", columns={"id"}), @ORM\Index(name="report_to", columns={"reports_to_id"}), @ORM\Index(name="user_name", columns={"user_name"}), @ORM\Index(name="user_password", columns={"user_password"})})
 * @ORM\Entity
 */
class Users
{


    /**
     * @var ContrattiCstm
     *
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\ContrattiCstm", mappedBy="users", cascade={"persist"})
     */
    private $contrattiCstm;


    /**
     * @var Contratti
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Contratti", mappedBy="users", cascade={"persist"})
     */
    private $contratti;


    /**
     * @var string
     *
     * @ORM\Column(name="id", type="string", length=36, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id = '\'\'';

    /**
     * @var string
     *
     * @ORM\Column(name="user_name", type="string", length=20, nullable=true)
     */
    private $userName = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="user_password", type="string", length=30, nullable=true)
     */
    private $userPassword = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="user_hash", type="string", length=32, nullable=true)
     */
    private $userHash = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="first_name", type="string", length=30, nullable=true)
     */
    private $firstName = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="last_name", type="string", length=30, nullable=true)
     */
    private $lastName = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="reports_to_id", type="string", length=36, nullable=true)
     */
    private $reportsToId = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="is_admin", type="string", length=3, nullable=true)
     */
    private $isAdmin = '\'0\'';

    /**
     * @var string
     *
     * @ORM\Column(name="receive_notifications", type="string", length=1, nullable=true)
     */
    private $receiveNotifications = '\'1\'';

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", length=65535, nullable=true)
     */
    private $description = 'NULL';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_entered", type="datetime", nullable=false)
     */
    private $dateEntered = '\'0000-00-00 00:00:00\'';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_modified", type="datetime", nullable=false)
     */
    private $dateModified = '\'0000-00-00 00:00:00\'';

    /**
     * @var string
     *
     * @ORM\Column(name="modified_user_id", type="string", length=36, nullable=true)
     */
    private $modifiedUserId = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="created_by", type="string", length=36, nullable=true)
     */
    private $createdBy = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=50, nullable=true)
     */
    private $title = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="department", type="string", length=50, nullable=true)
     */
    private $department = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="phone_home", type="string", length=50, nullable=true)
     */
    private $phoneHome = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="phone_mobile", type="string", length=50, nullable=true)
     */
    private $phoneMobile = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="phone_work", type="string", length=50, nullable=true)
     */
    private $phoneWork = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="phone_other", type="string", length=50, nullable=true)
     */
    private $phoneOther = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="phone_fax", type="string", length=50, nullable=true)
     */
    private $phoneFax = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="email1", type="string", length=100, nullable=true)
     */
    private $email1 = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="email2", type="string", length=100, nullable=true)
     */
    private $email2 = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=25, nullable=true)
     */
    private $status = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="address_street", type="string", length=150, nullable=true)
     */
    private $addressStreet = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="address_city", type="string", length=100, nullable=true)
     */
    private $addressCity = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="address_state", type="string", length=100, nullable=true)
     */
    private $addressState = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="address_country", type="string", length=25, nullable=true)
     */
    private $addressCountry = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="address_postalcode", type="string", length=9, nullable=true)
     */
    private $addressPostalcode = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="user_preferences", type="text", length=65535, nullable=true)
     */
    private $userPreferences = 'NULL';

    /**
     * @var boolean
     *
     * @ORM\Column(name="deleted", type="boolean", nullable=false)
     */
    private $deleted = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="portal_only", type="boolean", nullable=true)
     */
    private $portalOnly = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="employee_status", type="string", length=25, nullable=true)
     */
    private $employeeStatus = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="messenger_id", type="string", length=25, nullable=true)
     */
    private $messengerId = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="messenger_type", type="string", length=25, nullable=true)
     */
    private $messengerType = 'NULL';

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_group", type="boolean", nullable=true)
     */
    private $isGroup = '0';

    /**
     * @return Contratti
     */
    public function getContratti()
    {
        return $this->contratti;
    }

    /**
     * @param Contratti $contratti
     * @return Users
     */
    public function setContratti($contratti)
    {
        $this->contratti = $contratti;
        return $this;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return Users
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getUserName()
    {
        return $this->userName;
    }

    /**
     * @param string $userName
     * @return Users
     */
    public function setUserName($userName)
    {
        $this->userName = $userName;
        return $this;
    }

    /**
     * @return string
     */
    public function getUserPassword()
    {
        return $this->userPassword;
    }

    /**
     * @param string $userPassword
     * @return Users
     */
    public function setUserPassword($userPassword)
    {
        $this->userPassword = $userPassword;
        return $this;
    }

    /**
     * @return string
     */
    public function getUserHash()
    {
        return $this->userHash;
    }

    /**
     * @param string $userHash
     * @return Users
     */
    public function setUserHash($userHash)
    {
        $this->userHash = $userHash;
        return $this;
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     * @return Users
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
        return $this;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     * @return Users
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
        return $this;
    }

    /**
     * @return string
     */
    public function getReportsToId()
    {
        return $this->reportsToId;
    }

    /**
     * @param string $reportsToId
     * @return Users
     */
    public function setReportsToId($reportsToId)
    {
        $this->reportsToId = $reportsToId;
        return $this;
    }

    /**
     * @return string
     */
    public function getIsAdmin()
    {
        return $this->isAdmin;
    }

    /**
     * @param string $isAdmin
     * @return Users
     */
    public function setIsAdmin($isAdmin)
    {
        $this->isAdmin = $isAdmin;
        return $this;
    }

    /**
     * @return string
     */
    public function getReceiveNotifications()
    {
        return $this->receiveNotifications;
    }

    /**
     * @param string $receiveNotifications
     * @return Users
     */
    public function setReceiveNotifications($receiveNotifications)
    {
        $this->receiveNotifications = $receiveNotifications;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return Users
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateEntered()
    {
        return $this->dateEntered;
    }

    /**
     * @param \DateTime $dateEntered
     * @return Users
     */
    public function setDateEntered($dateEntered)
    {
        $this->dateEntered = $dateEntered;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateModified()
    {
        return $this->dateModified;
    }

    /**
     * @param \DateTime $dateModified
     * @return Users
     */
    public function setDateModified($dateModified)
    {
        $this->dateModified = $dateModified;
        return $this;
    }

    /**
     * @return string
     */
    public function getModifiedUserId()
    {
        return $this->modifiedUserId;
    }

    /**
     * @param string $modifiedUserId
     * @return Users
     */
    public function setModifiedUserId($modifiedUserId)
    {
        $this->modifiedUserId = $modifiedUserId;
        return $this;
    }

    /**
     * @return string
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * @param string $createdBy
     * @return Users
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     * @return Users
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string
     */
    public function getDepartment()
    {
        return $this->department;
    }

    /**
     * @param string $department
     * @return Users
     */
    public function setDepartment($department)
    {
        $this->department = $department;
        return $this;
    }

    /**
     * @return string
     */
    public function getPhoneHome()
    {
        return $this->phoneHome;
    }

    /**
     * @param string $phoneHome
     * @return Users
     */
    public function setPhoneHome($phoneHome)
    {
        $this->phoneHome = $phoneHome;
        return $this;
    }

    /**
     * @return string
     */
    public function getPhoneMobile()
    {
        return $this->phoneMobile;
    }

    /**
     * @param string $phoneMobile
     * @return Users
     */
    public function setPhoneMobile($phoneMobile)
    {
        $this->phoneMobile = $phoneMobile;
        return $this;
    }

    /**
     * @return string
     */
    public function getPhoneWork()
    {
        return $this->phoneWork;
    }

    /**
     * @param string $phoneWork
     * @return Users
     */
    public function setPhoneWork($phoneWork)
    {
        $this->phoneWork = $phoneWork;
        return $this;
    }

    /**
     * @return string
     */
    public function getPhoneOther()
    {
        return $this->phoneOther;
    }

    /**
     * @param string $phoneOther
     * @return Users
     */
    public function setPhoneOther($phoneOther)
    {
        $this->phoneOther = $phoneOther;
        return $this;
    }

    /**
     * @return string
     */
    public function getPhoneFax()
    {
        return $this->phoneFax;
    }

    /**
     * @param string $phoneFax
     * @return Users
     */
    public function setPhoneFax($phoneFax)
    {
        $this->phoneFax = $phoneFax;
        return $this;
    }

    /**
     * @return string
     */
    public function getEmail1()
    {
        return $this->email1;
    }

    /**
     * @param string $email1
     * @return Users
     */
    public function setEmail1($email1)
    {
        $this->email1 = $email1;
        return $this;
    }

    /**
     * @return string
     */
    public function getEmail2()
    {
        return $this->email2;
    }

    /**
     * @param string $email2
     * @return Users
     */
    public function setEmail2($email2)
    {
        $this->email2 = $email2;
        return $this;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     * @return Users
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return string
     */
    public function getAddressStreet()
    {
        return $this->addressStreet;
    }

    /**
     * @param string $addressStreet
     * @return Users
     */
    public function setAddressStreet($addressStreet)
    {
        $this->addressStreet = $addressStreet;
        return $this;
    }

    /**
     * @return string
     */
    public function getAddressCity()
    {
        return $this->addressCity;
    }

    /**
     * @param string $addressCity
     * @return Users
     */
    public function setAddressCity($addressCity)
    {
        $this->addressCity = $addressCity;
        return $this;
    }

    /**
     * @return string
     */
    public function getAddressState()
    {
        return $this->addressState;
    }

    /**
     * @param string $addressState
     * @return Users
     */
    public function setAddressState($addressState)
    {
        $this->addressState = $addressState;
        return $this;
    }

    /**
     * @return string
     */
    public function getAddressCountry()
    {
        return $this->addressCountry;
    }

    /**
     * @param string $addressCountry
     * @return Users
     */
    public function setAddressCountry($addressCountry)
    {
        $this->addressCountry = $addressCountry;
        return $this;
    }

    /**
     * @return string
     */
    public function getAddressPostalcode()
    {
        return $this->addressPostalcode;
    }

    /**
     * @param string $addressPostalcode
     * @return Users
     */
    public function setAddressPostalcode($addressPostalcode)
    {
        $this->addressPostalcode = $addressPostalcode;
        return $this;
    }

    /**
     * @return string
     */
    public function getUserPreferences()
    {
        return $this->userPreferences;
    }

    /**
     * @param string $userPreferences
     * @return Users
     */
    public function setUserPreferences($userPreferences)
    {
        $this->userPreferences = $userPreferences;
        return $this;
    }

    /**
     * @return bool
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * @param bool $deleted
     * @return Users
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;
        return $this;
    }

    /**
     * @return bool
     */
    public function isPortalOnly()
    {
        return $this->portalOnly;
    }

    /**
     * @param bool $portalOnly
     * @return Users
     */
    public function setPortalOnly($portalOnly)
    {
        $this->portalOnly = $portalOnly;
        return $this;
    }

    /**
     * @return string
     */
    public function getEmployeeStatus()
    {
        return $this->employeeStatus;
    }

    /**
     * @param string $employeeStatus
     * @return Users
     */
    public function setEmployeeStatus($employeeStatus)
    {
        $this->employeeStatus = $employeeStatus;
        return $this;
    }

    /**
     * @return string
     */
    public function getMessengerId()
    {
        return $this->messengerId;
    }

    /**
     * @param string $messengerId
     * @return Users
     */
    public function setMessengerId($messengerId)
    {
        $this->messengerId = $messengerId;
        return $this;
    }

    /**
     * @return string
     */
    public function getMessengerType()
    {
        return $this->messengerType;
    }

    /**
     * @param string $messengerType
     * @return Users
     */
    public function setMessengerType($messengerType)
    {
        $this->messengerType = $messengerType;
        return $this;
    }

    /**
     * @return bool
     */
    public function isGroup()
    {
        return $this->isGroup;
    }

    /**
     * @param bool $isGroup
     * @return Users
     */
    public function setIsGroup($isGroup)
    {
        $this->isGroup = $isGroup;
        return $this;
    }

    /**
     * @return ContrattiCstm
     */
    public function getContrattiCstm()
    {
        return $this->contrattiCstm;
    }

    /**
     * @param ContrattiCstm $contrattiCstm
     * @return Users
     */
    public function setContrattiCstm($contrattiCstm)
    {
        $this->contrattiCstm = $contrattiCstm;
        return $this;
    }

    public function __toString()
    {
        return $this->id;
    }


}

