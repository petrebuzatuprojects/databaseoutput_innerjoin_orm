<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ContrattiDettaglio
 *
 * @ORM\Table(name="contratti_dettaglio", indexes={@ORM\Index(name="idx_con_lsd_con", columns={"listini_dettaglio_id"}), @ORM\Index(name="idx_con_lsd_lsd", columns={"contratto_id"}), @ORM\Index(name="IDX_contratti_dettaglio_id", columns={"id"}), @ORM\Index(name="IDX_contratti_dettaglio_name", columns={"name"}), @ORM\Index(name="idx_contratti_listini_dettaglio", columns={"contratto_id", "listini_dettaglio_id"}), @ORM\Index(name="IX_contratti_dettaglio", columns={"contratto_id", "id"})})
 * @ORM\Entity
 */
class ContrattiDettaglio
{


    /**
     * @var OfferteWebDettaglio
     *
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\OfferteWebDettaglio", mappedBy="contrattiDettaglio")
     */
    private $offerteWebD;





    /**
     * @var Contratti
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Contratti", inversedBy="contrattiDettaglios")
     * @ORM\JoinColumn(name="contratto_id", referencedColumnName="id")
     */
    private $contratti;



    /**
     * @var string
     *
     * @ORM\Column(name="id", type="string", length=36, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id = '\'\'';


    /**
     * @var ListiniDettaglioCstm
     *
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\ListiniDettaglioCstm")
     * @ORM\JoinColumn(name="listini_dettaglio_id", referencedColumnName="id_c", nullable=true)
     */
    private $listiniDettaglio;

    /**
     * @var string
     *
     * @ORM\Column(name="contratto_id", type="string", length=36, nullable=true)
     */
    private $contrattoId = 'NULL';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_entered", type="datetime", nullable=false)
     */
    private $dateEntered = '\'0000-00-00 00:00:00\'';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_modified", type="datetime", nullable=true)
     */
    private $dateModified = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="assigned_user_id", type="string", length=36, nullable=false)
     */
    private $assignedUserId = '\'\'';

    /**
     * @var string
     *
     * @ORM\Column(name="modified_user_id", type="string", length=36, nullable=false)
     */
    private $modifiedUserId = '\'\'';

    /**
     * @var string
     *
     * @ORM\Column(name="created_by", type="string", length=36, nullable=false)
     */
    private $createdBy = '\'\'';

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100, nullable=false)
     */
    private $name = '\'\'';

    /**
     * @var string
     *
     * @ORM\Column(name="note", type="string", length=255, nullable=true)
     */
    private $note = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="importo", type="decimal", precision=10, scale=2, nullable=false)
     */
    private $importo = '0.00';

    /**
     * @var string
     *
     * @ORM\Column(name="quantita", type="decimal", precision=5, scale=2, nullable=false)
     */
    private $quantita = '0.00';

    /**
     * @var string
     *
     * @ORM\Column(name="iva", type="decimal", precision=5, scale=2, nullable=false)
     */
    private $iva = '0.00';

    /**
     * @var string
     *
     * @ORM\Column(name="importoconiva", type="decimal", precision=10, scale=2, nullable=false)
     */
    private $importoconiva = '0.00';

    /**
     * @var string
     *
     * @ORM\Column(name="anticipo", type="decimal", precision=10, scale=2, nullable=false)
     */
    private $anticipo = '0.00';

    /**
     * @var boolean
     *
     * @ORM\Column(name="deleted", type="boolean", nullable=false)
     */
    private $deleted = '0';

    /**
     * @return Contratti
     */
    public function getContratti()
    {
        return $this->contratti;
    }

    /**
     * @param Contratti $contratti
     * @return ContrattiDettaglio
     */
    public function setContratti($contratti)
    {
        $this->contratti = $contratti;
        return $this;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return ContrattiDettaglio
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getListiniDettaglioId()
    {
        return $this->listiniDettaglioId;
    }

    /**
     * @param string $listiniDettaglioId
     * @return ContrattiDettaglio
     */
    public function setListiniDettaglioId($listiniDettaglioId)
    {
        $this->listiniDettaglioId = $listiniDettaglioId;
        return $this;
    }

    /**
     * @return string
     */
    public function getContrattoId()
    {
        return $this->contrattoId;
    }

    /**
     * @param string $contrattoId
     * @return ContrattiDettaglio
     */
    public function setContrattoId($contrattoId)
    {
        $this->contrattoId = $contrattoId;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateEntered()
    {
        return $this->dateEntered;
    }

    /**
     * @param \DateTime $dateEntered
     * @return ContrattiDettaglio
     */
    public function setDateEntered($dateEntered)
    {
        $this->dateEntered = $dateEntered;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateModified()
    {
        return $this->dateModified;
    }

    /**
     * @param \DateTime $dateModified
     * @return ContrattiDettaglio
     */
    public function setDateModified($dateModified)
    {
        $this->dateModified = $dateModified;
        return $this;
    }

    /**
     * @return string
     */
    public function getAssignedUserId()
    {
        return $this->assignedUserId;
    }

    /**
     * @param string $assignedUserId
     * @return ContrattiDettaglio
     */
    public function setAssignedUserId($assignedUserId)
    {
        $this->assignedUserId = $assignedUserId;
        return $this;
    }

    /**
     * @return string
     */
    public function getModifiedUserId()
    {
        return $this->modifiedUserId;
    }

    /**
     * @param string $modifiedUserId
     * @return ContrattiDettaglio
     */
    public function setModifiedUserId($modifiedUserId)
    {
        $this->modifiedUserId = $modifiedUserId;
        return $this;
    }

    /**
     * @return string
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * @param string $createdBy
     * @return ContrattiDettaglio
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;
        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return ContrattiDettaglio
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * @param string $note
     * @return ContrattiDettaglio
     */
    public function setNote($note)
    {
        $this->note = $note;
        return $this;
    }

    /**
     * @return string
     */
    public function getImporto()
    {
        return $this->importo;
    }

    /**
     * @param string $importo
     * @return ContrattiDettaglio
     */
    public function setImporto($importo)
    {
        $this->importo = $importo;
        return $this;
    }

    /**
     * @return string
     */
    public function getQuantita()
    {
        return $this->quantita;
    }

    /**
     * @param string $quantita
     * @return ContrattiDettaglio
     */
    public function setQuantita($quantita)
    {
        $this->quantita = $quantita;
        return $this;
    }

    /**
     * @return string
     */
    public function getIva()
    {
        return $this->iva;
    }

    /**
     * @param string $iva
     * @return ContrattiDettaglio
     */
    public function setIva($iva)
    {
        $this->iva = $iva;
        return $this;
    }

    /**
     * @return string
     */
    public function getImportoconiva()
    {
        return $this->importoconiva;
    }

    /**
     * @param string $importoconiva
     * @return ContrattiDettaglio
     */
    public function setImportoconiva($importoconiva)
    {
        $this->importoconiva = $importoconiva;
        return $this;
    }

    /**
     * @return string
     */
    public function getAnticipo()
    {
        return $this->anticipo;
    }

    /**
     * @param string $anticipo
     * @return ContrattiDettaglio
     */
    public function setAnticipo($anticipo)
    {
        $this->anticipo = $anticipo;
        return $this;
    }

    /**
     * @return bool
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * @param bool $deleted
     * @return ContrattiDettaglio
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;
        return $this;
    }

    /**
     * @return ListiniDettaglioCstm
     */
    public function getListiniDettaglio()
    {
        return $this->listiniDettaglio;
    }

    /**
     * @param ListiniDettaglioCstm $listiniDettaglio
     * @return ContrattiDettaglio
     */
    public function setListiniDettaglio($listiniDettaglio)
    {
        $this->listiniDettaglio = $listiniDettaglio;
        return $this;
    }

    /**
     * @return OfferteWebDettaglio
     */
    public function getOfferteWebD()
    {
        return $this->offerteWebD;
    }

    /**
     * @param OfferteWebDettaglio $offerteWebD
     * @return ContrattiDettaglio
     */
    public function setOfferteWebD($offerteWebD)
    {
        $this->offerteWebD = $offerteWebD;
        return $this;
    }







}

