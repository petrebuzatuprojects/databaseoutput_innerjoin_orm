<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * OfferteWebDettaglio
 *
 * @ORM\Table(name="offerte_web_dettaglio")
 * @ORM\Entity
 */
class OfferteWebDettaglio
{


    /**
     * @var ContrattiDettaglio
     *
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\ContrattiDettaglio", inversedBy="offerteWebD")
     * @ORM\JoinColumn(name="Listini_Dettaglio_ID", referencedColumnName="listini_dettaglio_id", nullable=true)
     */
    private $contrattiDettaglio;


    /**
     * @var OfferteWeb
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\OfferteWeb", inversedBy="offerteWebDettaglio")
     * @ORM\JoinColumn(name="IDOfferta_Web", referencedColumnName="ID")
     */
    private $offerteWeb;


    /**
     * @var string
     *
     * @ORM\Column(name="ID", type="string", length=36, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id = '\'\'';

    /**
     * @var string
     *
     * @ORM\Column(name="IDOfferta_Web", type="string", length=36, nullable=true)
     */
    private $idoffertaWeb = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="Listini_Dettaglio_ID", type="string", length=36, nullable=true)
     */
    private $listiniDettaglioId = 'NULL';

    /**
     * @var integer
     *
     * @ORM\Column(name="ServizioAddizionale", type="smallint", nullable=true)
     */
    private $servizioaddizionale = 'NULL';

    /**
     * @var integer
     *
     * @ORM\Column(name="TipoCopertura", type="smallint", nullable=true)
     */
    private $tipocopertura = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="TipoPagamento", type="string", length=5, nullable=true)
     */
    private $tipopagamento = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="NoteContrattuali", type="text", length=65535, nullable=true)
     */
    private $notecontrattuali = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="Gruppo", type="string", length=50, nullable=true)
     */
    private $gruppo = 'NULL';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DataFineValidita", type="datetime", nullable=true)
     */
    private $datafinevalidita = 'NULL';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DataInizioValidita", type="datetime", nullable=true)
     */
    private $datainiziovalidita = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="IDName", type="string", length=20, nullable=true)
     */
    private $idname = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="Listini_Dettaglio_Name", type="string", length=100, nullable=true)
     */
    private $listiniDettaglioName = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="ID_Offerta_Web_Dettaglio_Child", type="string", length=36, nullable=true)
     */
    private $idOffertaWebDettaglioChild = 'NULL';

    /**
     * @var integer
     *
     * @ORM\Column(name="NumeroMesiValidita", type="integer", nullable=true)
     */
    private $numeromesivalidita = 'NULL';

    /**
     * @var integer
     *
     * @ORM\Column(name="NumeroMesiEvasione", type="integer", nullable=true)
     */
    private $numeromesievasione = 'NULL';

    /**
     * @return OfferteWeb
     */
    public function getOfferteWeb()
    {
        return $this->offerteWeb;
    }

    /**
     * @param OfferteWeb $offerteWeb
     * @return OfferteWebDettaglio
     */
    public function setOfferteWeb($offerteWeb)
    {
        $this->offerteWeb = $offerteWeb;
        return $this;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return OfferteWebDettaglio
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getIdoffertaWeb()
    {
        return $this->idoffertaWeb;
    }

    /**
     * @param string $idoffertaWeb
     * @return OfferteWebDettaglio
     */
    public function setIdoffertaWeb($idoffertaWeb)
    {
        $this->idoffertaWeb = $idoffertaWeb;
        return $this;
    }

    /**
     * @return string
     */
    public function getListiniDettaglioId()
    {
        return $this->listiniDettaglioId;
    }

    /**
     * @param string $listiniDettaglioId
     * @return OfferteWebDettaglio
     */
    public function setListiniDettaglioId($listiniDettaglioId)
    {
        $this->listiniDettaglioId = $listiniDettaglioId;
        return $this;
    }

    /**
     * @return int
     */
    public function getServizioaddizionale()
    {
        return $this->servizioaddizionale;
    }

    /**
     * @param int $servizioaddizionale
     * @return OfferteWebDettaglio
     */
    public function setServizioaddizionale($servizioaddizionale)
    {
        $this->servizioaddizionale = $servizioaddizionale;
        return $this;
    }

    /**
     * @return int
     */
    public function getTipocopertura()
    {
        return $this->tipocopertura;
    }

    /**
     * @param int $tipocopertura
     * @return OfferteWebDettaglio
     */
    public function setTipocopertura($tipocopertura)
    {
        $this->tipocopertura = $tipocopertura;
        return $this;
    }

    /**
     * @return string
     */
    public function getTipopagamento()
    {
        return $this->tipopagamento;
    }

    /**
     * @param string $tipopagamento
     * @return OfferteWebDettaglio
     */
    public function setTipopagamento($tipopagamento)
    {
        $this->tipopagamento = $tipopagamento;
        return $this;
    }

    /**
     * @return string
     */
    public function getNotecontrattuali()
    {
        return $this->notecontrattuali;
    }

    /**
     * @param string $notecontrattuali
     * @return OfferteWebDettaglio
     */
    public function setNotecontrattuali($notecontrattuali)
    {
        $this->notecontrattuali = $notecontrattuali;
        return $this;
    }

    /**
     * @return string
     */
    public function getGruppo()
    {
        return $this->gruppo;
    }

    /**
     * @param string $gruppo
     * @return OfferteWebDettaglio
     */
    public function setGruppo($gruppo)
    {
        $this->gruppo = $gruppo;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDatafinevalidita()
    {
        return $this->datafinevalidita;
    }

    /**
     * @param \DateTime $datafinevalidita
     * @return OfferteWebDettaglio
     */
    public function setDatafinevalidita($datafinevalidita)
    {
        $this->datafinevalidita = $datafinevalidita;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDatainiziovalidita()
    {
        return $this->datainiziovalidita;
    }

    /**
     * @param \DateTime $datainiziovalidita
     * @return OfferteWebDettaglio
     */
    public function setDatainiziovalidita($datainiziovalidita)
    {
        $this->datainiziovalidita = $datainiziovalidita;
        return $this;
    }

    /**
     * @return string
     */
    public function getIdname()
    {
        return $this->idname;
    }

    /**
     * @param string $idname
     * @return OfferteWebDettaglio
     */
    public function setIdname($idname)
    {
        $this->idname = $idname;
        return $this;
    }

    /**
     * @return string
     */
    public function getListiniDettaglioName()
    {
        return $this->listiniDettaglioName;
    }

    /**
     * @param string $listiniDettaglioName
     * @return OfferteWebDettaglio
     */
    public function setListiniDettaglioName($listiniDettaglioName)
    {
        $this->listiniDettaglioName = $listiniDettaglioName;
        return $this;
    }

    /**
     * @return string
     */
    public function getIdOffertaWebDettaglioChild()
    {
        return $this->idOffertaWebDettaglioChild;
    }

    /**
     * @param string $idOffertaWebDettaglioChild
     * @return OfferteWebDettaglio
     */
    public function setIdOffertaWebDettaglioChild($idOffertaWebDettaglioChild)
    {
        $this->idOffertaWebDettaglioChild = $idOffertaWebDettaglioChild;
        return $this;
    }

    /**
     * @return int
     */
    public function getNumeromesivalidita()
    {
        return $this->numeromesivalidita;
    }

    /**
     * @param int $numeromesivalidita
     * @return OfferteWebDettaglio
     */
    public function setNumeromesivalidita($numeromesivalidita)
    {
        $this->numeromesivalidita = $numeromesivalidita;
        return $this;
    }

    /**
     * @return int
     */
    public function getNumeromesievasione()
    {
        return $this->numeromesievasione;
    }

    /**
     * @param int $numeromesievasione
     * @return OfferteWebDettaglio
     */
    public function setNumeromesievasione($numeromesievasione)
    {
        $this->numeromesievasione = $numeromesievasione;
        return $this;
    }

    /**
     * @return ContrattiDettaglio
     */
    public function getContrattiDettaglio()
    {
        return $this->contrattiDettaglio;
    }

    /**
     * @param ContrattiDettaglio $contrattiDettaglio
     * @return OfferteWebDettaglio
     */
    public function setContrattiDettaglio($contrattiDettaglio)
    {
        $this->contrattiDettaglio = $contrattiDettaglio;
        return $this;
    }







}

