<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ListiniDettaglioCstm
 *
 * @ORM\Table(name="listini_dettaglio_cstm")
 * @ORM\Entity
 */
class ListiniDettaglioCstm
{


    /**
     * @var string
     *
     * @ORM\Column(name="id_c", type="string", length=36, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idC = '\'\'';

    /**
     * @var float
     *
     * @ORM\Column(name="Importo_c", type="float", precision=10, scale=0, nullable=false)
     */
    private $importoC = '0';

    /**
     * @var float
     *
     * @ORM\Column(name="Iva_c", type="float", precision=10, scale=0, nullable=false)
     */
    private $ivaC = '0';

    /**
     * @var float
     *
     * @ORM\Column(name="ImportoConIva_c", type="float", precision=10, scale=0, nullable=false)
     */
    private $importoconivaC = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="TipoFatturazione_c", type="string", length=20, nullable=false)
     */
    private $tipofatturazioneC = '\'K3\'';

    /**
     * @var string
     *
     * @ORM\Column(name="TipoDataCheckFatt_c", type="string", length=10, nullable=false)
     */
    private $tipodatacheckfattC = '\'\'';

    /**
     * @var string
     *
     * @ORM\Column(name="SezioneFattura_c", type="string", length=20, nullable=false)
     */
    private $sezionefatturaC = '\'Canoni\'';

    /**
     * @var string
     *
     * @ORM\Column(name="TipoCalcoloPeriodo_c", type="string", length=10, nullable=false)
     */
    private $tipocalcoloperiodoC = '\'TC2\'';

    /**
     * @var boolean
     *
     * @ORM\Column(name="FatturazioneAnticipata__c", type="boolean", nullable=true)
     */
    private $fatturazioneanticipataC = 'NULL';

    /**
     * @var float
     *
     * @ORM\Column(name="Fee_Dirette_c", type="float", precision=10, scale=0, nullable=true)
     */
    private $feeDiretteC = 'NULL';

    /**
     * @var float
     *
     * @ORM\Column(name="Fee_Indiretta_c", type="float", precision=10, scale=0, nullable=true)
     */
    private $feeIndirettaC = 'NULL';

    /**
     * @var integer
     *
     * @ORM\Column(name="ID_Listino_Fonia_c", type="integer", nullable=true)
     */
    private $idListinoFoniaC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="MBPC_Conto_c", type="string", length=7, nullable=true)
     */
    private $mbpcContoC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="MBPC_SottoConto_c", type="string", length=7, nullable=true)
     */
    private $mbpcSottocontoC = 'NULL';

    /**
     * @var boolean
     *
     * @ORM\Column(name="FlagVenditaOnLine_c", type="boolean", nullable=true)
     */
    private $flagvenditaonlineC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="Sezione_Contratto_c", type="string", length=10, nullable=true)
     */
    private $sezioneContrattoC = 'NULL';

    /**
     * @var integer
     *
     * @ORM\Column(name="NumMesiValidita_c", type="integer", nullable=true)
     */
    private $nummesivaliditaC = 'NULL';

    /**
     * @var integer
     *
     * @ORM\Column(name="NumMesiEvasione_c", type="integer", nullable=true)
     */
    private $nummesievasioneC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="Profilo_c", type="string", length=20, nullable=true)
     */
    private $profiloC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="CodiceArticolo_c", type="string", length=20, nullable=true)
     */
    private $codicearticoloC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="CodiceIva_c", type="string", length=10, nullable=false)
     */
    private $codiceivaC = '\'K4\'';

    /**
     * @var boolean
     *
     * @ORM\Column(name="IncassoAnticipo_c", type="boolean", nullable=true)
     */
    private $incassoanticipoC = 'NULL';

    /**
     * @var boolean
     *
     * @ORM\Column(name="Anticipo_c", type="boolean", nullable=true)
     */
    private $anticipoC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="DescrizioneExportPT_c", type="string", length=100, nullable=true)
     */
    private $descrizioneexportptC = 'NULL';

    /**
     * @var boolean
     *
     * @ORM\Column(name="FlagRateo_c", type="boolean", nullable=true)
     */
    private $flagrateoC = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="sezionalefattura_c", type="string", length=5, nullable=true)
     */
    private $sezionalefatturaC = 'NULL';

    /**
     * @return ContrattiDettaglio
     */
    public function getContrattiDettaglio()
    {
        return $this->contrattiDettaglio;
    }

    /**
     * @param ContrattiDettaglio $contrattiDettaglio
     * @return ListiniDettaglioCstm
     */
    public function setContrattiDettaglio($contrattiDettaglio)
    {
        $this->contrattiDettaglio = $contrattiDettaglio;
        return $this;
    }

    /**
     * @return string
     */
    public function getIdC()
    {
        return $this->idC;
    }

    /**
     * @param string $idC
     * @return ListiniDettaglioCstm
     */
    public function setIdC($idC)
    {
        $this->idC = $idC;
        return $this;
    }

    /**
     * @return float
     */
    public function getImportoC()
    {
        return $this->importoC;
    }

    /**
     * @param float $importoC
     * @return ListiniDettaglioCstm
     */
    public function setImportoC($importoC)
    {
        $this->importoC = $importoC;
        return $this;
    }

    /**
     * @return float
     */
    public function getIvaC()
    {
        return $this->ivaC;
    }

    /**
     * @param float $ivaC
     * @return ListiniDettaglioCstm
     */
    public function setIvaC($ivaC)
    {
        $this->ivaC = $ivaC;
        return $this;
    }

    /**
     * @return float
     */
    public function getImportoconivaC()
    {
        return $this->importoconivaC;
    }

    /**
     * @param float $importoconivaC
     * @return ListiniDettaglioCstm
     */
    public function setImportoconivaC($importoconivaC)
    {
        $this->importoconivaC = $importoconivaC;
        return $this;
    }

    /**
     * @return string
     */
    public function getTipofatturazioneC()
    {
        return $this->tipofatturazioneC;
    }

    /**
     * @param string $tipofatturazioneC
     * @return ListiniDettaglioCstm
     */
    public function setTipofatturazioneC($tipofatturazioneC)
    {
        $this->tipofatturazioneC = $tipofatturazioneC;
        return $this;
    }

    /**
     * @return string
     */
    public function getTipodatacheckfattC()
    {
        return $this->tipodatacheckfattC;
    }

    /**
     * @param string $tipodatacheckfattC
     * @return ListiniDettaglioCstm
     */
    public function setTipodatacheckfattC($tipodatacheckfattC)
    {
        $this->tipodatacheckfattC = $tipodatacheckfattC;
        return $this;
    }

    /**
     * @return string
     */
    public function getSezionefatturaC()
    {
        return $this->sezionefatturaC;
    }

    /**
     * @param string $sezionefatturaC
     * @return ListiniDettaglioCstm
     */
    public function setSezionefatturaC($sezionefatturaC)
    {
        $this->sezionefatturaC = $sezionefatturaC;
        return $this;
    }

    /**
     * @return string
     */
    public function getTipocalcoloperiodoC()
    {
        return $this->tipocalcoloperiodoC;
    }

    /**
     * @param string $tipocalcoloperiodoC
     * @return ListiniDettaglioCstm
     */
    public function setTipocalcoloperiodoC($tipocalcoloperiodoC)
    {
        $this->tipocalcoloperiodoC = $tipocalcoloperiodoC;
        return $this;
    }

    /**
     * @return bool
     */
    public function isFatturazioneanticipataC()
    {
        return $this->fatturazioneanticipataC;
    }

    /**
     * @param bool $fatturazioneanticipataC
     * @return ListiniDettaglioCstm
     */
    public function setFatturazioneanticipataC($fatturazioneanticipataC)
    {
        $this->fatturazioneanticipataC = $fatturazioneanticipataC;
        return $this;
    }

    /**
     * @return float
     */
    public function getFeeDiretteC()
    {
        return $this->feeDiretteC;
    }

    /**
     * @param float $feeDiretteC
     * @return ListiniDettaglioCstm
     */
    public function setFeeDiretteC($feeDiretteC)
    {
        $this->feeDiretteC = $feeDiretteC;
        return $this;
    }

    /**
     * @return float
     */
    public function getFeeIndirettaC()
    {
        return $this->feeIndirettaC;
    }

    /**
     * @param float $feeIndirettaC
     * @return ListiniDettaglioCstm
     */
    public function setFeeIndirettaC($feeIndirettaC)
    {
        $this->feeIndirettaC = $feeIndirettaC;
        return $this;
    }

    /**
     * @return int
     */
    public function getIdListinoFoniaC()
    {
        return $this->idListinoFoniaC;
    }

    /**
     * @param int $idListinoFoniaC
     * @return ListiniDettaglioCstm
     */
    public function setIdListinoFoniaC($idListinoFoniaC)
    {
        $this->idListinoFoniaC = $idListinoFoniaC;
        return $this;
    }

    /**
     * @return string
     */
    public function getMbpcContoC()
    {
        return $this->mbpcContoC;
    }

    /**
     * @param string $mbpcContoC
     * @return ListiniDettaglioCstm
     */
    public function setMbpcContoC($mbpcContoC)
    {
        $this->mbpcContoC = $mbpcContoC;
        return $this;
    }

    /**
     * @return string
     */
    public function getMbpcSottocontoC()
    {
        return $this->mbpcSottocontoC;
    }

    /**
     * @param string $mbpcSottocontoC
     * @return ListiniDettaglioCstm
     */
    public function setMbpcSottocontoC($mbpcSottocontoC)
    {
        $this->mbpcSottocontoC = $mbpcSottocontoC;
        return $this;
    }

    /**
     * @return bool
     */
    public function isFlagvenditaonlineC()
    {
        return $this->flagvenditaonlineC;
    }

    /**
     * @param bool $flagvenditaonlineC
     * @return ListiniDettaglioCstm
     */
    public function setFlagvenditaonlineC($flagvenditaonlineC)
    {
        $this->flagvenditaonlineC = $flagvenditaonlineC;
        return $this;
    }

    /**
     * @return string
     */
    public function getSezioneContrattoC()
    {
        return $this->sezioneContrattoC;
    }

    /**
     * @param string $sezioneContrattoC
     * @return ListiniDettaglioCstm
     */
    public function setSezioneContrattoC($sezioneContrattoC)
    {
        $this->sezioneContrattoC = $sezioneContrattoC;
        return $this;
    }

    /**
     * @return int
     */
    public function getNummesivaliditaC()
    {
        return $this->nummesivaliditaC;
    }

    /**
     * @param int $nummesivaliditaC
     * @return ListiniDettaglioCstm
     */
    public function setNummesivaliditaC($nummesivaliditaC)
    {
        $this->nummesivaliditaC = $nummesivaliditaC;
        return $this;
    }

    /**
     * @return int
     */
    public function getNummesievasioneC()
    {
        return $this->nummesievasioneC;
    }

    /**
     * @param int $nummesievasioneC
     * @return ListiniDettaglioCstm
     */
    public function setNummesievasioneC($nummesievasioneC)
    {
        $this->nummesievasioneC = $nummesievasioneC;
        return $this;
    }

    /**
     * @return string
     */
    public function getProfiloC()
    {
        return $this->profiloC;
    }

    /**
     * @param string $profiloC
     * @return ListiniDettaglioCstm
     */
    public function setProfiloC($profiloC)
    {
        $this->profiloC = $profiloC;
        return $this;
    }

    /**
     * @return string
     */
    public function getCodicearticoloC()
    {
        return $this->codicearticoloC;
    }

    /**
     * @param string $codicearticoloC
     * @return ListiniDettaglioCstm
     */
    public function setCodicearticoloC($codicearticoloC)
    {
        $this->codicearticoloC = $codicearticoloC;
        return $this;
    }

    /**
     * @return string
     */
    public function getCodiceivaC()
    {
        return $this->codiceivaC;
    }

    /**
     * @param string $codiceivaC
     * @return ListiniDettaglioCstm
     */
    public function setCodiceivaC($codiceivaC)
    {
        $this->codiceivaC = $codiceivaC;
        return $this;
    }

    /**
     * @return bool
     */
    public function isIncassoanticipoC()
    {
        return $this->incassoanticipoC;
    }

    /**
     * @param bool $incassoanticipoC
     * @return ListiniDettaglioCstm
     */
    public function setIncassoanticipoC($incassoanticipoC)
    {
        $this->incassoanticipoC = $incassoanticipoC;
        return $this;
    }

    /**
     * @return bool
     */
    public function isAnticipoC()
    {
        return $this->anticipoC;
    }

    /**
     * @param bool $anticipoC
     * @return ListiniDettaglioCstm
     */
    public function setAnticipoC($anticipoC)
    {
        $this->anticipoC = $anticipoC;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescrizioneexportptC()
    {
        return $this->descrizioneexportptC;
    }

    /**
     * @param string $descrizioneexportptC
     * @return ListiniDettaglioCstm
     */
    public function setDescrizioneexportptC($descrizioneexportptC)
    {
        $this->descrizioneexportptC = $descrizioneexportptC;
        return $this;
    }

    /**
     * @return bool
     */
    public function isFlagrateoC()
    {
        return $this->flagrateoC;
    }

    /**
     * @param bool $flagrateoC
     * @return ListiniDettaglioCstm
     */
    public function setFlagrateoC($flagrateoC)
    {
        $this->flagrateoC = $flagrateoC;
        return $this;
    }

    /**
     * @return string
     */
    public function getSezionalefatturaC()
    {
        return $this->sezionalefatturaC;
    }

    /**
     * @param string $sezionalefatturaC
     * @return ListiniDettaglioCstm
     */
    public function setSezionalefatturaC($sezionalefatturaC)
    {
        $this->sezionalefatturaC = $sezionalefatturaC;
        return $this;
    }




}

