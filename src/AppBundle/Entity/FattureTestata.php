<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FattureTestata
 *
 * @ORM\Table(name="fatture_testata", uniqueConstraints={@ORM\UniqueConstraint(name="UK_Fatture_Testata", columns={"IDRecord", "SpeseVarie"})}, indexes={@ORM\Index(name="Account", columns={"account_id"}), @ORM\Index(name="IDX_account_chiusura", columns={"account_id", "fatturachiusura"}), @ORM\Index(name="IDX_contratto_chiusura", columns={"IDContratto", "fatturachiusura"}), @ORM\Index(name="IDX_Fatture_Testata", columns={"Sezionale", "NumDocSez"}), @ORM\Index(name="IDX_Fatture_Testata_CBill", columns={"CBill"}), @ORM\Index(name="IDX_Fatture_Testata_IDContratto", columns={"IDContratto"}), @ORM\Index(name="IDX_Fatture_Testata_IDFattureElaborazioni", columns={"IDFattureElaborazioni"}), @ORM\Index(name="IDX_Fatture_Testata_Note", columns={"Note"}), @ORM\Index(name="IDX_Fatture_Testata_NumDocSez", columns={"NumDocSez"}), @ORM\Index(name="IDX_Fatture_Testata2", columns={"NumeroFattura", "DataFattura"}), @ORM\Index(name="IDX_Fatture_Testata3", columns={"Sezionale", "NumDocSez", "DataFattura", "account_id"}), @ORM\Index(name="IDX_Fatture_Testata4", columns={"DataFattura", "NumDocSez"}), @ORM\Index(name="IDX_Fatture_Testata5", columns={"Periodicita", "CodiceTipoFattura"}), @ORM\Index(name="IDX_Fatture_Testata6", columns={"account_id", "IDContratto"}), @ORM\Index(name="IDX_Fatture_Testata7", columns={"Sezionale", "NumDocSez", "DataFattura"}), @ORM\Index(name="IX_Fatture_Testata_IDRecord", columns={"IDRecord"}), @ORM\Index(name="NumeroFattura", columns={"NumeroFattura"}), @ORM\Index(name="NumFat", columns={"NumeroFattura"}), @ORM\Index(name="Periodicita", columns={"Periodicita"}), @ORM\Index(name="Periodo", columns={"Periodicita", "NumeroFattura"})})
 * @ORM\Entity
 */
class FattureTestata
{


    /**
     * @var Contratti
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Contratti", inversedBy="fattureTestata")
     * @ORM\JoinColumn(name="IDContratto", referencedColumnName="id")
     */
    private $contratti;


    /**
     * @var integer
     *
     * @ORM\Column(name="IDRecord", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idrecord;

    /**
     * @var integer
     *
     * @ORM\Column(name="IDFattureElaborazioni", type="integer", nullable=true)
     */
    private $idfattureelaborazioni = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="account_id", type="string", length=36, nullable=true)
     */
    private $accountId = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="IDTipoDocumento", type="string", length=1, nullable=true)
     */
    private $idtipodocumento = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="NumeroFattura", type="string", length=15, nullable=true)
     */
    private $numerofattura = 'NULL';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DataFattura", type="datetime", nullable=false)
     */
    private $datafattura = '\'0000-00-00 00:00:00\'';

    /**
     * @var string
     *
     * @ORM\Column(name="IDTipoPagamento", type="string", length=60, nullable=true)
     */
    private $idtipopagamento = null;

    /**
     * @var boolean
     *
     * @ORM\Column(name="FlagDaContabilizzare", type="boolean", nullable=true)
     */
    private $flagdacontabilizzare = 'NULL';

    /**
     * @var integer
     *
     * @ORM\Column(name="IDSchemaContabilizzazione", type="integer", nullable=true)
     */
    private $idschemacontabilizzazione = 'NULL';

    /**
     * @var integer
     *
     * @ORM\Column(name="IDRID", type="integer", nullable=true)
     */
    private $idrid = 'NULL';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DataScadenzaPagamento", type="datetime", nullable=true)
     */
    private $datascadenzapagamento = 'NULL';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DataCreazione", type="datetime", nullable=true)
     */
    private $datacreazione = 'NULL';

    /**
     * @var boolean
     *
     * @ORM\Column(name="FlagAnnullato", type="boolean", nullable=true)
     */
    private $flagannullato = 'NULL';

    /**
     * @var integer
     *
     * @ORM\Column(name="Periodicita", type="integer", nullable=true)
     */
    private $periodicita = 'NULL';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DataStampaFattura", type="datetime", nullable=true)
     */
    private $datastampafattura = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="StatoRIDFattura", type="string", length=3, nullable=false)
     */
    private $statoridfattura = '\'K1\'';

    /**
     * @var string
     *
     * @ORM\Column(name="Note", type="string", length=1024, nullable=true)
     */
    private $note = 'NULL';

    /**
     * @var integer
     *
     * @ORM\Column(name="CARG_ID", type="integer", nullable=true)
     */
    private $cargId = 'NULL';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DataRegistrazione", type="datetime", nullable=true)
     */
    private $dataregistrazione = 'NULL';

    /**
     * @var integer
     *
     * @ORM\Column(name="NumeroRegistrazione", type="integer", nullable=true)
     */
    private $numeroregistrazione = 'NULL';

    /**
     * @var boolean
     *
     * @ORM\Column(name="EsitoContabilizzazione", type="boolean", nullable=true)
     */
    private $esitocontabilizzazione = 'NULL';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DataOraContabilizzazione", type="datetime", nullable=true)
     */
    private $dataoracontabilizzazione = 'NULL';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DataOraInvioEmail", type="datetime", nullable=true)
     */
    private $dataorainvioemail = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="IDContratto", type="string", length=36, nullable=true)
     */
    private $idcontratto = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="SpeseVarie", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $spesevarie = '0.00';

    /**
     * @var string
     *
     * @ORM\Column(name="DescrizioneSpeseVarie", type="string", length=256, nullable=true)
     */
    private $descrizionespesevarie = 'NULL';

    /**
     * @var integer
     *
     * @ORM\Column(name="IDIvaSpeseVarie", type="integer", nullable=true)
     */
    private $idivaspesevarie = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="CodiceTipoFattura", type="string", length=10, nullable=true)
     */
    private $codicetipofattura = 'NULL';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DataInvioPosteItaliane", type="datetime", nullable=true)
     */
    private $datainvioposteitaliane = 'NULL';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DataExport", type="datetime", nullable=true)
     */
    private $dataexport = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="Sezionale", type="string", length=2, nullable=true)
     */
    private $sezionale = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="NumDocSez", type="string", length=13, nullable=true)
     */
    private $numdocsez = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="StatoContratto", type="string", length=20, nullable=true)
     */
    private $statocontratto = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="StatoDisdetta", type="string", length=5, nullable=true)
     */
    private $statodisdetta = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="TipoCPE", type="string", length=32, nullable=true)
     */
    private $tipocpe = 'NULL';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DataDisdetta", type="date", nullable=true)
     */
    private $datadisdetta = 'NULL';

    /**
     * @var boolean
     *
     * @ORM\Column(name="Utilizzo30gg", type="boolean", nullable=true)
     */
    private $utilizzo30gg = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="MessaggioInviato", type="boolean", nullable=true)
     */
    private $messaggioinviato = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="fatturachiusura", type="boolean", nullable=true)
     */
    private $fatturachiusura = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="CBill", type="string", length=18, nullable=true)
     */
    private $cbill = 'NULL';

    /**
     * @return Contratti
     */
    public function getContratti()
    {
        return $this->contratti;
    }

    /**
     * @param Contratti $contratti
     * @return FattureTestata
     */
    public function setContratti($contratti)
    {
        $this->contratti = $contratti;
        return $this;
    }

    /**
     * @return int
     */
    public function getIdrecord()
    {
        return $this->idrecord;
    }

    /**
     * @param int $idrecord
     * @return FattureTestata
     */
    public function setIdrecord($idrecord)
    {
        $this->idrecord = $idrecord;
        return $this;
    }

    /**
     * @return int
     */
    public function getIdfattureelaborazioni()
    {
        return $this->idfattureelaborazioni;
    }

    /**
     * @param int $idfattureelaborazioni
     * @return FattureTestata
     */
    public function setIdfattureelaborazioni($idfattureelaborazioni)
    {
        $this->idfattureelaborazioni = $idfattureelaborazioni;
        return $this;
    }

    /**
     * @return string
     */
    public function getAccountId()
    {
        return $this->accountId;
    }

    /**
     * @param string $accountId
     * @return FattureTestata
     */
    public function setAccountId($accountId)
    {
        $this->accountId = $accountId;
        return $this;
    }

    /**
     * @return string
     */
    public function getIdtipodocumento()
    {
        return $this->idtipodocumento;
    }

    /**
     * @param string $idtipodocumento
     * @return FattureTestata
     */
    public function setIdtipodocumento($idtipodocumento)
    {
        $this->idtipodocumento = $idtipodocumento;
        return $this;
    }

    /**
     * @return string
     */
    public function getNumerofattura()
    {
        return $this->numerofattura;
    }

    /**
     * @param string $numerofattura
     * @return FattureTestata
     */
    public function setNumerofattura($numerofattura)
    {
        $this->numerofattura = $numerofattura;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDatafattura()
    {
        return $this->datafattura;
    }

    /**
     * @param \DateTime $datafattura
     * @return FattureTestata
     */
    public function setDatafattura($datafattura)
    {
        $this->datafattura = $datafattura;
        return $this;
    }

    /**
     * @return string
     */
    public function getIdtipopagamento()
    {
        return $this->idtipopagamento;
    }

    /**
     * @param string $idtipopagamento
     * @return FattureTestata
     */
    public function setIdtipopagamento($idtipopagamento)
    {
        $this->idtipopagamento = $idtipopagamento;
        return $this;
    }

    /**
     * @return bool
     */
    public function isFlagdacontabilizzare()
    {
        return $this->flagdacontabilizzare;
    }

    /**
     * @param bool $flagdacontabilizzare
     * @return FattureTestata
     */
    public function setFlagdacontabilizzare($flagdacontabilizzare)
    {
        $this->flagdacontabilizzare = $flagdacontabilizzare;
        return $this;
    }

    /**
     * @return int
     */
    public function getIdschemacontabilizzazione()
    {
        return $this->idschemacontabilizzazione;
    }

    /**
     * @param int $idschemacontabilizzazione
     * @return FattureTestata
     */
    public function setIdschemacontabilizzazione($idschemacontabilizzazione)
    {
        $this->idschemacontabilizzazione = $idschemacontabilizzazione;
        return $this;
    }

    /**
     * @return int
     */
    public function getIdrid()
    {
        return $this->idrid;
    }

    /**
     * @param int $idrid
     * @return FattureTestata
     */
    public function setIdrid($idrid)
    {
        $this->idrid = $idrid;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDatascadenzapagamento()
    {
        return $this->datascadenzapagamento;
    }

    /**
     * @param \DateTime $datascadenzapagamento
     * @return FattureTestata
     */
    public function setDatascadenzapagamento($datascadenzapagamento)
    {
        $this->datascadenzapagamento = $datascadenzapagamento;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDatacreazione()
    {
        return $this->datacreazione;
    }

    /**
     * @param \DateTime $datacreazione
     * @return FattureTestata
     */
    public function setDatacreazione($datacreazione)
    {
        $this->datacreazione = $datacreazione;
        return $this;
    }

    /**
     * @return bool
     */
    public function isFlagannullato()
    {
        return $this->flagannullato;
    }

    /**
     * @param bool $flagannullato
     * @return FattureTestata
     */
    public function setFlagannullato($flagannullato)
    {
        $this->flagannullato = $flagannullato;
        return $this;
    }

    /**
     * @return int
     */
    public function getPeriodicita()
    {
        return $this->periodicita;
    }

    /**
     * @param int $periodicita
     * @return FattureTestata
     */
    public function setPeriodicita($periodicita)
    {
        $this->periodicita = $periodicita;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDatastampafattura()
    {
        return $this->datastampafattura;
    }

    /**
     * @param \DateTime $datastampafattura
     * @return FattureTestata
     */
    public function setDatastampafattura($datastampafattura)
    {
        $this->datastampafattura = $datastampafattura;
        return $this;
    }

    /**
     * @return string
     */
    public function getStatoridfattura()
    {
        return $this->statoridfattura;
    }

    /**
     * @param string $statoridfattura
     * @return FattureTestata
     */
    public function setStatoridfattura($statoridfattura)
    {
        $this->statoridfattura = $statoridfattura;
        return $this;
    }

    /**
     * @return string
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * @param string $note
     * @return FattureTestata
     */
    public function setNote($note)
    {
        $this->note = $note;
        return $this;
    }

    /**
     * @return int
     */
    public function getCargId()
    {
        return $this->cargId;
    }

    /**
     * @param int $cargId
     * @return FattureTestata
     */
    public function setCargId($cargId)
    {
        $this->cargId = $cargId;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDataregistrazione()
    {
        return $this->dataregistrazione;
    }

    /**
     * @param \DateTime $dataregistrazione
     * @return FattureTestata
     */
    public function setDataregistrazione($dataregistrazione)
    {
        $this->dataregistrazione = $dataregistrazione;
        return $this;
    }

    /**
     * @return int
     */
    public function getNumeroregistrazione()
    {
        return $this->numeroregistrazione;
    }

    /**
     * @param int $numeroregistrazione
     * @return FattureTestata
     */
    public function setNumeroregistrazione($numeroregistrazione)
    {
        $this->numeroregistrazione = $numeroregistrazione;
        return $this;
    }

    /**
     * @return bool
     */
    public function isEsitocontabilizzazione()
    {
        return $this->esitocontabilizzazione;
    }

    /**
     * @param bool $esitocontabilizzazione
     * @return FattureTestata
     */
    public function setEsitocontabilizzazione($esitocontabilizzazione)
    {
        $this->esitocontabilizzazione = $esitocontabilizzazione;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDataoracontabilizzazione()
    {
        return $this->dataoracontabilizzazione;
    }

    /**
     * @param \DateTime $dataoracontabilizzazione
     * @return FattureTestata
     */
    public function setDataoracontabilizzazione($dataoracontabilizzazione)
    {
        $this->dataoracontabilizzazione = $dataoracontabilizzazione;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDataorainvioemail()
    {
        return $this->dataorainvioemail;
    }

    /**
     * @param \DateTime $dataorainvioemail
     * @return FattureTestata
     */
    public function setDataorainvioemail($dataorainvioemail)
    {
        $this->dataorainvioemail = $dataorainvioemail;
        return $this;
    }

    /**
     * @return string
     */
    public function getIdcontratto()
    {
        return $this->idcontratto;
    }

    /**
     * @param string $idcontratto
     * @return FattureTestata
     */
    public function setIdcontratto($idcontratto)
    {
        $this->idcontratto = $idcontratto;
        return $this;
    }

    /**
     * @return string
     */
    public function getSpesevarie()
    {
        return $this->spesevarie;
    }

    /**
     * @param string $spesevarie
     * @return FattureTestata
     */
    public function setSpesevarie($spesevarie)
    {
        $this->spesevarie = $spesevarie;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescrizionespesevarie()
    {
        return $this->descrizionespesevarie;
    }

    /**
     * @param string $descrizionespesevarie
     * @return FattureTestata
     */
    public function setDescrizionespesevarie($descrizionespesevarie)
    {
        $this->descrizionespesevarie = $descrizionespesevarie;
        return $this;
    }

    /**
     * @return int
     */
    public function getIdivaspesevarie()
    {
        return $this->idivaspesevarie;
    }

    /**
     * @param int $idivaspesevarie
     * @return FattureTestata
     */
    public function setIdivaspesevarie($idivaspesevarie)
    {
        $this->idivaspesevarie = $idivaspesevarie;
        return $this;
    }

    /**
     * @return string
     */
    public function getCodicetipofattura()
    {
        return $this->codicetipofattura;
    }

    /**
     * @param string $codicetipofattura
     * @return FattureTestata
     */
    public function setCodicetipofattura($codicetipofattura)
    {
        $this->codicetipofattura = $codicetipofattura;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDatainvioposteitaliane()
    {
        return $this->datainvioposteitaliane;
    }

    /**
     * @param \DateTime $datainvioposteitaliane
     * @return FattureTestata
     */
    public function setDatainvioposteitaliane($datainvioposteitaliane)
    {
        $this->datainvioposteitaliane = $datainvioposteitaliane;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDataexport()
    {
        return $this->dataexport;
    }

    /**
     * @param \DateTime $dataexport
     * @return FattureTestata
     */
    public function setDataexport($dataexport)
    {
        $this->dataexport = $dataexport;
        return $this;
    }

    /**
     * @return string
     */
    public function getSezionale()
    {
        return $this->sezionale;
    }

    /**
     * @param string $sezionale
     * @return FattureTestata
     */
    public function setSezionale($sezionale)
    {
        $this->sezionale = $sezionale;
        return $this;
    }

    /**
     * @return string
     */
    public function getNumdocsez()
    {
        return $this->numdocsez;
    }

    /**
     * @param string $numdocsez
     * @return FattureTestata
     */
    public function setNumdocsez($numdocsez)
    {
        $this->numdocsez = $numdocsez;
        return $this;
    }

    /**
     * @return string
     */
    public function getStatocontratto()
    {
        return $this->statocontratto;
    }

    /**
     * @param string $statocontratto
     * @return FattureTestata
     */
    public function setStatocontratto($statocontratto)
    {
        $this->statocontratto = $statocontratto;
        return $this;
    }

    /**
     * @return string
     */
    public function getStatodisdetta()
    {
        return $this->statodisdetta;
    }

    /**
     * @param string $statodisdetta
     * @return FattureTestata
     */
    public function setStatodisdetta($statodisdetta)
    {
        $this->statodisdetta = $statodisdetta;
        return $this;
    }

    /**
     * @return string
     */
    public function getTipocpe()
    {
        return $this->tipocpe;
    }

    /**
     * @param string $tipocpe
     * @return FattureTestata
     */
    public function setTipocpe($tipocpe)
    {
        $this->tipocpe = $tipocpe;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDatadisdetta()
    {
        return $this->datadisdetta;
    }

    /**
     * @param \DateTime $datadisdetta
     * @return FattureTestata
     */
    public function setDatadisdetta($datadisdetta)
    {
        $this->datadisdetta = $datadisdetta;
        return $this;
    }

    /**
     * @return bool
     */
    public function isUtilizzo30gg()
    {
        return $this->utilizzo30gg;
    }

    /**
     * @param bool $utilizzo30gg
     * @return FattureTestata
     */
    public function setUtilizzo30gg($utilizzo30gg)
    {
        $this->utilizzo30gg = $utilizzo30gg;
        return $this;
    }

    /**
     * @return bool
     */
    public function isMessaggioinviato()
    {
        return $this->messaggioinviato;
    }

    /**
     * @param bool $messaggioinviato
     * @return FattureTestata
     */
    public function setMessaggioinviato($messaggioinviato)
    {
        $this->messaggioinviato = $messaggioinviato;
        return $this;
    }

    /**
     * @return bool
     */
    public function isFatturachiusura()
    {
        return $this->fatturachiusura;
    }

    /**
     * @param bool $fatturachiusura
     * @return FattureTestata
     */
    public function setFatturachiusura($fatturachiusura)
    {
        $this->fatturachiusura = $fatturachiusura;
        return $this;
    }

    /**
     * @return string
     */
    public function getCbill()
    {
        return $this->cbill;
    }

    /**
     * @param string $cbill
     * @return FattureTestata
     */
    public function setCbill($cbill)
    {
        $this->cbill = $cbill;
        return $this;
    }

    public function __toString()
    {
    return $this->numerofattura;
    }




}

