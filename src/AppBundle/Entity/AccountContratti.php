<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AccountContratti
 *
 * @ORM\Table(name="accounts_contratti", indexes={@ORM\Index(name="idx_a_o_opp_acc_del", columns={"contratto_id", "account_id", "deleted"}), @ORM\Index(name="idx_acc_opp_acc", columns={"account_id"}), @ORM\Index(name="idx_acc_opp_opp", columns={"contratto_id"}), @ORM\Index(name="idx_account_contratto", columns={"account_id", "contratto_id"})})
 * @ORM\Entity
 */
class AccountContratti
{

    /**
     * @var Account
     *
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Account", inversedBy="accountContratti")
     * @ORM\JoinColumn(name="account_id", referencedColumnName="id")
     */
    private $account;


    /**
     * @var Contratti
     *
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Contratti", inversedBy="accountContratti")
     * @ORM\JoinColumn(name="contratto_id", referencedColumnName="id")
     */
    private $contratti;



    /**
     * @var string
     *
     * @ORM\Column(name="id", type="string", length=36, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id = '\'\'';

    /**
     * @var string
     *
     * @ORM\Column(name="contratto_id", type="string", length=36, nullable=true)
     */
    private $contrattoId = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="account_id", type="string", length=36, nullable=true)
     */
    private $accountId = 'NULL';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_modified", type="datetime", nullable=true)
     */
    private $dateModified = 'NULL';

    /**
     * @var boolean
     *
     * @ORM\Column(name="deleted", type="boolean", nullable=false)
     */
    private $deleted = '0';

    /**
     * @return Contratti
     */
    public function getContratti()
    {
        return $this->contratti;
    }

    /**
     * @param Contratti $contratti
     * @return AccountContratti
     */
    public function setContratti($contratti)
    {
        $this->contratti = $contratti;
        return $this;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return AccountContratti
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getContrattoId()
    {
        return $this->contrattoId;
    }

    /**
     * @param string $contrattoId
     * @return AccountContratti
     */
    public function setContrattoId($contrattoId)
    {
        $this->contrattoId = $contrattoId;
        return $this;
    }

    /**
     * @return string
     */
    public function getAccountId()
    {
        return $this->accountId;
    }

    /**
     * @param string $accountId
     * @return AccountContratti
     */
    public function setAccountId($accountId)
    {
        $this->accountId = $accountId;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateModified()
    {
        return $this->dateModified;
    }

    /**
     * @param \DateTime $dateModified
     * @return AccountContratti
     */
    public function setDateModified($dateModified)
    {
        $this->dateModified = $dateModified;
        return $this;
    }

    /**
     * @return bool
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * @param bool $deleted
     * @return AccountContratti
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;
        return $this;
    }

    /**
     * @return Account
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * @param Account $account
     * @return AccountContratti
     */
    public function setAccount($account)
    {
        $this->account = $account;
        return $this;
    }



    public function __toString()
    {
        return $this->accountId;
    }


}

