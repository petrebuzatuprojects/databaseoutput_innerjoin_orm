<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * AccountCstm
 *
 * @ORM\Table(name="accounts_cstm", indexes={@ORM\Index(name="id_c", columns={"id_c"}), @ORM\Index(name="IDX_accounts_cstm", columns={"AccountType_c", "ExportInGamma_c"}), @ORM\Index(name="IDX_accounts_cstm_AccountType_c", columns={"AccountType_c"}), @ORM\Index(name="IDX_accounts_cstm_CodiceDebitore_c", columns={"CodiceDebitore_c"}), @ORM\Index(name="IDX_accounts_cstm_CodiceDepositoGamma_c", columns={"CodiceDepositoGamma_c"}), @ORM\Index(name="IDX_accounts_cstm_CodiceFiscale_c", columns={"CodiceFiscale_c"}), @ORM\Index(name="IDX_accounts_cstm_user_id", columns={"user_id"}), @ORM\Index(name="IDX_accounts_cstm2", columns={"Mag_DepositoPrincipale_c", "user_id"}), @ORM\Index(name="IDX_accounts_cstm3", columns={"AccountType_c", "CodiceFiscale_c", "id_c"}), @ORM\Index(name="IDX_accounts_cstm4", columns={"id_c", "checkcoverage_c", "datecheckcoverage_c"}), @ORM\Index(name="IDX_accounts_cstm5", columns={"id_c", "checkcoverage_c"}), @ORM\Index(name="IDX_accounts_cstm6", columns={"coverage_type_c"}), @ORM\Index(name="UK_accounts_utentiie", columns={"UtenteIE_c"})})
 * @ORM\Entity
 */
class AccountCstm
{


    /**
     * @var Account
     *
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Account", inversedBy="AccountCstm")
     * @ORM\JoinColumn(name="id_c", referencedColumnName="id")
     */
    private $account;

    /**
     * @var string
     *
     * @ORM\Column(name="id_c", type="string", length=36, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $idC = '\'\'';

    /**
     * @var string
     *
     * @ORM\Column(name="CodiceFiscale_c", type="string", length=16, nullable=false)
     */
    private $codicefiscaleC;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="OrderDate_c", type="date", nullable=true)
     */
    private $orderdateC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="TipoContratto_c", type="string", length=45, nullable=false)
     */
    private $tipocontrattoC = '\'Entry\'';

    /**
     * @var string
     *
     * @ORM\Column(name="LBLDatiContratto_c", type="string", length=50, nullable=true)
     */
    private $lbldaticontrattoC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="TipoPagAntenna_c", type="string", length=25, nullable=false)
     */
    private $tipopagantennaC = '\'Canone\'';

    /**
     * @var float
     *
     * @ORM\Column(name="ImportoAntenna_c", type="float", precision=10, scale=0, nullable=true)
     */
    private $importoantennaC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="TipoAntenna_c", type="string", length=50, nullable=false)
     */
    private $tipoantennaC = '\'TipoA\'';

    /**
     * @var float
     *
     * @ORM\Column(name="ContributoAttivazione_c", type="float", precision=10, scale=0, nullable=false)
     */
    private $contributoattivazioneC = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="InstallazioneRichiesta_c", type="boolean", nullable=true)
     */
    private $installazionerichiestaC = 'NULL';

    /**
     * @var float
     *
     * @ORM\Column(name="InstallazioneContributo_c", type="float", precision=10, scale=0, nullable=true)
     */
    private $installazionecontributoC = 'NULL';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DataAttivazione_c", type="date", nullable=true)
     */
    private $dataattivazioneC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="CodiceAntenna_c", type="string", length=50, nullable=false)
     */
    private $codiceantennaC = '\'\'';

    /**
     * @var string
     *
     * @ORM\Column(name="IPAntenna_c", type="string", length=15, nullable=false)
     */
    private $ipantennaC = '\'\'';

    /**
     * @var string
     *
     * @ORM\Column(name="Azienda_c", type="string", length=25, nullable=false)
     */
    private $aziendaC = '\'wavemax\'';

    /**
     * @var string
     *
     * @ORM\Column(name="AccountDescription_c", type="string", length=255, nullable=true)
     */
    private $accountdescriptionC = 'NULL';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DataInstallazione_c", type="date", nullable=true)
     */
    private $datainstallazioneC = 'NULL';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DataSpedizione_c", type="date", nullable=true)
     */
    private $dataspedizioneC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="CodiceCliente_c", type="string", length=15, nullable=true)
     */
    private $codiceclienteC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="AccountType_c", type="string", length=25, nullable=false)
     */
    private $accounttypeC = '\'\'';

    /**
     * @var float
     *
     * @ORM\Column(name="CanoneMensileServizio_c", type="float", precision=10, scale=0, nullable=false)
     */
    private $canonemensileservizioC = '0';

    /**
     * @var float
     *
     * @ORM\Column(name="CanoneMensileAntenna_c", type="float", precision=10, scale=0, nullable=true)
     */
    private $canonemensileantennaC = 'NULL';

    /**
     * @var float
     *
     * @ORM\Column(name="IP_Statico_Numero_c", type="float", precision=10, scale=0, nullable=true)
     */
    private $ipStaticoNumeroC = 'NULL';

    /**
     * @var float
     *
     * @ORM\Column(name="CanoneIPStatici_c", type="float", precision=10, scale=0, nullable=true)
     */
    private $canoneipstaticiC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="AccountStatus_c", type="string", length=20, nullable=false)
     */
    private $accountstatusC = '\'Attivo\'';

    /**
     * @var string
     *
     * @ORM\Column(name="FreeCatalogKey_c", type="string", length=50, nullable=true)
     */
    private $freecatalogkeyC = 'NULL';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DataInizioAssistenza_c", type="date", nullable=true)
     */
    private $datainizioassistenzaC = 'NULL';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DataFineAssistenza_c", type="date", nullable=true)
     */
    private $datafineassistenzaC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="ZonaCommerciale_c", type="string", length=10, nullable=true)
     */
    private $zonacommercialeC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="AccountNotes_c", type="text", length=65535, nullable=true)
     */
    private $accountnotesC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="shipping_address_regione_c", type="string", length=20, nullable=true)
     */
    private $shippingAddressRegioneC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="billing_address_regione_c_c", type="string", length=20, nullable=true)
     */
    private $billingAddressRegioneCC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="AccountClasse_c", type="string", length=10, nullable=true)
     */
    private $accountclasseC = 'NULL';

    /**
     * @var boolean
     *
     * @ORM\Column(name="Account_Voip_c", type="boolean", nullable=true)
     */
    private $accountVoipC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="Network_c", type="string", length=10, nullable=true)
     */
    private $networkC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="UtenteIE_c", type="string", length=8, nullable=true)
     */
    private $utenteieC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="PasswordIE_c", type="string", length=8, nullable=true)
     */
    private $passwordieC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="BancaRid_c", type="string", length=20, nullable=true)
     */
    private $bancaridC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="MBAN_Nomignolo_c", type="string", length=7, nullable=true)
     */
    private $mbanNomignoloC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="DocumentoIdentita_c", type="string", length=3, nullable=true)
     */
    private $documentoidentitaC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="RilasciatoDa_c", type="string", length=3, nullable=true)
     */
    private $rilasciatodaC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="NumeroDocumento_c", type="string", length=50, nullable=true)
     */
    private $numerodocumentoC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="LuogoRilascioDocumento_c", type="string", length=255, nullable=true)
     */
    private $luogorilasciodocumentoC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="DataRilascioDocumento_c", type="string", length=255, nullable=true)
     */
    private $datarilasciodocumentoC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="Shipping_Nazione_c", type="string", length=100, nullable=true)
     */
    private $shippingNazioneC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="Sesso_c", type="string", length=2, nullable=true)
     */
    private $sessoC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="RL_Cognome_c", type="string", length=255, nullable=true)
     */
    private $rlCognomeC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="RL_Nome_c", type="string", length=255, nullable=true)
     */
    private $rlNomeC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="RL_Sesso_c", type="string", length=1, nullable=true)
     */
    private $rlSessoC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="RL_Provincia_c", type="string", length=255, nullable=true)
     */
    private $rlProvinciaC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="RL_Nazione_c", type="string", length=200, nullable=true)
     */
    private $rlNazioneC = 'NULL';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="RL_DataNascita_c", type="date", nullable=true)
     */
    private $rlDatanascitaC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="RL_LuogoNascita_c", type="string", length=255, nullable=true)
     */
    private $rlLuogonascitaC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="RL_CF_c", type="string", length=16, nullable=true)
     */
    private $rlCfC = 'NULL';

    /**
     * @var boolean
     *
     * @ORM\Column(name="check_password_changed_c", type="boolean", nullable=true)
     */
    private $checkPasswordChangedC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="ComuneNascita_c", type="string", length=100, nullable=true)
     */
    private $comunenascitaC = 'NULL';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DataNascita_c", type="date", nullable=true)
     */
    private $datanascitaC = 'NULL';

    /**
     * @var integer
     *
     * @ORM\Column(name="iddomanda_c", type="integer", nullable=true)
     */
    private $iddomandaC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="Uid1_c", type="string", length=255, nullable=true)
     */
    private $uid1C = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="Uid2_c", type="string", length=255, nullable=true)
     */
    private $uid2C = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="Uid3_c", type="string", length=255, nullable=true)
     */
    private $uid3C = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="NazioneNascita_c", type="string", length=100, nullable=true)
     */
    private $nazionenascitaC = 'NULL';

    /**
     * @var integer
     *
     * @ORM\Column(name="NumCasellePosta_c", type="integer", nullable=false)
     */
    private $numcasellepostaC = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="ProvNascita_c", type="string", length=100, nullable=true)
     */
    private $provnascitaC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="risposta_c", type="string", length=255, nullable=true)
     */
    private $rispostaC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="CodiceCCIAA_c", type="string", length=20, nullable=true)
     */
    private $codicecciaaC = 'NULL';

    /**
     * @var boolean
     *
     * @ORM\Column(name="accettazione_privacy_c", type="boolean", nullable=false)
     */
    private $accettazionePrivacyC = '0';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DataExport_c", type="datetime", nullable=true)
     */
    private $dataexportC = 'NULL';

    /**
     * @var float
     *
     * @ORM\Column(name="SpeseVarie_c", type="float", precision=10, scale=0, nullable=true)
     */
    private $spesevarieC = '1.5';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="ExportInGamma_c", type="datetime", nullable=true)
     */
    private $exportingammaC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="CodiceDepositoGamma_c", type="string", length=2, nullable=true)
     */
    private $codicedepositogammaC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="user_id", type="string", length=36, nullable=true)
     */
    private $userId = 'NULL';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="ExportDepInGamma_c", type="datetime", nullable=true)
     */
    private $exportdepingammaC = 'NULL';

    /**
     * @var integer
     *
     * @ORM\Column(name="iSepaRichiestaInfo", type="integer", nullable=false)
     */
    private $iseparichiestainfo = '0';

    /**
     * @var integer
     *
     * @ORM\Column(name="iSepaTipoCliente", type="integer", nullable=true)
     */
    private $isepatipocliente = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="ISepaIBAN", type="string", length=30, nullable=true)
     */
    private $isepaiban = 'NULL';

    /**
     * @var integer
     *
     * @ORM\Column(name="ISepaFacoltaStorno", type="integer", nullable=true)
     */
    private $isepafacoltastorno = 'NULL';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="ISepaDataSottoscrizione", type="date", nullable=true)
     */
    private $isepadatasottoscrizione = 'NULL';

    /**
     * @var integer
     *
     * @ORM\Column(name="ISepaMotivazioneDiniego", type="integer", nullable=true)
     */
    private $isepamotivazionediniego = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="ISepaNuovaBanca", type="string", length=5, nullable=true)
     */
    private $isepanuovabanca = 'NULL';

    /**
     * @var integer
     *
     * @ORM\Column(name="Mag_DepositoPrincipale_c", type="integer", nullable=false)
     */
    private $magDepositoprincipaleC = '1';

    /**
     * @var string
     *
     * @ORM\Column(name="Rid_Sottoscrittore_c", type="string", length=45, nullable=true)
     */
    private $ridSottoscrittoreC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="Rid_Sottoscrittore_Indirizzo_c", type="string", length=255, nullable=true)
     */
    private $ridSottoscrittoreIndirizzoC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="Rid_Sottoscrittore_CodiceFiscale_c", type="string", length=16, nullable=true)
     */
    private $ridSottoscrittoreCodicefiscaleC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="Rid_Sottoscrittore_Localita_c", type="string", length=255, nullable=true)
     */
    private $ridSottoscrittoreLocalitaC = 'NULL';

    /**
     * @var integer
     *
     * @ORM\Column(name="CoreB2B", type="integer", nullable=true)
     */
    private $coreb2b = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="TipoCodiceRID_c", type="string", length=5, nullable=true)
     */
    private $tipocodiceridC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="CodiceDebitore_c", type="string", length=16, nullable=true)
     */
    private $codicedebitoreC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="TipoCliente_c", type="string", length=2, nullable=true)
     */
    private $tipoclienteC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="FacoltaStorno_c", type="string", length=2, nullable=true)
     */
    private $facoltastornoC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="CodiceIndividuale_c", type="string", length=2, nullable=true)
     */
    private $codiceindividualeC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="IBAN_c", type="string", length=40, nullable=true)
     */
    private $ibanC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="TipoSequenzaIncasso_c", type="string", length=2, nullable=true)
     */
    private $tiposequenzaincassoC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="StatusRID_c", type="string", length=2, nullable=true)
     */
    private $statusridC = '\'K0\'';

    /**
     * @var string
     *
     * @ORM\Column(name="TipoIncassoSDD_c", type="string", length=2, nullable=true)
     */
    private $tipoincassosddC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="Sottoscrittore_CF_c", type="string", length=16, nullable=true)
     */
    private $sottoscrittoreCfC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="Sottoscrittore_Indirizzo_c", type="string", length=255, nullable=true)
     */
    private $sottoscrittoreIndirizzoC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="Sottoscrittore_c", type="string", length=150, nullable=true)
     */
    private $sottoscrittoreC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="Sottoscrittore_Localita_c", type="string", length=255, nullable=true)
     */
    private $sottoscrittoreLocalitaC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="SubDealerCode_c", type="string", length=50, nullable=true)
     */
    private $subdealercodeC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="SubDealerReportToId_c", type="string", length=36, nullable=true)
     */
    private $subdealerreporttoidC = 'NULL';

    /**
     * @var boolean
     *
     * @ORM\Column(name="checkcoverage_c", type="boolean", nullable=true)
     */
    private $checkcoverageC = '0';

    /**
     * @var boolean
     *
     * @ORM\Column(name="coverage_type_c", type="boolean", nullable=true)
     */
    private $coverageTypeC = 'NULL';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datecheckcoverage_c", type="date", nullable=true)
     */
    private $datecheckcoverageC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="ftel_cod_uni_ou_c", type="string", length=255, nullable=true)
     */
    private $ftelCodUniOuC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="ftel_indirizzopec_c", type="string", length=150, nullable=true)
     */
    private $ftelIndirizzopecC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="codicegamma_c", type="decimal", precision=8, scale=0, nullable=true)
     */
    private $codicegammaC = 'NULL';

    /**
     * @var integer
     *
     * @ORM\Column(name="CG1M_CODICE_CG16_c", type="integer", nullable=true)
     */
    private $cg1mCodiceCg16C = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="latitudine_c", type="decimal", precision=18, scale=9, nullable=true)
     */
    private $latitudineC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="longitudine_c", type="decimal", precision=18, scale=9, nullable=true)
     */
    private $longitudineC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="returnedstreet_c", type="string", length=1024, nullable=true)
     */
    private $returnedstreetC = 'NULL';

    /**
     * @var boolean
     *
     * @ORM\Column(name="fatturazioneunificata_c", type="boolean", nullable=true)
     */
    private $fatturazioneunificataC = 'NULL';

    /**
     * @var boolean
     *
     * @ORM\Column(name="includifeesubdealer_c", type="boolean", nullable=true)
     */
    private $includifeesubdealerC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="operatore_c", type="string", length=50, nullable=true)
     */
    private $operatoreC = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="codiceaffiliato_c", type="string", length=75, nullable=true)
     */
    private $codiceaffiliatoC = 'NULL';

    /**
     * @return Account
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * @param Account $account
     * @return AccountCstm
     */
    public function setAccount($account)
    {
        $this->account = $account;
        return $this;
    }

    /**
     * @return string
     */
    public function getIdC()
    {
        return $this->idC;
    }

    /**
     * @param string $idC
     * @return AccountCstm
     */
    public function setIdC($idC)
    {
        $this->idC = $idC;
        return $this;
    }

    /**
     * @return string
     */
    public function getCodicefiscaleC()
    {
        return $this->codicefiscaleC;
    }

    /**
     * @param string $codicefiscaleC
     * @return AccountCstm
     */
    public function setCodicefiscaleC($codicefiscaleC)
    {
        $this->codicefiscaleC = $codicefiscaleC;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getOrderdateC()
    {
        return $this->orderdateC;
    }

    /**
     * @param \DateTime $orderdateC
     * @return AccountCstm
     */
    public function setOrderdateC($orderdateC)
    {
        $this->orderdateC = $orderdateC;
        return $this;
    }

    /**
     * @return string
     */
    public function getTipocontrattoC()
    {
        return $this->tipocontrattoC;
    }

    /**
     * @param string $tipocontrattoC
     * @return AccountCstm
     */
    public function setTipocontrattoC($tipocontrattoC)
    {
        $this->tipocontrattoC = $tipocontrattoC;
        return $this;
    }

    /**
     * @return string
     */
    public function getLbldaticontrattoC()
    {
        return $this->lbldaticontrattoC;
    }

    /**
     * @param string $lbldaticontrattoC
     * @return AccountCstm
     */
    public function setLbldaticontrattoC($lbldaticontrattoC)
    {
        $this->lbldaticontrattoC = $lbldaticontrattoC;
        return $this;
    }

    /**
     * @return string
     */
    public function getTipopagantennaC()
    {
        return $this->tipopagantennaC;
    }

    /**
     * @param string $tipopagantennaC
     * @return AccountCstm
     */
    public function setTipopagantennaC($tipopagantennaC)
    {
        $this->tipopagantennaC = $tipopagantennaC;
        return $this;
    }

    /**
     * @return float
     */
    public function getImportoantennaC()
    {
        return $this->importoantennaC;
    }

    /**
     * @param float $importoantennaC
     * @return AccountCstm
     */
    public function setImportoantennaC($importoantennaC)
    {
        $this->importoantennaC = $importoantennaC;
        return $this;
    }

    /**
     * @return string
     */
    public function getTipoantennaC()
    {
        return $this->tipoantennaC;
    }

    /**
     * @param string $tipoantennaC
     * @return AccountCstm
     */
    public function setTipoantennaC($tipoantennaC)
    {
        $this->tipoantennaC = $tipoantennaC;
        return $this;
    }

    /**
     * @return float
     */
    public function getContributoattivazioneC()
    {
        return $this->contributoattivazioneC;
    }

    /**
     * @param float $contributoattivazioneC
     * @return AccountCstm
     */
    public function setContributoattivazioneC($contributoattivazioneC)
    {
        $this->contributoattivazioneC = $contributoattivazioneC;
        return $this;
    }

    /**
     * @return bool
     */
    public function isInstallazionerichiestaC()
    {
        return $this->installazionerichiestaC;
    }

    /**
     * @param bool $installazionerichiestaC
     * @return AccountCstm
     */
    public function setInstallazionerichiestaC($installazionerichiestaC)
    {
        $this->installazionerichiestaC = $installazionerichiestaC;
        return $this;
    }

    /**
     * @return float
     */
    public function getInstallazionecontributoC()
    {
        return $this->installazionecontributoC;
    }

    /**
     * @param float $installazionecontributoC
     * @return AccountCstm
     */
    public function setInstallazionecontributoC($installazionecontributoC)
    {
        $this->installazionecontributoC = $installazionecontributoC;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDataattivazioneC()
    {
        return $this->dataattivazioneC;
    }

    /**
     * @param \DateTime $dataattivazioneC
     * @return AccountCstm
     */
    public function setDataattivazioneC($dataattivazioneC)
    {
        $this->dataattivazioneC = $dataattivazioneC;
        return $this;
    }

    /**
     * @return string
     */
    public function getCodiceantennaC()
    {
        return $this->codiceantennaC;
    }

    /**
     * @param string $codiceantennaC
     * @return AccountCstm
     */
    public function setCodiceantennaC($codiceantennaC)
    {
        $this->codiceantennaC = $codiceantennaC;
        return $this;
    }

    /**
     * @return string
     */
    public function getIpantennaC()
    {
        return $this->ipantennaC;
    }

    /**
     * @param string $ipantennaC
     * @return AccountCstm
     */
    public function setIpantennaC($ipantennaC)
    {
        $this->ipantennaC = $ipantennaC;
        return $this;
    }

    /**
     * @return string
     */
    public function getAziendaC()
    {
        return $this->aziendaC;
    }

    /**
     * @param string $aziendaC
     * @return AccountCstm
     */
    public function setAziendaC($aziendaC)
    {
        $this->aziendaC = $aziendaC;
        return $this;
    }

    /**
     * @return string
     */
    public function getAccountdescriptionC()
    {
        return $this->accountdescriptionC;
    }

    /**
     * @param string $accountdescriptionC
     * @return AccountCstm
     */
    public function setAccountdescriptionC($accountdescriptionC)
    {
        $this->accountdescriptionC = $accountdescriptionC;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDatainstallazioneC()
    {
        return $this->datainstallazioneC;
    }

    /**
     * @param \DateTime $datainstallazioneC
     * @return AccountCstm
     */
    public function setDatainstallazioneC($datainstallazioneC)
    {
        $this->datainstallazioneC = $datainstallazioneC;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDataspedizioneC()
    {
        return $this->dataspedizioneC;
    }

    /**
     * @param \DateTime $dataspedizioneC
     * @return AccountCstm
     */
    public function setDataspedizioneC($dataspedizioneC)
    {
        $this->dataspedizioneC = $dataspedizioneC;
        return $this;
    }

    /**
     * @return string
     */
    public function getCodiceclienteC()
    {
        return $this->codiceclienteC;
    }

    /**
     * @param string $codiceclienteC
     * @return AccountCstm
     */
    public function setCodiceclienteC($codiceclienteC)
    {
        $this->codiceclienteC = $codiceclienteC;
        return $this;
    }

    /**
     * @return string
     */
    public function getAccounttypeC()
    {
        return $this->accounttypeC;
    }

    /**
     * @param string $accounttypeC
     * @return AccountCstm
     */
    public function setAccounttypeC($accounttypeC)
    {
        $this->accounttypeC = $accounttypeC;
        return $this;
    }

    /**
     * @return float
     */
    public function getCanonemensileservizioC()
    {
        return $this->canonemensileservizioC;
    }

    /**
     * @param float $canonemensileservizioC
     * @return AccountCstm
     */
    public function setCanonemensileservizioC($canonemensileservizioC)
    {
        $this->canonemensileservizioC = $canonemensileservizioC;
        return $this;
    }

    /**
     * @return float
     */
    public function getCanonemensileantennaC()
    {
        return $this->canonemensileantennaC;
    }

    /**
     * @param float $canonemensileantennaC
     * @return AccountCstm
     */
    public function setCanonemensileantennaC($canonemensileantennaC)
    {
        $this->canonemensileantennaC = $canonemensileantennaC;
        return $this;
    }

    /**
     * @return float
     */
    public function getIpStaticoNumeroC()
    {
        return $this->ipStaticoNumeroC;
    }

    /**
     * @param float $ipStaticoNumeroC
     * @return AccountCstm
     */
    public function setIpStaticoNumeroC($ipStaticoNumeroC)
    {
        $this->ipStaticoNumeroC = $ipStaticoNumeroC;
        return $this;
    }

    /**
     * @return float
     */
    public function getCanoneipstaticiC()
    {
        return $this->canoneipstaticiC;
    }

    /**
     * @param float $canoneipstaticiC
     * @return AccountCstm
     */
    public function setCanoneipstaticiC($canoneipstaticiC)
    {
        $this->canoneipstaticiC = $canoneipstaticiC;
        return $this;
    }

    /**
     * @return string
     */
    public function getAccountstatusC()
    {
        return $this->accountstatusC;
    }

    /**
     * @param string $accountstatusC
     * @return AccountCstm
     */
    public function setAccountstatusC($accountstatusC)
    {
        $this->accountstatusC = $accountstatusC;
        return $this;
    }

    /**
     * @return string
     */
    public function getFreecatalogkeyC()
    {
        return $this->freecatalogkeyC;
    }

    /**
     * @param string $freecatalogkeyC
     * @return AccountCstm
     */
    public function setFreecatalogkeyC($freecatalogkeyC)
    {
        $this->freecatalogkeyC = $freecatalogkeyC;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDatainizioassistenzaC()
    {
        return $this->datainizioassistenzaC;
    }

    /**
     * @param \DateTime $datainizioassistenzaC
     * @return AccountCstm
     */
    public function setDatainizioassistenzaC($datainizioassistenzaC)
    {
        $this->datainizioassistenzaC = $datainizioassistenzaC;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDatafineassistenzaC()
    {
        return $this->datafineassistenzaC;
    }

    /**
     * @param \DateTime $datafineassistenzaC
     * @return AccountCstm
     */
    public function setDatafineassistenzaC($datafineassistenzaC)
    {
        $this->datafineassistenzaC = $datafineassistenzaC;
        return $this;
    }

    /**
     * @return string
     */
    public function getZonacommercialeC()
    {
        return $this->zonacommercialeC;
    }

    /**
     * @param string $zonacommercialeC
     * @return AccountCstm
     */
    public function setZonacommercialeC($zonacommercialeC)
    {
        $this->zonacommercialeC = $zonacommercialeC;
        return $this;
    }

    /**
     * @return string
     */
    public function getAccountnotesC()
    {
        return $this->accountnotesC;
    }

    /**
     * @param string $accountnotesC
     * @return AccountCstm
     */
    public function setAccountnotesC($accountnotesC)
    {
        $this->accountnotesC = $accountnotesC;
        return $this;
    }

    /**
     * @return string
     */
    public function getShippingAddressRegioneC()
    {
        return $this->shippingAddressRegioneC;
    }

    /**
     * @param string $shippingAddressRegioneC
     * @return AccountCstm
     */
    public function setShippingAddressRegioneC($shippingAddressRegioneC)
    {
        $this->shippingAddressRegioneC = $shippingAddressRegioneC;
        return $this;
    }

    /**
     * @return string
     */
    public function getBillingAddressRegioneCC()
    {
        return $this->billingAddressRegioneCC;
    }

    /**
     * @param string $billingAddressRegioneCC
     * @return AccountCstm
     */
    public function setBillingAddressRegioneCC($billingAddressRegioneCC)
    {
        $this->billingAddressRegioneCC = $billingAddressRegioneCC;
        return $this;
    }

    /**
     * @return string
     */
    public function getAccountclasseC()
    {
        return $this->accountclasseC;
    }

    /**
     * @param string $accountclasseC
     * @return AccountCstm
     */
    public function setAccountclasseC($accountclasseC)
    {
        $this->accountclasseC = $accountclasseC;
        return $this;
    }

    /**
     * @return bool
     */
    public function isAccountVoipC()
    {
        return $this->accountVoipC;
    }

    /**
     * @param bool $accountVoipC
     * @return AccountCstm
     */
    public function setAccountVoipC($accountVoipC)
    {
        $this->accountVoipC = $accountVoipC;
        return $this;
    }

    /**
     * @return string
     */
    public function getNetworkC()
    {
        return $this->networkC;
    }

    /**
     * @param string $networkC
     * @return AccountCstm
     */
    public function setNetworkC($networkC)
    {
        $this->networkC = $networkC;
        return $this;
    }

    /**
     * @return string
     */
    public function getUtenteieC()
    {
        return $this->utenteieC;
    }

    /**
     * @param string $utenteieC
     * @return AccountCstm
     */
    public function setUtenteieC($utenteieC)
    {
        $this->utenteieC = $utenteieC;
        return $this;
    }

    /**
     * @return string
     */
    public function getPasswordieC()
    {
        return $this->passwordieC;
    }

    /**
     * @param string $passwordieC
     * @return AccountCstm
     */
    public function setPasswordieC($passwordieC)
    {
        $this->passwordieC = $passwordieC;
        return $this;
    }

    /**
     * @return string
     */
    public function getBancaridC()
    {
        return $this->bancaridC;
    }

    /**
     * @param string $bancaridC
     * @return AccountCstm
     */
    public function setBancaridC($bancaridC)
    {
        $this->bancaridC = $bancaridC;
        return $this;
    }

    /**
     * @return string
     */
    public function getMbanNomignoloC()
    {
        return $this->mbanNomignoloC;
    }

    /**
     * @param string $mbanNomignoloC
     * @return AccountCstm
     */
    public function setMbanNomignoloC($mbanNomignoloC)
    {
        $this->mbanNomignoloC = $mbanNomignoloC;
        return $this;
    }

    /**
     * @return string
     */
    public function getDocumentoidentitaC()
    {
        return $this->documentoidentitaC;
    }

    /**
     * @param string $documentoidentitaC
     * @return AccountCstm
     */
    public function setDocumentoidentitaC($documentoidentitaC)
    {
        $this->documentoidentitaC = $documentoidentitaC;
        return $this;
    }

    /**
     * @return string
     */
    public function getRilasciatodaC()
    {
        return $this->rilasciatodaC;
    }

    /**
     * @param string $rilasciatodaC
     * @return AccountCstm
     */
    public function setRilasciatodaC($rilasciatodaC)
    {
        $this->rilasciatodaC = $rilasciatodaC;
        return $this;
    }

    /**
     * @return string
     */
    public function getNumerodocumentoC()
    {
        return $this->numerodocumentoC;
    }

    /**
     * @param string $numerodocumentoC
     * @return AccountCstm
     */
    public function setNumerodocumentoC($numerodocumentoC)
    {
        $this->numerodocumentoC = $numerodocumentoC;
        return $this;
    }

    /**
     * @return string
     */
    public function getLuogorilasciodocumentoC()
    {
        return $this->luogorilasciodocumentoC;
    }

    /**
     * @param string $luogorilasciodocumentoC
     * @return AccountCstm
     */
    public function setLuogorilasciodocumentoC($luogorilasciodocumentoC)
    {
        $this->luogorilasciodocumentoC = $luogorilasciodocumentoC;
        return $this;
    }

    /**
     * @return string
     */
    public function getDatarilasciodocumentoC()
    {
        return $this->datarilasciodocumentoC;
    }

    /**
     * @param string $datarilasciodocumentoC
     * @return AccountCstm
     */
    public function setDatarilasciodocumentoC($datarilasciodocumentoC)
    {
        $this->datarilasciodocumentoC = $datarilasciodocumentoC;
        return $this;
    }

    /**
     * @return string
     */
    public function getShippingNazioneC()
    {
        return $this->shippingNazioneC;
    }

    /**
     * @param string $shippingNazioneC
     * @return AccountCstm
     */
    public function setShippingNazioneC($shippingNazioneC)
    {
        $this->shippingNazioneC = $shippingNazioneC;
        return $this;
    }

    /**
     * @return string
     */
    public function getSessoC()
    {
        return $this->sessoC;
    }

    /**
     * @param string $sessoC
     * @return AccountCstm
     */
    public function setSessoC($sessoC)
    {
        $this->sessoC = $sessoC;
        return $this;
    }

    /**
     * @return string
     */
    public function getRlCognomeC()
    {
        return $this->rlCognomeC;
    }

    /**
     * @param string $rlCognomeC
     * @return AccountCstm
     */
    public function setRlCognomeC($rlCognomeC)
    {
        $this->rlCognomeC = $rlCognomeC;
        return $this;
    }

    /**
     * @return string
     */
    public function getRlNomeC()
    {
        return $this->rlNomeC;
    }

    /**
     * @param string $rlNomeC
     * @return AccountCstm
     */
    public function setRlNomeC($rlNomeC)
    {
        $this->rlNomeC = $rlNomeC;
        return $this;
    }

    /**
     * @return string
     */
    public function getRlSessoC()
    {
        return $this->rlSessoC;
    }

    /**
     * @param string $rlSessoC
     * @return AccountCstm
     */
    public function setRlSessoC($rlSessoC)
    {
        $this->rlSessoC = $rlSessoC;
        return $this;
    }

    /**
     * @return string
     */
    public function getRlProvinciaC()
    {
        return $this->rlProvinciaC;
    }

    /**
     * @param string $rlProvinciaC
     * @return AccountCstm
     */
    public function setRlProvinciaC($rlProvinciaC)
    {
        $this->rlProvinciaC = $rlProvinciaC;
        return $this;
    }

    /**
     * @return string
     */
    public function getRlNazioneC()
    {
        return $this->rlNazioneC;
    }

    /**
     * @param string $rlNazioneC
     * @return AccountCstm
     */
    public function setRlNazioneC($rlNazioneC)
    {
        $this->rlNazioneC = $rlNazioneC;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getRlDatanascitaC()
    {
        return $this->rlDatanascitaC;
    }

    /**
     * @param \DateTime $rlDatanascitaC
     * @return AccountCstm
     */
    public function setRlDatanascitaC($rlDatanascitaC)
    {
        $this->rlDatanascitaC = $rlDatanascitaC;
        return $this;
    }

    /**
     * @return string
     */
    public function getRlLuogonascitaC()
    {
        return $this->rlLuogonascitaC;
    }

    /**
     * @param string $rlLuogonascitaC
     * @return AccountCstm
     */
    public function setRlLuogonascitaC($rlLuogonascitaC)
    {
        $this->rlLuogonascitaC = $rlLuogonascitaC;
        return $this;
    }

    /**
     * @return string
     */
    public function getRlCfC()
    {
        return $this->rlCfC;
    }

    /**
     * @param string $rlCfC
     * @return AccountCstm
     */
    public function setRlCfC($rlCfC)
    {
        $this->rlCfC = $rlCfC;
        return $this;
    }

    /**
     * @return bool
     */
    public function isCheckPasswordChangedC()
    {
        return $this->checkPasswordChangedC;
    }

    /**
     * @param bool $checkPasswordChangedC
     * @return AccountCstm
     */
    public function setCheckPasswordChangedC($checkPasswordChangedC)
    {
        $this->checkPasswordChangedC = $checkPasswordChangedC;
        return $this;
    }

    /**
     * @return string
     */
    public function getComunenascitaC()
    {
        return $this->comunenascitaC;
    }

    /**
     * @param string $comunenascitaC
     * @return AccountCstm
     */
    public function setComunenascitaC($comunenascitaC)
    {
        $this->comunenascitaC = $comunenascitaC;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDatanascitaC()
    {
        return $this->datanascitaC;
    }

    /**
     * @param \DateTime $datanascitaC
     * @return AccountCstm
     */
    public function setDatanascitaC($datanascitaC)
    {
        $this->datanascitaC = $datanascitaC;
        return $this;
    }

    /**
     * @return int
     */
    public function getIddomandaC()
    {
        return $this->iddomandaC;
    }

    /**
     * @param int $iddomandaC
     * @return AccountCstm
     */
    public function setIddomandaC($iddomandaC)
    {
        $this->iddomandaC = $iddomandaC;
        return $this;
    }

    /**
     * @return string
     */
    public function getUid1C()
    {
        return $this->uid1C;
    }

    /**
     * @param string $uid1C
     * @return AccountCstm
     */
    public function setUid1C($uid1C)
    {
        $this->uid1C = $uid1C;
        return $this;
    }

    /**
     * @return string
     */
    public function getUid2C()
    {
        return $this->uid2C;
    }

    /**
     * @param string $uid2C
     * @return AccountCstm
     */
    public function setUid2C($uid2C)
    {
        $this->uid2C = $uid2C;
        return $this;
    }

    /**
     * @return string
     */
    public function getUid3C()
    {
        return $this->uid3C;
    }

    /**
     * @param string $uid3C
     * @return AccountCstm
     */
    public function setUid3C($uid3C)
    {
        $this->uid3C = $uid3C;
        return $this;
    }

    /**
     * @return string
     */
    public function getNazionenascitaC()
    {
        return $this->nazionenascitaC;
    }

    /**
     * @param string $nazionenascitaC
     * @return AccountCstm
     */
    public function setNazionenascitaC($nazionenascitaC)
    {
        $this->nazionenascitaC = $nazionenascitaC;
        return $this;
    }

    /**
     * @return int
     */
    public function getNumcasellepostaC()
    {
        return $this->numcasellepostaC;
    }

    /**
     * @param int $numcasellepostaC
     * @return AccountCstm
     */
    public function setNumcasellepostaC($numcasellepostaC)
    {
        $this->numcasellepostaC = $numcasellepostaC;
        return $this;
    }

    /**
     * @return string
     */
    public function getProvnascitaC()
    {
        return $this->provnascitaC;
    }

    /**
     * @param string $provnascitaC
     * @return AccountCstm
     */
    public function setProvnascitaC($provnascitaC)
    {
        $this->provnascitaC = $provnascitaC;
        return $this;
    }

    /**
     * @return string
     */
    public function getRispostaC()
    {
        return $this->rispostaC;
    }

    /**
     * @param string $rispostaC
     * @return AccountCstm
     */
    public function setRispostaC($rispostaC)
    {
        $this->rispostaC = $rispostaC;
        return $this;
    }

    /**
     * @return string
     */
    public function getCodicecciaaC()
    {
        return $this->codicecciaaC;
    }

    /**
     * @param string $codicecciaaC
     * @return AccountCstm
     */
    public function setCodicecciaaC($codicecciaaC)
    {
        $this->codicecciaaC = $codicecciaaC;
        return $this;
    }

    /**
     * @return bool
     */
    public function isAccettazionePrivacyC()
    {
        return $this->accettazionePrivacyC;
    }

    /**
     * @param bool $accettazionePrivacyC
     * @return AccountCstm
     */
    public function setAccettazionePrivacyC($accettazionePrivacyC)
    {
        $this->accettazionePrivacyC = $accettazionePrivacyC;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDataexportC()
    {
        return $this->dataexportC;
    }

    /**
     * @param \DateTime $dataexportC
     * @return AccountCstm
     */
    public function setDataexportC($dataexportC)
    {
        $this->dataexportC = $dataexportC;
        return $this;
    }

    /**
     * @return float
     */
    public function getSpesevarieC()
    {
        return $this->spesevarieC;
    }

    /**
     * @param float $spesevarieC
     * @return AccountCstm
     */
    public function setSpesevarieC($spesevarieC)
    {
        $this->spesevarieC = $spesevarieC;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getExportingammaC()
    {
        return $this->exportingammaC;
    }

    /**
     * @param \DateTime $exportingammaC
     * @return AccountCstm
     */
    public function setExportingammaC($exportingammaC)
    {
        $this->exportingammaC = $exportingammaC;
        return $this;
    }

    /**
     * @return string
     */
    public function getCodicedepositogammaC()
    {
        return $this->codicedepositogammaC;
    }

    /**
     * @param string $codicedepositogammaC
     * @return AccountCstm
     */
    public function setCodicedepositogammaC($codicedepositogammaC)
    {
        $this->codicedepositogammaC = $codicedepositogammaC;
        return $this;
    }

    /**
     * @return string
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param string $userId
     * @return AccountCstm
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getExportdepingammaC()
    {
        return $this->exportdepingammaC;
    }

    /**
     * @param \DateTime $exportdepingammaC
     * @return AccountCstm
     */
    public function setExportdepingammaC($exportdepingammaC)
    {
        $this->exportdepingammaC = $exportdepingammaC;
        return $this;
    }

    /**
     * @return int
     */
    public function getIseparichiestainfo()
    {
        return $this->iseparichiestainfo;
    }

    /**
     * @param int $iseparichiestainfo
     * @return AccountCstm
     */
    public function setIseparichiestainfo($iseparichiestainfo)
    {
        $this->iseparichiestainfo = $iseparichiestainfo;
        return $this;
    }

    /**
     * @return int
     */
    public function getIsepatipocliente()
    {
        return $this->isepatipocliente;
    }

    /**
     * @param int $isepatipocliente
     * @return AccountCstm
     */
    public function setIsepatipocliente($isepatipocliente)
    {
        $this->isepatipocliente = $isepatipocliente;
        return $this;
    }

    /**
     * @return string
     */
    public function getIsepaiban()
    {
        return $this->isepaiban;
    }

    /**
     * @param string $isepaiban
     * @return AccountCstm
     */
    public function setIsepaiban($isepaiban)
    {
        $this->isepaiban = $isepaiban;
        return $this;
    }

    /**
     * @return int
     */
    public function getIsepafacoltastorno()
    {
        return $this->isepafacoltastorno;
    }

    /**
     * @param int $isepafacoltastorno
     * @return AccountCstm
     */
    public function setIsepafacoltastorno($isepafacoltastorno)
    {
        $this->isepafacoltastorno = $isepafacoltastorno;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getIsepadatasottoscrizione()
    {
        return $this->isepadatasottoscrizione;
    }

    /**
     * @param \DateTime $isepadatasottoscrizione
     * @return AccountCstm
     */
    public function setIsepadatasottoscrizione($isepadatasottoscrizione)
    {
        $this->isepadatasottoscrizione = $isepadatasottoscrizione;
        return $this;
    }

    /**
     * @return int
     */
    public function getIsepamotivazionediniego()
    {
        return $this->isepamotivazionediniego;
    }

    /**
     * @param int $isepamotivazionediniego
     * @return AccountCstm
     */
    public function setIsepamotivazionediniego($isepamotivazionediniego)
    {
        $this->isepamotivazionediniego = $isepamotivazionediniego;
        return $this;
    }

    /**
     * @return string
     */
    public function getIsepanuovabanca()
    {
        return $this->isepanuovabanca;
    }

    /**
     * @param string $isepanuovabanca
     * @return AccountCstm
     */
    public function setIsepanuovabanca($isepanuovabanca)
    {
        $this->isepanuovabanca = $isepanuovabanca;
        return $this;
    }

    /**
     * @return int
     */
    public function getMagDepositoprincipaleC()
    {
        return $this->magDepositoprincipaleC;
    }

    /**
     * @param int $magDepositoprincipaleC
     * @return AccountCstm
     */
    public function setMagDepositoprincipaleC($magDepositoprincipaleC)
    {
        $this->magDepositoprincipaleC = $magDepositoprincipaleC;
        return $this;
    }

    /**
     * @return string
     */
    public function getRidSottoscrittoreC()
    {
        return $this->ridSottoscrittoreC;
    }

    /**
     * @param string $ridSottoscrittoreC
     * @return AccountCstm
     */
    public function setRidSottoscrittoreC($ridSottoscrittoreC)
    {
        $this->ridSottoscrittoreC = $ridSottoscrittoreC;
        return $this;
    }

    /**
     * @return string
     */
    public function getRidSottoscrittoreIndirizzoC()
    {
        return $this->ridSottoscrittoreIndirizzoC;
    }

    /**
     * @param string $ridSottoscrittoreIndirizzoC
     * @return AccountCstm
     */
    public function setRidSottoscrittoreIndirizzoC($ridSottoscrittoreIndirizzoC)
    {
        $this->ridSottoscrittoreIndirizzoC = $ridSottoscrittoreIndirizzoC;
        return $this;
    }

    /**
     * @return string
     */
    public function getRidSottoscrittoreCodicefiscaleC()
    {
        return $this->ridSottoscrittoreCodicefiscaleC;
    }

    /**
     * @param string $ridSottoscrittoreCodicefiscaleC
     * @return AccountCstm
     */
    public function setRidSottoscrittoreCodicefiscaleC($ridSottoscrittoreCodicefiscaleC)
    {
        $this->ridSottoscrittoreCodicefiscaleC = $ridSottoscrittoreCodicefiscaleC;
        return $this;
    }

    /**
     * @return string
     */
    public function getRidSottoscrittoreLocalitaC()
    {
        return $this->ridSottoscrittoreLocalitaC;
    }

    /**
     * @param string $ridSottoscrittoreLocalitaC
     * @return AccountCstm
     */
    public function setRidSottoscrittoreLocalitaC($ridSottoscrittoreLocalitaC)
    {
        $this->ridSottoscrittoreLocalitaC = $ridSottoscrittoreLocalitaC;
        return $this;
    }

    /**
     * @return int
     */
    public function getCoreb2b()
    {
        return $this->coreb2b;
    }

    /**
     * @param int $coreb2b
     * @return AccountCstm
     */
    public function setCoreb2b($coreb2b)
    {
        $this->coreb2b = $coreb2b;
        return $this;
    }

    /**
     * @return string
     */
    public function getTipocodiceridC()
    {
        return $this->tipocodiceridC;
    }

    /**
     * @param string $tipocodiceridC
     * @return AccountCstm
     */
    public function setTipocodiceridC($tipocodiceridC)
    {
        $this->tipocodiceridC = $tipocodiceridC;
        return $this;
    }

    /**
     * @return string
     */
    public function getCodicedebitoreC()
    {
        return $this->codicedebitoreC;
    }

    /**
     * @param string $codicedebitoreC
     * @return AccountCstm
     */
    public function setCodicedebitoreC($codicedebitoreC)
    {
        $this->codicedebitoreC = $codicedebitoreC;
        return $this;
    }

    /**
     * @return string
     */
    public function getTipoclienteC()
    {
        return $this->tipoclienteC;
    }

    /**
     * @param string $tipoclienteC
     * @return AccountCstm
     */
    public function setTipoclienteC($tipoclienteC)
    {
        $this->tipoclienteC = $tipoclienteC;
        return $this;
    }

    /**
     * @return string
     */
    public function getFacoltastornoC()
    {
        return $this->facoltastornoC;
    }

    /**
     * @param string $facoltastornoC
     * @return AccountCstm
     */
    public function setFacoltastornoC($facoltastornoC)
    {
        $this->facoltastornoC = $facoltastornoC;
        return $this;
    }

    /**
     * @return string
     */
    public function getCodiceindividualeC()
    {
        return $this->codiceindividualeC;
    }

    /**
     * @param string $codiceindividualeC
     * @return AccountCstm
     */
    public function setCodiceindividualeC($codiceindividualeC)
    {
        $this->codiceindividualeC = $codiceindividualeC;
        return $this;
    }

    /**
     * @return string
     */
    public function getIbanC()
    {
        return $this->ibanC;
    }

    /**
     * @param string $ibanC
     * @return AccountCstm
     */
    public function setIbanC($ibanC)
    {
        $this->ibanC = $ibanC;
        return $this;
    }

    /**
     * @return string
     */
    public function getTiposequenzaincassoC()
    {
        return $this->tiposequenzaincassoC;
    }

    /**
     * @param string $tiposequenzaincassoC
     * @return AccountCstm
     */
    public function setTiposequenzaincassoC($tiposequenzaincassoC)
    {
        $this->tiposequenzaincassoC = $tiposequenzaincassoC;
        return $this;
    }

    /**
     * @return string
     */
    public function getStatusridC()
    {
        return $this->statusridC;
    }

    /**
     * @param string $statusridC
     * @return AccountCstm
     */
    public function setStatusridC($statusridC)
    {
        $this->statusridC = $statusridC;
        return $this;
    }

    /**
     * @return string
     */
    public function getTipoincassosddC()
    {
        return $this->tipoincassosddC;
    }

    /**
     * @param string $tipoincassosddC
     * @return AccountCstm
     */
    public function setTipoincassosddC($tipoincassosddC)
    {
        $this->tipoincassosddC = $tipoincassosddC;
        return $this;
    }

    /**
     * @return string
     */
    public function getSottoscrittoreCfC()
    {
        return $this->sottoscrittoreCfC;
    }

    /**
     * @param string $sottoscrittoreCfC
     * @return AccountCstm
     */
    public function setSottoscrittoreCfC($sottoscrittoreCfC)
    {
        $this->sottoscrittoreCfC = $sottoscrittoreCfC;
        return $this;
    }

    /**
     * @return string
     */
    public function getSottoscrittoreIndirizzoC()
    {
        return $this->sottoscrittoreIndirizzoC;
    }

    /**
     * @param string $sottoscrittoreIndirizzoC
     * @return AccountCstm
     */
    public function setSottoscrittoreIndirizzoC($sottoscrittoreIndirizzoC)
    {
        $this->sottoscrittoreIndirizzoC = $sottoscrittoreIndirizzoC;
        return $this;
    }

    /**
     * @return string
     */
    public function getSottoscrittoreC()
    {
        return $this->sottoscrittoreC;
    }

    /**
     * @param string $sottoscrittoreC
     * @return AccountCstm
     */
    public function setSottoscrittoreC($sottoscrittoreC)
    {
        $this->sottoscrittoreC = $sottoscrittoreC;
        return $this;
    }

    /**
     * @return string
     */
    public function getSottoscrittoreLocalitaC()
    {
        return $this->sottoscrittoreLocalitaC;
    }

    /**
     * @param string $sottoscrittoreLocalitaC
     * @return AccountCstm
     */
    public function setSottoscrittoreLocalitaC($sottoscrittoreLocalitaC)
    {
        $this->sottoscrittoreLocalitaC = $sottoscrittoreLocalitaC;
        return $this;
    }

    /**
     * @return string
     */
    public function getSubdealercodeC()
    {
        return $this->subdealercodeC;
    }

    /**
     * @param string $subdealercodeC
     * @return AccountCstm
     */
    public function setSubdealercodeC($subdealercodeC)
    {
        $this->subdealercodeC = $subdealercodeC;
        return $this;
    }

    /**
     * @return string
     */
    public function getSubdealerreporttoidC()
    {
        return $this->subdealerreporttoidC;
    }

    /**
     * @param string $subdealerreporttoidC
     * @return AccountCstm
     */
    public function setSubdealerreporttoidC($subdealerreporttoidC)
    {
        $this->subdealerreporttoidC = $subdealerreporttoidC;
        return $this;
    }

    /**
     * @return bool
     */
    public function isCheckcoverageC()
    {
        return $this->checkcoverageC;
    }

    /**
     * @param bool $checkcoverageC
     * @return AccountCstm
     */
    public function setCheckcoverageC($checkcoverageC)
    {
        $this->checkcoverageC = $checkcoverageC;
        return $this;
    }

    /**
     * @return bool
     */
    public function isCoverageTypeC()
    {
        return $this->coverageTypeC;
    }

    /**
     * @param bool $coverageTypeC
     * @return AccountCstm
     */
    public function setCoverageTypeC($coverageTypeC)
    {
        $this->coverageTypeC = $coverageTypeC;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDatecheckcoverageC()
    {
        return $this->datecheckcoverageC;
    }

    /**
     * @param \DateTime $datecheckcoverageC
     * @return AccountCstm
     */
    public function setDatecheckcoverageC($datecheckcoverageC)
    {
        $this->datecheckcoverageC = $datecheckcoverageC;
        return $this;
    }

    /**
     * @return string
     */
    public function getFtelCodUniOuC()
    {
        return $this->ftelCodUniOuC;
    }

    /**
     * @param string $ftelCodUniOuC
     * @return AccountCstm
     */
    public function setFtelCodUniOuC($ftelCodUniOuC)
    {
        $this->ftelCodUniOuC = $ftelCodUniOuC;
        return $this;
    }

    /**
     * @return string
     */
    public function getFtelIndirizzopecC()
    {
        return $this->ftelIndirizzopecC;
    }

    /**
     * @param string $ftelIndirizzopecC
     * @return AccountCstm
     */
    public function setFtelIndirizzopecC($ftelIndirizzopecC)
    {
        $this->ftelIndirizzopecC = $ftelIndirizzopecC;
        return $this;
    }

    /**
     * @return string
     */
    public function getCodicegammaC()
    {
        return $this->codicegammaC;
    }

    /**
     * @param string $codicegammaC
     * @return AccountCstm
     */
    public function setCodicegammaC($codicegammaC)
    {
        $this->codicegammaC = $codicegammaC;
        return $this;
    }

    /**
     * @return int
     */
    public function getCg1mCodiceCg16C()
    {
        return $this->cg1mCodiceCg16C;
    }

    /**
     * @param int $cg1mCodiceCg16C
     * @return AccountCstm
     */
    public function setCg1mCodiceCg16C($cg1mCodiceCg16C)
    {
        $this->cg1mCodiceCg16C = $cg1mCodiceCg16C;
        return $this;
    }

    /**
     * @return string
     */
    public function getLatitudineC()
    {
        return $this->latitudineC;
    }

    /**
     * @param string $latitudineC
     * @return AccountCstm
     */
    public function setLatitudineC($latitudineC)
    {
        $this->latitudineC = $latitudineC;
        return $this;
    }

    /**
     * @return string
     */
    public function getLongitudineC()
    {
        return $this->longitudineC;
    }

    /**
     * @param string $longitudineC
     * @return AccountCstm
     */
    public function setLongitudineC($longitudineC)
    {
        $this->longitudineC = $longitudineC;
        return $this;
    }

    /**
     * @return string
     */
    public function getReturnedstreetC()
    {
        return $this->returnedstreetC;
    }

    /**
     * @param string $returnedstreetC
     * @return AccountCstm
     */
    public function setReturnedstreetC($returnedstreetC)
    {
        $this->returnedstreetC = $returnedstreetC;
        return $this;
    }

    /**
     * @return bool
     */
    public function isFatturazioneunificataC()
    {
        return $this->fatturazioneunificataC;
    }

    /**
     * @param bool $fatturazioneunificataC
     * @return AccountCstm
     */
    public function setFatturazioneunificataC($fatturazioneunificataC)
    {
        $this->fatturazioneunificataC = $fatturazioneunificataC;
        return $this;
    }

    /**
     * @return bool
     */
    public function isIncludifeesubdealerC()
    {
        return $this->includifeesubdealerC;
    }

    /**
     * @param bool $includifeesubdealerC
     * @return AccountCstm
     */
    public function setIncludifeesubdealerC($includifeesubdealerC)
    {
        $this->includifeesubdealerC = $includifeesubdealerC;
        return $this;
    }

    /**
     * @return string
     */
    public function getOperatoreC()
    {
        return $this->operatoreC;
    }

    /**
     * @param string $operatoreC
     * @return AccountCstm
     */
    public function setOperatoreC($operatoreC)
    {
        $this->operatoreC = $operatoreC;
        return $this;
    }

    /**
     * @return string
     */
    public function getCodiceaffiliatoC()
    {
        return $this->codiceaffiliatoC;
    }

    /**
     * @param string $codiceaffiliatoC
     * @return AccountCstm
     */
    public function setCodiceaffiliatoC($codiceaffiliatoC)
    {
        $this->codiceaffiliatoC = $codiceaffiliatoC;
        return $this;
    }





}

