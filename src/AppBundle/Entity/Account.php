<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Account
 *
 * @ORM\Table(name="accounts", indexes={@ORM\Index(name="idx_accnt_id_del", columns={"id", "deleted"}), @ORM\Index(name="IDX_accounts", columns={"name", "deleted"}), @ORM\Index(name="IDX_accounts_id", columns={"id"}), @ORM\Index(name="IDX_accounts_name", columns={"name"}), @ORM\Index(name="IDX_accounts_parent_id", columns={"parent_id"}), @ORM\Index(name="IDX_accounts_phone_alternate", columns={"phone_alternate"}), @ORM\Index(name="IDX_accounts_phone_office", columns={"phone_office", "phone_fax", "phone_alternate"}), @ORM\Index(name="IDX_accounts2", columns={"name", "deleted", "billing_address_state", "billing_address_city"})})
 * @ORM\Entity
 */
class Account
{

    /**
     * @var AccountCstm
     *
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\AccountCstm", mappedBy="account", cascade={"persist"})
     */
    private $accountCstm;

    /**
     * @var AccountContratti
     *
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\AccountContratti", mappedBy="account", cascade={"persist"})
     */
    private $accountContratti;

    /**
     * @var string
     *
     * @ORM\Column(name="id", type="string", length=36, nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id = '\'\'';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_entered", type="datetime", nullable=false)
     */
    private $dateEntered = '\'0000-00-00 00:00:00\'';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_modified", type="datetime", nullable=false)
     */
    private $dateModified = '\'0000-00-00 00:00:00\'';

    /**
     * @var string
     *
     * @ORM\Column(name="modified_user_id", type="string", length=36, nullable=false)
     */
    private $modifiedUserId = '\'\'';

    /**
     * @var string
     *
     * @ORM\Column(name="assigned_user_id", type="string", length=36, nullable=true)
     */
    private $assignedUserId = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="created_by", type="string", length=36, nullable=true)
     */
    private $createdBy = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=150, nullable=true)
     */
    private $name = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="parent_id", type="string", length=36, nullable=true)
     */
    private $parentId = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="account_type", type="string", length=25, nullable=true)
     */
    private $accountType = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="industry", type="string", length=25, nullable=true)
     */
    private $industry = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="annual_revenue", type="string", length=25, nullable=true)
     */
    private $annualRevenue = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="phone_fax", type="string", length=25, nullable=true)
     */
    private $phoneFax = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="billing_address_street", type="string", length=150, nullable=true)
     */
    private $billingAddressStreet = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="billing_address_city", type="string", length=100, nullable=true)
     */
    private $billingAddressCity = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="billing_address_state", type="string", length=100, nullable=true)
     */
    private $billingAddressState = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="billing_address_postalcode", type="string", length=20, nullable=true)
     */
    private $billingAddressPostalcode = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="billing_address_country", type="string", length=100, nullable=true)
     */
    private $billingAddressCountry = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", length=65535, nullable=true)
     */
    private $description = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="rating", type="string", length=25, nullable=true)
     */
    private $rating = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="phone_office", type="string", length=25, nullable=true)
     */
    private $phoneOffice = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="phone_alternate", type="string", length=25, nullable=true)
     */
    private $phoneAlternate = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="email1", type="string", length=100, nullable=true)
     */
    private $email1 = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="email2", type="string", length=100, nullable=true)
     */
    private $email2 = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="website", type="string", length=255, nullable=true)
     */
    private $website = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="ownership", type="string", length=100, nullable=true)
     */
    private $ownership = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="employees", type="string", length=10, nullable=true)
     */
    private $employees = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="sic_code", type="string", length=10, nullable=true)
     */
    private $sicCode = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="ticker_symbol", type="string", length=10, nullable=true)
     */
    private $tickerSymbol = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="shipping_address_street", type="string", length=150, nullable=true)
     */
    private $shippingAddressStreet = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="shipping_address_city", type="string", length=100, nullable=true)
     */
    private $shippingAddressCity = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="shipping_address_state", type="string", length=100, nullable=true)
     */
    private $shippingAddressState = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="shipping_address_postalcode", type="string", length=20, nullable=true)
     */
    private $shippingAddressPostalcode = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="shipping_address_country", type="string", length=100, nullable=true)
     */
    private $shippingAddressCountry = 'NULL';

    /**
     * @var boolean
     *
     * @ORM\Column(name="deleted", type="boolean", nullable=false)
     */
    private $deleted = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="created_by_name", type="string", length=45, nullable=true)
     */
    private $createdByName = '\'\'';

    /**
     * @return AccountContratti
     */
    public function getAccountContratti()
    {
        return $this->accountContratti;
    }

    /**
     * @param AccountContratti $accountContratti
     * @return Account
     */
    public function setAccountContratti($accountContratti)
    {
        $this->accountContratti = $accountContratti;
        return $this;
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return Account
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateEntered()
    {
        return $this->dateEntered;
    }

    /**
     * @param \DateTime $dateEntered
     * @return Account
     */
    public function setDateEntered($dateEntered)
    {
        $this->dateEntered = $dateEntered;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDateModified()
    {
        return $this->dateModified;
    }

    /**
     * @param \DateTime $dateModified
     * @return Account
     */
    public function setDateModified($dateModified)
    {
        $this->dateModified = $dateModified;
        return $this;
    }

    /**
     * @return string
     */
    public function getModifiedUserId()
    {
        return $this->modifiedUserId;
    }

    /**
     * @param string $modifiedUserId
     * @return Account
     */
    public function setModifiedUserId($modifiedUserId)
    {
        $this->modifiedUserId = $modifiedUserId;
        return $this;
    }

    /**
     * @return string
     */
    public function getAssignedUserId()
    {
        return $this->assignedUserId;
    }

    /**
     * @param string $assignedUserId
     * @return Account
     */
    public function setAssignedUserId($assignedUserId)
    {
        $this->assignedUserId = $assignedUserId;
        return $this;
    }

    /**
     * @return string
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * @param string $createdBy
     * @return Account
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;
        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Account
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getParentId()
    {
        return $this->parentId;
    }

    /**
     * @param string $parentId
     * @return Account
     */
    public function setParentId($parentId)
    {
        $this->parentId = $parentId;
        return $this;
    }

    /**
     * @return string
     */
    public function getAccountType()
    {
        return $this->accountType;
    }

    /**
     * @param string $accountType
     * @return Account
     */
    public function setAccountType($accountType)
    {
        $this->accountType = $accountType;
        return $this;
    }

    /**
     * @return string
     */
    public function getIndustry()
    {
        return $this->industry;
    }

    /**
     * @param string $industry
     * @return Account
     */
    public function setIndustry($industry)
    {
        $this->industry = $industry;
        return $this;
    }

    /**
     * @return string
     */
    public function getAnnualRevenue()
    {
        return $this->annualRevenue;
    }

    /**
     * @param string $annualRevenue
     * @return Account
     */
    public function setAnnualRevenue($annualRevenue)
    {
        $this->annualRevenue = $annualRevenue;
        return $this;
    }

    /**
     * @return string
     */
    public function getPhoneFax()
    {
        return $this->phoneFax;
    }

    /**
     * @param string $phoneFax
     * @return Account
     */
    public function setPhoneFax($phoneFax)
    {
        $this->phoneFax = $phoneFax;
        return $this;
    }

    /**
     * @return string
     */
    public function getBillingAddressStreet()
    {
        return $this->billingAddressStreet;
    }

    /**
     * @param string $billingAddressStreet
     * @return Account
     */
    public function setBillingAddressStreet($billingAddressStreet)
    {
        $this->billingAddressStreet = $billingAddressStreet;
        return $this;
    }

    /**
     * @return string
     */
    public function getBillingAddressCity()
    {
        return $this->billingAddressCity;
    }

    /**
     * @param string $billingAddressCity
     * @return Account
     */
    public function setBillingAddressCity($billingAddressCity)
    {
        $this->billingAddressCity = $billingAddressCity;
        return $this;
    }

    /**
     * @return string
     */
    public function getBillingAddressState()
    {
        return $this->billingAddressState;
    }

    /**
     * @param string $billingAddressState
     * @return Account
     */
    public function setBillingAddressState($billingAddressState)
    {
        $this->billingAddressState = $billingAddressState;
        return $this;
    }

    /**
     * @return string
     */
    public function getBillingAddressPostalcode()
    {
        return $this->billingAddressPostalcode;
    }

    /**
     * @param string $billingAddressPostalcode
     * @return Account
     */
    public function setBillingAddressPostalcode($billingAddressPostalcode)
    {
        $this->billingAddressPostalcode = $billingAddressPostalcode;
        return $this;
    }

    /**
     * @return string
     */
    public function getBillingAddressCountry()
    {
        return $this->billingAddressCountry;
    }

    /**
     * @param string $billingAddressCountry
     * @return Account
     */
    public function setBillingAddressCountry($billingAddressCountry)
    {
        $this->billingAddressCountry = $billingAddressCountry;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return Account
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return string
     */
    public function getRating()
    {
        return $this->rating;
    }

    /**
     * @param string $rating
     * @return Account
     */
    public function setRating($rating)
    {
        $this->rating = $rating;
        return $this;
    }

    /**
     * @return string
     */
    public function getPhoneOffice()
    {
        return $this->phoneOffice;
    }

    /**
     * @param string $phoneOffice
     * @return Account
     */
    public function setPhoneOffice($phoneOffice)
    {
        $this->phoneOffice = $phoneOffice;
        return $this;
    }

    /**
     * @return string
     */
    public function getPhoneAlternate()
    {
        return $this->phoneAlternate;
    }

    /**
     * @param string $phoneAlternate
     * @return Account
     */
    public function setPhoneAlternate($phoneAlternate)
    {
        $this->phoneAlternate = $phoneAlternate;
        return $this;
    }

    /**
     * @return string
     */
    public function getEmail1()
    {
        return $this->email1;
    }

    /**
     * @param string $email1
     * @return Account
     */
    public function setEmail1($email1)
    {
        $this->email1 = $email1;
        return $this;
    }

    /**
     * @return string
     */
    public function getEmail2()
    {
        return $this->email2;
    }

    /**
     * @param string $email2
     * @return Account
     */
    public function setEmail2($email2)
    {
        $this->email2 = $email2;
        return $this;
    }

    /**
     * @return string
     */
    public function getWebsite()
    {
        return $this->website;
    }

    /**
     * @param string $website
     * @return Account
     */
    public function setWebsite($website)
    {
        $this->website = $website;
        return $this;
    }

    /**
     * @return string
     */
    public function getOwnership()
    {
        return $this->ownership;
    }

    /**
     * @param string $ownership
     * @return Account
     */
    public function setOwnership($ownership)
    {
        $this->ownership = $ownership;
        return $this;
    }

    /**
     * @return string
     */
    public function getEmployees()
    {
        return $this->employees;
    }

    /**
     * @param string $employees
     * @return Account
     */
    public function setEmployees($employees)
    {
        $this->employees = $employees;
        return $this;
    }

    /**
     * @return string
     */
    public function getSicCode()
    {
        return $this->sicCode;
    }

    /**
     * @param string $sicCode
     * @return Account
     */
    public function setSicCode($sicCode)
    {
        $this->sicCode = $sicCode;
        return $this;
    }

    /**
     * @return string
     */
    public function getTickerSymbol()
    {
        return $this->tickerSymbol;
    }

    /**
     * @param string $tickerSymbol
     * @return Account
     */
    public function setTickerSymbol($tickerSymbol)
    {
        $this->tickerSymbol = $tickerSymbol;
        return $this;
    }

    /**
     * @return string
     */
    public function getShippingAddressStreet()
    {
        return $this->shippingAddressStreet;
    }

    /**
     * @param string $shippingAddressStreet
     * @return Account
     */
    public function setShippingAddressStreet($shippingAddressStreet)
    {
        $this->shippingAddressStreet = $shippingAddressStreet;
        return $this;
    }

    /**
     * @return string
     */
    public function getShippingAddressCity()
    {
        return $this->shippingAddressCity;
    }

    /**
     * @param string $shippingAddressCity
     * @return Account
     */
    public function setShippingAddressCity($shippingAddressCity)
    {
        $this->shippingAddressCity = $shippingAddressCity;
        return $this;
    }

    /**
     * @return string
     */
    public function getShippingAddressState()
    {
        return $this->shippingAddressState;
    }

    /**
     * @param string $shippingAddressState
     * @return Account
     */
    public function setShippingAddressState($shippingAddressState)
    {
        $this->shippingAddressState = $shippingAddressState;
        return $this;
    }

    /**
     * @return string
     */
    public function getShippingAddressPostalcode()
    {
        return $this->shippingAddressPostalcode;
    }

    /**
     * @param string $shippingAddressPostalcode
     * @return Account
     */
    public function setShippingAddressPostalcode($shippingAddressPostalcode)
    {
        $this->shippingAddressPostalcode = $shippingAddressPostalcode;
        return $this;
    }

    /**
     * @return string
     */
    public function getShippingAddressCountry()
    {
        return $this->shippingAddressCountry;
    }

    /**
     * @param string $shippingAddressCountry
     * @return Account
     */
    public function setShippingAddressCountry($shippingAddressCountry)
    {
        $this->shippingAddressCountry = $shippingAddressCountry;
        return $this;
    }

    /**
     * @return bool
     */
    public function isDeleted()
    {
        return $this->deleted;
    }

    /**
     * @param bool $deleted
     * @return Account
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;
        return $this;
    }

    /**
     * @return string
     */
    public function getCreatedByName()
    {
        return $this->createdByName;
    }

    /**
     * @param string $createdByName
     * @return Account
     */
    public function setCreatedByName($createdByName)
    {
        $this->createdByName = $createdByName;
        return $this;
    }

    public function __toString()
    {
        return $this->id;
    }

    /**
     * @return AccountCstm
     */
    public function getAccountCstm()
    {
        return $this->accountCstm;
    }

    /**
     * @param AccountCstm $accountCstm
     * @return Account
     */
    public function setAccountCstm($accountCstm)
    {
        $this->accountCstm = $accountCstm;
        return $this;
    }



}

