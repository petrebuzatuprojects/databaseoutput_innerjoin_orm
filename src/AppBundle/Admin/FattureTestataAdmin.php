<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class FattureTestataAdmin extends AbstractAdmin
{
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('IDContratto')
            ->add('numeroFattura')
            ->add('dataFattura')
            ->add('dataScadenzaPagamento')
            ->add('tipoPagamento')
            ->add('codiceTipoFattura')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('IDContratto')
            ->add('numeroFattura')
            ->add('dataFattura')
            ->add('dataScadenzaPagamento')
            ->add('tipoPagamento')
            ->add('codiceTipoFattura')
            ->add('_action', null, [
                'actions' => [
                    'show' => [],
                    'edit' => [],
                    'delete' => [],
                ],
            ])
        ;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('IDContratto')
            ->add('numeroFattura')
            ->add('dataFattura')
            ->add('dataScadenzaPagamento')
            ->add('tipoPagamento')
            ->add('codiceTipoFattura')
        ;
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('IDContratto')
            ->add('numeroFattura')
            ->add('dataFattura')
            ->add('dataScadenzaPagamento')
            ->add('tipoPagamento')
            ->add('codiceTipoFattura')
        ;
    }
}
