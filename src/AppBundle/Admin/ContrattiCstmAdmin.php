<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class ContrattiCstmAdmin extends AbstractAdmin
{
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id_c')
            ->add('contratti')
            ->add('numeroContrattoC')
            ->add('dataSpedizioneC')
            ->add('tipoPagamentoC')
            ->add('offerteWeb')

            ->add('user')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper

            ->add('id_c')
            ->add('contratti')
            ->add('numeroContrattoC')
            ->add('dataSpedizioneC')
            ->add('tipoPagamentoC')
            ->add('offerteWeb')

            ->add('user')
            ->add('_action', null, [
                'actions' => [
                    'show' => [],
                    'edit' => [],
                    'delete' => [],
                ],
            ])
        ;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper


            ->add('contratti')
            ->add('numeroContrattoC')
            ->add('dataSpedizioneC')
            ->add('tipoPagamentoC')
            ->add('offerteWeb')

            ->add('user')

        ;
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id_c')
            ->add('contratti')
            ->add('numeroContrattoC')
            ->add('dataSpedizioneC')
            ->add('tipoPagamentoC')
            ->add('offerteWeb')

            ->add('user')
        ;
    }
}
